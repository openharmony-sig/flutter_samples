import 'dart:async';

import 'package:openvalley_smart_agriculture/models/json/weather_24_hours_api.dart';
import 'package:openvalley_smart_agriculture/models/json/weather_7_days_api.dart';
import 'package:openvalley_smart_agriculture/models/json/weather_7_days_suggestion_api.dart';
import 'package:openvalley_smart_agriculture/models/json/weather_now_api.dart';
import 'package:openvalley_smart_agriculture/utils/HttpUtil.dart';

import '../utils/StringUtil.dart';
import 'json/suggestion_api.dart';

SuggestionApi? suggestionApi;
Weather7DaysApi? weather7daysApi;
Weather7DaysSuggestionApi? weather7daysSuggestionApi;
Weather24HoursApi? weather24hoursApi;
WeatherNowApi? weatherNowApi;

class SenseInfo {
  //所在位置
  String? location;

  //当前温度
  String? tempNow;

  //最低温°C/最高温°C
  String? tempMaxMin;

  //天气/风向
  String? weatherTip;

  //今日提示
  String? todayTip;

  //体感提示
  String? somatosensoryTip;

  //划船提示
  String? boatingTip;

  //洗车提示
  String? carWashingTip;

  //过敏提示
  String? allergyTip;
}

Future<void> setSuggestionApi() async {
  suggestionApi = await apiGetModel<SuggestionApi>();
}

Future<void> setWeatherNowApi() async {
  weatherNowApi = await apiGetModel<WeatherNowApi>();
}

Future<void> setWeather7DaysApi() async {
  weather7daysApi = await apiGetModel<Weather7DaysApi>();
}

Future<void> setWeather7DaysSuggestionApi() async {
  weather7daysSuggestionApi = await apiGetModel<Weather7DaysSuggestionApi>();
}

Future<SenseInfo> getSenseInfo() async {
  List<Future> list = [
    setSuggestionApi(),
    setWeatherNowApi(),
    setWeather7DaysApi(),
    setWeather7DaysSuggestionApi(),
  ];
  //并发请求
  await Future.wait(list);

  SenseInfo senseInfo = SenseInfo();
  senseInfo.location = suggestionApi?.results?[0].location?.name;
  senseInfo.tempNow = weatherNowApi?.now?.temp;

  senseInfo.tempMaxMin =
      "${weather7daysApi?.daily?[0].tempMin}°C/${weather7daysApi?.daily?[0].tempMax}°C";
  senseInfo.weatherTip =
      "${weather7daysApi?.daily?[0].textDay}/${weather7daysApi?.daily?[0].windDirDay}";

  senseInfo.todayTip = weather7daysSuggestionApi?.data?.forecast?[0].notice;

  senseInfo.somatosensoryTip =
      paddingSpace(suggestionApi?.results?[0].suggestion?[0].comfort?.brief);
  senseInfo.boatingTip =
      paddingSpace(suggestionApi?.results?[0].suggestion?[0].boating?.brief);
  senseInfo.carWashingTip =
      paddingSpace(suggestionApi?.results?[0].suggestion?[0].carWashing?.brief);
  senseInfo.allergyTip =
      paddingSpace(suggestionApi?.results?[0].suggestion?[0].allergy?.brief);
  return senseInfo;
}
