import 'package:openvalley_smart_agriculture/models/json/base_api.dart';

class Weather24HoursApi extends BaseApi{
  String? code;
  String? updateTime;
  String? fxLink;
  List<Hourly>? hourly;
  Refer? refer;

  Weather24HoursApi(
      {this.code, this.updateTime, this.fxLink, this.hourly, this.refer});

  Weather24HoursApi.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    updateTime = json['updateTime'];
    fxLink = json['fxLink'];
    if (json['hourly'] != null) {
      hourly = <Hourly>[];
      json['hourly'].forEach((v) {
        hourly!.add(new Hourly.fromJson(v));
      });
    }
    refer = json['refer'] != null ? new Refer.fromJson(json['refer']) : null;
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['updateTime'] = this.updateTime;
    data['fxLink'] = this.fxLink;
    if (this.hourly != null) {
      data['hourly'] = this.hourly!.map((v) => v.toJson()).toList();
    }
    if (this.refer != null) {
      data['refer'] = this.refer!.toJson();
    }
    return data;
  }
}

class Hourly {
  String? fxTime;
  String? temp;
  String? icon;
  String? text;
  String? wind360;
  String? windDir;
  String? windScale;
  String? windSpeed;
  String? humidity;
  String? precip;
  String? pressure;
  String? cloud;
  String? dew;

  Hourly(
      {this.fxTime,
        this.temp,
        this.icon,
        this.text,
        this.wind360,
        this.windDir,
        this.windScale,
        this.windSpeed,
        this.humidity,
        this.precip,
        this.pressure,
        this.cloud,
        this.dew});

  Hourly.fromJson(Map<String, dynamic> json) {
    fxTime = json['fxTime'];
    temp = json['temp'];
    icon = json['icon'];
    text = json['text'];
    wind360 = json['wind360'];
    windDir = json['windDir'];
    windScale = json['windScale'];
    windSpeed = json['windSpeed'];
    humidity = json['humidity'];
    precip = json['precip'];
    pressure = json['pressure'];
    cloud = json['cloud'];
    dew = json['dew'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['fxTime'] = this.fxTime;
    data['temp'] = this.temp;
    data['icon'] = this.icon;
    data['text'] = this.text;
    data['wind360'] = this.wind360;
    data['windDir'] = this.windDir;
    data['windScale'] = this.windScale;
    data['windSpeed'] = this.windSpeed;
    data['humidity'] = this.humidity;
    data['precip'] = this.precip;
    data['pressure'] = this.pressure;
    data['cloud'] = this.cloud;
    data['dew'] = this.dew;
    return data;
  }
}

class Refer {
  List<String>? sources;
  List<String>? license;

  Refer({this.sources, this.license});

  Refer.fromJson(Map<String, dynamic> json) {
    sources = json['sources'].cast<String>();
    license = json['license'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['sources'] = this.sources;
    data['license'] = this.license;
    return data;
  }
}
