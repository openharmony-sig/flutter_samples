import 'package:openvalley_smart_agriculture/models/json/base_api.dart';

class WeatherNowApi extends BaseApi{
  String? code;
  String? updateTime;
  String? fxLink;
  Now? now;
  Refer? refer;

  WeatherNowApi(
      {this.code, this.updateTime, this.fxLink, this.now, this.refer});

  WeatherNowApi.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    updateTime = json['updateTime'];
    fxLink = json['fxLink'];
    now = json['now'] != null ? new Now.fromJson(json['now']) : null;
    refer = json['refer'] != null ? new Refer.fromJson(json['refer']) : null;
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['updateTime'] = this.updateTime;
    data['fxLink'] = this.fxLink;
    if (this.now != null) {
      data['now'] = this.now!.toJson();
    }
    if (this.refer != null) {
      data['refer'] = this.refer!.toJson();
    }
    return data;
  }
}

class Now {
  String? obsTime;
  String? temp;
  String? feelsLike;
  String? icon;
  String? text;
  String? wind360;
  String? windDir;
  String? windScale;
  String? windSpeed;
  String? humidity;
  String? precip;
  String? pressure;
  String? vis;
  String? cloud;
  String? dew;

  Now(
      {this.obsTime,
      this.temp,
      this.feelsLike,
      this.icon,
      this.text,
      this.wind360,
      this.windDir,
      this.windScale,
      this.windSpeed,
      this.humidity,
      this.precip,
      this.pressure,
      this.vis,
      this.cloud,
      this.dew});

  Now.fromJson(Map<String, dynamic> json) {
    obsTime = json['obsTime'];
    temp = json['temp'];
    feelsLike = json['feelsLike'];
    icon = json['icon'];
    text = json['text'];
    wind360 = json['wind360'];
    windDir = json['windDir'];
    windScale = json['windScale'];
    windSpeed = json['windSpeed'];
    humidity = json['humidity'];
    precip = json['precip'];
    pressure = json['pressure'];
    vis = json['vis'];
    cloud = json['cloud'];
    dew = json['dew'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['obsTime'] = this.obsTime;
    data['temp'] = this.temp;
    data['feelsLike'] = this.feelsLike;
    data['icon'] = this.icon;
    data['text'] = this.text;
    data['wind360'] = this.wind360;
    data['windDir'] = this.windDir;
    data['windScale'] = this.windScale;
    data['windSpeed'] = this.windSpeed;
    data['humidity'] = this.humidity;
    data['precip'] = this.precip;
    data['pressure'] = this.pressure;
    data['vis'] = this.vis;
    data['cloud'] = this.cloud;
    data['dew'] = this.dew;
    return data;
  }
}

class Refer {
  List<String>? sources;
  List<String>? license;

  Refer({this.sources, this.license});

  Refer.fromJson(Map<String, dynamic> json) {
    sources = json['sources'].cast<String>();
    license = json['license'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['sources'] = this.sources;
    data['license'] = this.license;
    return data;
  }
}
