import 'package:openvalley_smart_agriculture/constants/api_config.dart';

import 'base_api.dart';

class SuggestionApi implements BaseApi {
  List<Results>? results;

  SuggestionApi({this.results});

  SuggestionApi.fromJson(Map<String, dynamic> json) {
    if (json['results'] != null) {
      results = <Results>[];
      json['results'].forEach((v) {
        results!.add(Results.fromJson(v));
      });
    }
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (results != null) {
      data['results'] = results!.map((v) => v.toJson()).toList();
    }
    return data;
  }

}

class Results {
  Location? location;
  List<Suggestion>? suggestion;
  String? lastUpdate;

  Results({this.location, this.suggestion, this.lastUpdate});

  Results.fromJson(Map<String, dynamic> json) {
    location =
        json['location'] != null ? Location.fromJson(json['location']) : null;
    if (json['suggestion'] != null) {
      suggestion = <Suggestion>[];
      json['suggestion'].forEach((v) {
        suggestion!.add(Suggestion.fromJson(v));
      });
    }
    lastUpdate = json['last_update'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (location != null) {
      data['location'] = location!.toJson();
    }
    if (suggestion != null) {
      data['suggestion'] = suggestion!.map((v) => v.toJson()).toList();
    }
    data['last_update'] = lastUpdate;
    return data;
  }
}

class Location {
  String? id;
  String? name;
  String? country;
  String? path;
  String? timezone;
  String? timezoneOffset;

  Location(
      {this.id,
      this.name,
      this.country,
      this.path,
      this.timezone,
      this.timezoneOffset});

  Location.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    country = json['country'];
    path = json['path'];
    timezone = json['timezone'];
    timezoneOffset = json['timezone_offset'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['country'] = country;
    data['path'] = path;
    data['timezone'] = timezone;
    data['timezone_offset'] = timezoneOffset;
    return data;
  }
}

class Suggestion {
  String? date;
  Ac? ac;
  Ac? airPollution;
  Ac? airing;
  Ac? allergy;
  Ac? beer;
  Ac? boating;
  Ac? carWashing;
  Ac? comfort;
  Ac? dressing;
  Ac? fishing;
  Ac? flu;
  Ac? kiteflying;
  Ac? makeup;
  Ac? mood;
  Ac? morningSport;
  Ac? roadCondition;
  Ac? shopping;
  Ac? sport;
  Ac? sunscreen;
  Ac? traffic;
  Ac? umbrella;
  Ac? uv;

  Suggestion(
      {this.date,
      this.ac,
      this.airPollution,
      this.airing,
      this.allergy,
      this.beer,
      this.boating,
      this.carWashing,
      this.comfort,
      this.dressing,
      this.fishing,
      this.flu,
      this.kiteflying,
      this.makeup,
      this.mood,
      this.morningSport,
      this.roadCondition,
      this.shopping,
      this.sport,
      this.sunscreen,
      this.traffic,
      this.umbrella,
      this.uv});

  Suggestion.fromJson(Map<String, dynamic> json) {
    date = json['date'];
    ac = json['ac'] != null ? Ac.fromJson(json['ac']) : null;
    airPollution = json['air_pollution'] != null
        ? Ac.fromJson(json['air_pollution'])
        : null;
    airing = json['airing'] != null ? Ac.fromJson(json['airing']) : null;
    allergy = json['allergy'] != null ? Ac.fromJson(json['allergy']) : null;
    beer = json['beer'] != null ? Ac.fromJson(json['beer']) : null;
    boating = json['boating'] != null ? Ac.fromJson(json['boating']) : null;
    carWashing =
        json['car_washing'] != null ? Ac.fromJson(json['car_washing']) : null;
    comfort = json['comfort'] != null ? Ac.fromJson(json['comfort']) : null;
    dressing = json['dressing'] != null ? Ac.fromJson(json['dressing']) : null;
    fishing = json['fishing'] != null ? Ac.fromJson(json['fishing']) : null;
    flu = json['flu'] != null ? Ac.fromJson(json['flu']) : null;
    kiteflying =
        json['kiteflying'] != null ? Ac.fromJson(json['kiteflying']) : null;
    makeup = json['makeup'] != null ? Ac.fromJson(json['makeup']) : null;
    mood = json['mood'] != null ? Ac.fromJson(json['mood']) : null;
    morningSport = json['morning_sport'] != null
        ? Ac.fromJson(json['morning_sport'])
        : null;
    roadCondition = json['road_condition'] != null
        ? Ac.fromJson(json['road_condition'])
        : null;
    shopping = json['shopping'] != null ? Ac.fromJson(json['shopping']) : null;
    sport = json['sport'] != null ? Ac.fromJson(json['sport']) : null;
    sunscreen =
        json['sunscreen'] != null ? Ac.fromJson(json['sunscreen']) : null;
    traffic = json['traffic'] != null ? Ac.fromJson(json['traffic']) : null;
    umbrella = json['umbrella'] != null ? Ac.fromJson(json['umbrella']) : null;
    uv = json['uv'] != null ? Ac.fromJson(json['uv']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['date'] = date;
    if (ac != null) {
      data['ac'] = ac!.toJson();
    }
    if (airPollution != null) {
      data['air_pollution'] = airPollution!.toJson();
    }
    if (airing != null) {
      data['airing'] = airing!.toJson();
    }
    if (allergy != null) {
      data['allergy'] = allergy!.toJson();
    }
    if (beer != null) {
      data['beer'] = beer!.toJson();
    }
    if (boating != null) {
      data['boating'] = boating!.toJson();
    }
    if (carWashing != null) {
      data['car_washing'] = carWashing!.toJson();
    }
    if (comfort != null) {
      data['comfort'] = comfort!.toJson();
    }
    if (dressing != null) {
      data['dressing'] = dressing!.toJson();
    }
    if (fishing != null) {
      data['fishing'] = fishing!.toJson();
    }
    if (flu != null) {
      data['flu'] = flu!.toJson();
    }
    if (kiteflying != null) {
      data['kiteflying'] = kiteflying!.toJson();
    }
    if (makeup != null) {
      data['makeup'] = makeup!.toJson();
    }
    if (mood != null) {
      data['mood'] = mood!.toJson();
    }
    if (morningSport != null) {
      data['morning_sport'] = morningSport!.toJson();
    }
    if (roadCondition != null) {
      data['road_condition'] = roadCondition!.toJson();
    }
    if (shopping != null) {
      data['shopping'] = shopping!.toJson();
    }
    if (sport != null) {
      data['sport'] = sport!.toJson();
    }
    if (sunscreen != null) {
      data['sunscreen'] = sunscreen!.toJson();
    }
    if (traffic != null) {
      data['traffic'] = traffic!.toJson();
    }
    if (umbrella != null) {
      data['umbrella'] = umbrella!.toJson();
    }
    if (uv != null) {
      data['uv'] = uv!.toJson();
    }
    return data;
  }
}

class Ac {
  String? brief;
  String? details;

  Ac({this.brief, this.details});

  Ac.fromJson(Map<String, dynamic> json) {
    brief = json['brief'];
    details = json['details'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['brief'] = brief;
    data['details'] = details;
    return data;
  }
}
