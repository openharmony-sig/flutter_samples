import 'package:openvalley_smart_agriculture/models/json/weather_24_hours_api.dart';

import '../utils/HttpUtil.dart';
import '../utils/AssetsUtil.dart';

class HourWeatherInfo {
  //天气图片
  String? weatherAssets;

  //时间描述
  String? timeDesc;

  //温度
  String? temp;
}

Weather24HoursApi? weather24HoursApi;

Future<List<HourWeatherInfo>> getHourWeatherInfo() async {
  weather24HoursApi = await apiGetModel<Weather24HoursApi>();

  List<HourWeatherInfo> list = [];
  var hourlyList = weather24HoursApi?.hourly;
  for (int i = 0; i < (hourlyList?.length ?? 0); i++) {
    Hourly? hourly = hourlyList?[i];
    HourWeatherInfo info = HourWeatherInfo();
    info.weatherAssets = getWeatherAsset(hourly?.text);

    info.temp = "${hourly?.temp??"18"}°C";

    if (i == 0) {
      info.timeDesc = "现在";
    } else {
      String hourStr = hourly?.fxTime?.substring(11, 13) ?? "00";
      int hour = int.parse(hourStr);
      //+7小时等于当前时间
      int now = hour + 7;
      if (now >= 24) {
        now -= 24;
      }
      if (now > 9) {
        hourStr = "$now:00";
      } else {
        hourStr = "0$now:00";
      }
      info.timeDesc = hourStr;
    }
    list.add(info);
  }
  return list;
}
