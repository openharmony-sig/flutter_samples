import 'menu_model.dart';

enum NewsType { agriculturalDevelopment, qualitySafety, diseasePrevention }

class NewsInfo extends MenuModel {
  final NewsType type;

  final List<NewsItem>? newsItems;

  NewsInfo(this.type, {String title = "", this.newsItems}) : super(title);
}

class NewsItem {
  final String? title;
  final String? date;
  final String? location;

  NewsItem({this.title, this.date,this.location});
}

List<NewsInfo> getNewsInfoList() {
  List<NewsInfo> list = [];
  for (var value in NewsType.values) {
    list.add(getNewsInfo(value));
  }
  return list;
}

NewsInfo getNewsInfo(NewsType baseType) {
  switch (baseType) {
    case NewsType.agriculturalDevelopment:
      return NewsInfo(NewsType.agriculturalDevelopment,
          title: "农业发展",
          newsItems: [
            NewsItem(
              title: '六门闸社区：渔民退捕上岸 收入3年翻一番',
              date: '2022-08-09',
              location: '[湖南]',
            ),
            NewsItem(
              title: '邵阳：油菜集中育苗4.96万亩，可移栽大田50万亩以上',
              date: '2022-10-09',
              location: '[湖南]',
            ),
            NewsItem(
              title: '山西切实抓好秋收秋种',
              date: '2022-04-09',
              location: '[山西]',
            ),
            NewsItem(
              title: '黑龙江省粮食作物秋收面积过半',
              date: '2022-01-09',
              location: '[黑龙江]',
            ),
            NewsItem(
              title: '聚焦重点难点 强化政策研究——加快完善天津市乡村振兴政策体系',
              date: '2022-10-09',
              location: '[天津]',
            ),
            NewsItem(
              title: '天津送来的红香酥梨熟了── 天津市对口帮扶甘肃省武威市古浪县引...',
              date: '2022-10-08',
              location: '[天津]',
            ),
            NewsItem(
              title: '蒙自市：14万亩石榴产值达12亿元',
              date: '2022-10-08',
              location: '[云南]',
            ),
            NewsItem(
              title: '大足 共绘现代版“富春山居图”',
              date: '2022-10-08',
              location: '[重庆]',
            ),
            NewsItem(
              title: '重庆新增一高产马铃薯品种 “渝科薯1号”平均亩产达2408.4公斤',
              date: '2022-10-08',
              location: '[重庆]',
            ),
            NewsItem(
              title: '河北定州市五个农业区域公用品牌正式发布',
              date: '2022-10-08',
              location: '[河北]',
            ),
            NewsItem(
              title: '康县阳坝镇：正是秋收好光景 天麻金薯采收忙',
              date: '2022-10-07',
              location: '[甘肃]',
            ),
            NewsItem(
              title: '新疆“沙漠第一村”晚熟甜瓜迎丰收 民众采摘忙',
              date: '2022-10-07',
              location: '[新疆]',
            ),
            NewsItem(
              title: '打造美丽乡村 提升富民产业 村庄规划助推乡村振兴',
              date: '2022-10-07',
              location: '[河南]',
            ),
            NewsItem(
              title: '数十年如一日深耕陇上小麦新品种选育',
              date: '2022-10-08',
              location: '[甘肃]',
            ),
            NewsItem(
              title: '农科院研究秸秆还田取得新进展',
              date: '2022-10-08',
              location: '[湖北]',
            ),
            NewsItem(
              title: '甘蔗航天育种取得新突破”',
              date: '2022-10-08',
              location: '[湖南]',
            ),
            NewsItem(
              title: '21种玫瑰杂交种子完成太空遨游回到昆明',
              date: '2022-10-08',
              location: '[云南]',
            ),
            NewsItem(
              title: '当AI遇见农业 智能化生产触手可',
              date: '2022-10-08',
              location: '[河南]',
            ),
            NewsItem(
              title: '新电商怎么“生生造出”一个新产业？',
              date: '2022-10-07',
              location: '[湖北]',
            ),
            NewsItem(
              title: '白羽肉鸡新品种产业化提速',
              date: '2022-10-07',
              location: '[山东]',
            ),
            NewsItem(
              title: '耐盐大豆新品系完成实收测产 亩产达270公斤以上',
              date: '2022-10-07',
              location: '[山东]',
            ),
          ]);

    case NewsType.qualitySafety:
      return NewsInfo(NewsType.qualitySafety, title: "质量安全", newsItems: [
        NewsItem(
          title: '张合成：推进新时代农业高质量发展',
          date: '2022-10-09',
        ),
        NewsItem(
          title: '全面提高农业质量效益和竞争力：中国经济网—《经济日报》',
          date: '2022-10-08',
        ),
        NewsItem(
          title: '农业农村部办公厅关于深入学习贯彻《中华人民共和国农产品质量安全法》的通知',
          date: '2022-10-08',
        ),
        NewsItem(
          title: '农业农村部关于实施农产品“三品一标”四大行动的通知',
          date: '2022-10-08',
        ),
        NewsItem(
          title: '保障“舌尖上的安全”！农业农村部印发通知，学习贯彻农产品质量安全法',
          date: '2022-10-08',
        ),
        NewsItem(
          title: '农业高质量发展，湘潭有什么“密码”？',
          date: '2022-10-07',
        ),
        NewsItem(
          title: '【中央媒体看民勤】央视新闻CCTV-13关注民勤节水农业 看绿洲大地奏响“节水丰收曲”',
          date: '2022-10-07',
        ),
        NewsItem(
          title: '黑龙江佳木斯全力构建现代农业发展样板区 探索农业高质量发展新路',
          date: '2022-10-07',
        ),
        NewsItem(
          title: '陈晓华：推进农业标准化 保障农产品质量安全',
          date: '2022-10-06',
        ),
        NewsItem(
          title: '【农产品质量安全工作简报 第49期】在部省共同打造青海绿色有机农畜产品输出地推进会议上的讲话',
          date: '2022-10-06',
        ),
        NewsItem(
          title: '继往开来 重塑农产品质量安全治理新秩序',
          date: '2022-10-05',
        ),
        NewsItem(
          title: '民勤丰收：感受农业高质量发展',
          date: '2022-10-09',
        ),
        NewsItem(
          title: '正是秋收好光景 天麻金薯采收忙',
          date: '2022-10-09',
        ),
        NewsItem(
          title: '玉米“入库”“上楼” 农民储粮不愁',
          date: '2022-11-07',
        ),
        NewsItem(
          title: '第四届“文昌椰子”品牌建设与发展年会召开',
          date: '2022-11-07',
        ),
        NewsItem(
          title: '乡村游感受丰收喜悦',
          date: '2022-11-07',
        ),
        NewsItem(
          title: '志愿服务传真情 文明实践润乡风',
          date: '2022-11-07',
        ),
        NewsItem(
          title: '米再破“吨粮”助力陇东旱塬夯实粮食安全根基',
          date: '2022-11-07',
        ),
        NewsItem(
          title: '人民日报评论员：让开放为全球发展带来新的光明前程',
          date: '2022-11-07',
        ),
        NewsItem(
          title: '冬小麦和冬油菜播种约九成 秋冬种进展总体顺利',
          date: '2022-11-07',
        ),
        NewsItem(
          title: '秋粮收购工作开局良好进展顺利',
          date: '2022-11-07',
        ),
      ]);
    case NewsType.diseasePrevention:
      return NewsInfo(NewsType.diseasePrevention, title: "疾病防治", newsItems: [
        NewsItem(
          title: '黑龙江省农业农村厅召开全省秋季重大动物疫病防控工作视频会议',
          date: '2022-09-23',
        ),
        NewsItem(
          title: '病虫情报第12期——切实抓好晚稻穗期病虫防治工作',
          date: '2022-09-08',
        ),
        NewsItem(
          title: '娄底召开全市秋冬季动物疫病防控工作会暨重大动物疫病防控应急管理培训班',
          date: '2022-09-07',
        ),
        NewsItem(
          title: '水稻除草新技术  省时省工又省药',
          date: '2022-09-21',
        ),
        NewsItem(
          title: '慈利县召开秋季重大动物疫病防控工作动员会',
          date: '2022-09-21',
        ),
        NewsItem(
          title: '安康市汉阴县平梁镇：做好秋季重大动物疫病防控工作，促进畜牧产业持续健康发展',
          date: '2022-09-19',
        ),
        NewsItem(
          title: '中国动物疫病预防控制中心召开秋冬季重大动物疫病防控工作推进会 ',
          date: '2022-09-17',
        ),
        NewsItem(
          title: '三江街道召开2022年秋季动物重大疫病防控工作部署会议',
          date: '2022-09-17',
        ),
        NewsItem(
          title: '县畜牧水产农机事务中心：我县召开2022年全县秋冬季重大动物疫病防控暨动物防疫技能培训会',
          date: '2022-09-17',
        ),
        NewsItem(
          title: '湖南：持续发力抗旱，夺取粮食丰收',
          date: '2022-09-17',
        ),
        NewsItem(
          title: '切实做好秋季重大动物疫病防控工作，强化非洲猪瘟等重大动物疫病常态化防控，推进重点人畜共患病防控',
          date: '2022-09-17',
        ),
        NewsItem(
          title: '农业农村部：推进动物疫病净化工作',
          date: '2022-09-17',
        ),
        NewsItem(
          title: '我国兽用抗菌药使用减量化行动试点达标养殖场调研分析',
          date: '2022-09-17',
        ),
        NewsItem(
          title: '湖南省水稻病虫害防控效果暨植保贡献率评价试验现场观摩会在醴陵召开',
          date: '2022-09-17',
        ),
        NewsItem(
          title: '顺义区召开2022年秋季重大动物疫病防控工作会',
          date: '2022-09-17',
        ),
        NewsItem(
          title: '湖南省农业农村减排固碳实施方案发布',
          date: '2022-09-17',
        ),
        NewsItem(
          title: '疫情下，再谈大城市的“菜篮子”自给率',
          date: '2022-11-07',
        ),
        NewsItem(
          title: '猪肉价格下跌，更要抓好稳价保供',
          date: '2022-11-07',
        ),
        NewsItem(
          title: '中国特色粮食安全的全景式记录——评纪录片《端牢中国饭碗》',
          date: '2022-11-07',
        ),
        NewsItem(
          title: '河北: 疫情防控与农业生产，考的就是统筹题',
          date: '2022-11-07',
        ),
        NewsItem(
          title: '浙江庆元林菇共育系统被粮农组织认定为全球重要农业文化遗产',
          date: '2022-11-07',
        ),
      ]);
  }
}
