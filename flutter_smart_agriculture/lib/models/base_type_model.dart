import 'menu_model.dart';
import '../utils/Config.dart';

enum BaseType { strawberry, tomato, celeryCabbage, cucumber }

class BaseTypeInfo extends MenuModel {
  final BaseType type;

  final String? name;
  final String? plantDate; //种植日期
  final String? phenodate; //物候期
  final String? harvestingDate; //收获期
  final String? plantedArea; //种植面积
  final String imageAsset; //图片

  BaseTypeInfo(this.type,
      {String title = "",
      this.name,
      this.plantDate,
      this.phenodate,
      this.harvestingDate,
      this.plantedArea,
      this.imageAsset = "/assets/images/app_icon.png"})
      : super(title);
}

List<BaseTypeInfo> getBaseTypeList() {
  List<BaseTypeInfo> list = [];
  if (isGlobalListMoreTest) {
    for (int i = 0; i < 30; i++) {
      for (var value in BaseType.values) {
        list.add(getBaseTypeInfo(value));
      }
    }
  } else {
    for (var value in BaseType.values) {
      list.add(getBaseTypeInfo(value));
    }
  }
  return list;
}

BaseTypeInfo getBaseTypeInfo(BaseType baseType) {
  switch (baseType) {
    case BaseType.strawberry:
      return BaseTypeInfo(
        BaseType.strawberry,
        title: "草莓大棚",
        name: "草莓",
        plantDate: "2023.05.16",
        phenodate: "2023.05.16",
        harvestingDate: "2023.10.01",
        plantedArea: "456.3㎡",
        imageAsset: "assets/images/caomei.webp",
      );
    case BaseType.tomato:
      return BaseTypeInfo(
        BaseType.tomato,
        title: "西红柿大棚",
        name: "西红柿",
        plantDate: "2023.06.16",
        phenodate: "2023.06.16",
        harvestingDate: "2023.11.11",
        plantedArea: "378.5㎡",
        imageAsset: "assets/images/tomato.png",
      );
    case BaseType.celeryCabbage:
      return BaseTypeInfo(
        BaseType.celeryCabbage,
        title: "大白菜大棚",
        name: "大白菜",
        plantDate: "2023.07.07",
        phenodate: "2023.07.08",
        harvestingDate: "2023.09.28",
        plantedArea: "245.2㎡",
        imageAsset: "assets/images/cabbage.jpg",
      );
    case BaseType.cucumber:
      return BaseTypeInfo(
        BaseType.cucumber,
        title: "黄瓜大棚",
        name: "黄瓜",
        plantDate: "2023.08.10",
        phenodate: "2023.08.17",
        harvestingDate: "2023.10.29",
        plantedArea: "165.0㎡",
        imageAsset: "assets/images/cucumber.jpg",
      );
  }
}
