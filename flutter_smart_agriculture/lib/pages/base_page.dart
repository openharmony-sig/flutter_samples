import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:openvalley_smart_agriculture/models/base_type_model.dart';

import '../components/image_tab.dart';

class BasePage extends StatefulWidget {
  const BasePage({super.key});

  @override
  State<StatefulWidget> createState() => _BasePageState();
}

class _BasePageState extends State<BasePage> {
  List<BaseTypeInfo> infoList = getBaseTypeList();

  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color(0xfff3f5f7),
      child: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/home_top_bg.png'),
                fit: BoxFit.cover,
              ),
            ),
            height: 360,
          ),
          Container(
              padding: const EdgeInsets.only(top: 60),
              // decoration: const BoxDecoration(
              //   // gradient: LinearGradient(
              //   //   begin: Alignment.topCenter,
              //   //   end: Alignment.center,
              //   //   colors: [
              //   //     Colors.green,
              //   //     Colors.white,
              //   //   ],
              //   // ),
              //   // color: Color(0xffbed9cb),
              //   // ),
              //   image: DecorationImage(
              //     image: AssetImage('assets/images/bg.png'),
              //     fit: BoxFit.cover,
              //   ),
              // ),
              child: Column(
                children: [
                  Container(
                    padding: const EdgeInsets.only(
                        left: 20, bottom: 30, right: 20, top: 10),
                    alignment: Alignment.topLeft,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Text(
                          "智慧农业",
                          // style: TextStyle(fontSize: 28, color: Colors.white),
                          style: TextStyle(
                              fontSize: 24,
                              fontWeight: FontWeight.w800,
                              color: Color(0xe6000000)),
                        ),
                        Image.asset(
                          'assets/images/ov_logo.png',
                          width: 75,
                          height: 30,
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding:
                          const EdgeInsets.only(bottom: 0, left: 20, right: 20),
                      child: ListView.builder(
                          padding: EdgeInsets.zero,
                          itemCount: infoList.length,
                          itemBuilder: (context, index) {
                            return ImageTab(infoList[index]);
                          }),
                    ),
                  )
                ],
              )),
        ],
      ),
    );
  }
}
