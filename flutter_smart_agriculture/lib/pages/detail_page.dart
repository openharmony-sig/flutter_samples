import 'dart:math';

import 'package:flutter/material.dart';

import '../models/base_type_model.dart';

class DetailPage extends StatefulWidget {
  BaseTypeInfo baseTypeInfo;

  DetailPage(this.baseTypeInfo, {super.key});

  @override
  State<StatefulWidget> createState() => _DetailPageState();
}

class ImageItem extends StatelessWidget {
  String image;
  String date;
  String desc;

  ImageItem(this.image, this.date, this.desc, {super.key});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        children: [
          Image.asset(
            image,
            width: 24,
            height: 24,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 6, bottom: 6),
            child: Text(
              date,
              style: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
          ),
          Text(
            desc,
            style: const TextStyle(fontSize: 14, color: Color(0x66000000),fontWeight: FontWeight.normal),
          ),
        ],
      ),
    );
  }
}

class ProgressItem extends StatelessWidget {
  double progressValue;
  int progressColor;
  String desc;
  String text;

  ProgressItem(this.progressValue, this.progressColor, this.desc, this.text,
      {super.key});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        children: [
          Stack(
            alignment: Alignment.center,
            children: [
              SizedBox(
                width: 48,
                height: 48,
                child: CircularProgressIndicator(
                  value: progressValue,
                  color: Color(progressColor),
                  backgroundColor: const Color(0xfff3f3f3),
                ),
              ),
              Text(
                text,
                style: TextStyle(
                  fontSize: 16,
                  color: Color(progressColor),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 12,
          ),
          Text(
            desc,
            style: const TextStyle(fontSize: 14, color: Color(0x66000000)),
          ),
        ],
      ),
    );
  }
}

class _DetailPageState extends State<DetailPage> {
  @override
  Widget build(BuildContext context) {
    var info = widget.baseTypeInfo;
    return Material(
      color: const Color.fromARGB(255, 243, 245, 247),
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top: 60),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 24, top: 8, bottom: 16),
                child: Row(
                  children: [
                    GestureDetector(
                      child: Image.asset(
                        "assets/images/ic_public_back.png",
                        width: 24,
                        height: 24,
                      ),
                      onTap: () => {Navigator.pop(context)},
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 12.0),
                      child: Text(
                        info.title,
                        style: const TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ),
              ),
              AspectRatio(
                aspectRatio: 16 / 9,
                child: Image.asset(info.imageAsset, fit: BoxFit.cover),
              ),
              const Padding(
                padding: EdgeInsets.only(top: 20, left: 24, bottom: 8),
                child: Text(
                  "作物信息",
                  style: TextStyle(fontSize: 14, color: Color(0x66000000)),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 12, right: 12),
                child: Card(
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(16.0), // 设置圆角半径
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 12, left: 12, right: 12, bottom: 28),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(info.name ?? "",
                            style: const TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold)),
                        const SizedBox(
                          height: 12,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            ImageItem("assets/images/ic_detail0.png",
                                info.plantDate ?? "", "种植日期"),
                            Container(
                              width: 1, // 线的宽度
                              height: 56, // 线的高度，设置为无限高度以填满父容器
                              color: const Color(0x0d000000), // 线的颜色
                            ),
                            ImageItem("assets/images/ic_detail1.png",
                                info.phenodate ?? "", "当季候物期"),
                          ],
                        ),
                        const SizedBox(
                          height: 28,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            ImageItem("assets/images/ic_detail2.png",
                                info.harvestingDate ?? "", "预计收获期"),
                            Container(
                              width: 1, // 线的宽度
                              height: 56, // 线的高度，设置为无限高度以填满父容器
                              color: const Color(0x0d000000), // 线的颜色
                            ),
                            ImageItem("assets/images/ic_detail3.png",
                                info.plantedArea ?? "", "种植面积"),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(top: 20, left: 24, bottom: 8),
                child: Text(
                  "环境检测",
                  style: TextStyle(fontSize: 14, color: Color(0x66000000)),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 12, right: 12, bottom: 32),
                child: Card(
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(16.0), // 设置圆角半径
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 36, top: 24, right: 36, bottom: 24),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ProgressItem(0.7, 0xffe84026, "土壤湿度", "35%"),
                        ProgressItem(0.5, 0xff0a81f7, "空气湿度", "25°C"),
                        ProgressItem(0.25, 0xff0a81f7, "光照强度", "87"),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
