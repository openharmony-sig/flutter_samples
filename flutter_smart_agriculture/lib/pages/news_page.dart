import 'package:flutter/material.dart';
import 'package:openvalley_smart_agriculture/models/news_type_model.dart';

import '../components/base_content.dart';
import '../components/custom_menu_bar.dart';
import '../components/news_list.dart';

class NewsPage extends StatefulWidget {
  const NewsPage({super.key});

  @override
  State<StatefulWidget> createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> {
  List<NewsInfo> infoList = getNewsInfoList();
  int _selectIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.only(top: 60),
        child: Column(
          children: [
            CustomMenuBar(infoList, (index) {
              setState(() {
                _selectIndex = index;
              });
            }),
            Expanded(
              child: NewsList(infoList[_selectIndex]),
            )
          ],
        ),
      ),
    );
  }
}
