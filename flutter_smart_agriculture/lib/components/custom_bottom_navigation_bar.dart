import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

typedef ValueChanged<T> = void Function(T value);

class CustomBottomNavigationBar extends StatefulWidget {
  final ValueChanged<int>? onTap;

  const CustomBottomNavigationBar({super.key, this.onTap});

  @override
  State<CustomBottomNavigationBar> createState() =>
      CustomBottomNavigationBarState();
}

class CustomBottomNavigationBarState extends State<CustomBottomNavigationBar> {
  int _selectIndex = 0;

  void setIndex(int index) {
    setState(() {
      _selectIndex = index;
    });
  }

  List<Widget> _buildTabItems() {
    return [
      _buildTabItem(svg: "assets/images/ic_home.svg", text: '我的基地', index: 0),
      _buildTabItem(svg: "assets/images/ic_cloud.svg", text: '气象查询', index: 1),
      _buildTabItem(svg: "assets/images/ic_new.svg", text: '农业新闻', index: 2),
    ];
  }

  Widget _buildTabItem(
      {String svg = "assets/images/ic_home.svg",
      String text = "home",
      int index = 0}) {
    var isSelected = _selectIndex == index;
    return Expanded(
        flex: 1,
        child: GestureDetector(
            onTap: () {
              log("选择改变：$_selectIndex");
              if (index != _selectIndex) {
                widget.onTap?.call(index);
              }
              setIndex(index);
            },
            child: Container(
              padding: const EdgeInsets.only(top: 15, bottom: 15),
              color: Colors.white,
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SvgPicture.asset(
                      svg,
                      color: isSelected
                          ? const Color(0xff0a81f7)
                          : const Color(0xff999999),
                      height: 32,
                      width: 32,
                    ),
                    const SizedBox(
                      height: 3,
                    ),
                    Text(
                      text,
                      style: TextStyle(
                          color: isSelected
                              ? const Color(0xff0a81f7)
                              : const Color(0xff999999),
                          fontWeight: FontWeight.w500,
                          fontSize: 10),
                    )
                  ],
                ),
              ),
            )));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 5.0,
            offset: Offset(0.0, 3.0),
          ),
        ],
      ),
      child: Row(
        children: _buildTabItems(),
      ),
    );
  }
}
