import 'package:flutter/material.dart';
import 'package:openvalley_smart_agriculture/models/base_type_model.dart';

class BaseInfoTab extends StatefulWidget {
  final BaseType baseType;

  const BaseInfoTab(this.baseType, {super.key});

  @override
  State<StatefulWidget> createState() => _BaseInfoTabState();
}

class _BaseInfoTabState extends State<BaseInfoTab> {
  @override
  Widget build(BuildContext context) {
    BaseTypeInfo info = getBaseTypeInfo(widget.baseType);
    return Container(
      margin: const EdgeInsets.only(bottom: 100, top: 10),
      padding: const EdgeInsets.only(left: 20, right: 20, top: 30, bottom: 30),
      width: 300,
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Image.asset(
                info.imageAsset,
                fit: BoxFit.fill,
                width: 50,
                height: 50,
              ),
              Container(
                padding: const EdgeInsets.only(left: 20),
              ),
              Text(
                "作物类型：${info.name}",
                style: const TextStyle(fontSize: 18, color: Color(0xff666666)),
              )
            ],
          ),
          const Divider(
            height: 12,
            color: Colors.transparent,
          ),
          Text(
            "种植日期:${info.plantDate}",
            style: const TextStyle(fontSize: 18, color: Color(0xff666666)),
          ),
          const Divider(
            height: 12,
            color: Colors.transparent,
          ),
          Text(
            "当前物候期:${info.phenodate}",
            style: const TextStyle(fontSize: 18, color: Color(0xff666666)),
          ),
          const Divider(
            height: 12,
            color: Colors.transparent,
          ),
          Text(
            "预计收获期:${info.harvestingDate}",
            style: const TextStyle(fontSize: 18, color: Color(0xff666666)),
          ),
          const Divider(
            height: 12,
            color: Colors.transparent,
          ),
          Text(
            "种植面积:${info.plantedArea}",
            style: const TextStyle(fontSize: 18, color: Color(0xff666666)),
          ),
          Expanded(child: Container()),
          const Divider(
            thickness: 2,
            color: Color(0xffCCCCCC),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10, bottom: 10, left: 20),
            child: Row(
              children: [
                const Text(
                  "土壤湿度",
                  style: TextStyle(fontSize: 22, color: Color(0xff666666)),
                ),
                const SizedBox(
                  width: 20,
                ),
                Stack(
                  alignment: Alignment.center,
                  children: const [
                    SizedBox(
                      width: 50,
                      height: 50,
                      child: CircularProgressIndicator(
                        value: 100,
                        color: Colors.red,
                      ),
                    ),
                    Text(
                      '15%',
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          const Divider(
            thickness: 2,
            color: Color(0xffCCCCCC),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10, bottom: 10, left: 20),
            child: Row(
              children: [
                const Text(
                  "空气温度",
                  style: TextStyle(fontSize: 22, color: Color(0xff666666)),
                ),
                const SizedBox(
                  width: 20,
                ),
                Stack(
                  alignment: Alignment.center,
                  children: const [
                    SizedBox(
                      width: 50,
                      height: 50,
                      child: CircularProgressIndicator(
                        value: 100,
                        color: Colors.grey,
                      ),
                    ),
                    Text(
                      '26°C',
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          const Divider(
            thickness: 2,
            color: Color(0xffCCCCCC),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10, bottom: 10, left: 20),
            child: Row(
              children: [
                const Text(
                  "光照强度",
                  style: TextStyle(fontSize: 22, color: Color(0xff666666)),
                ),
                const SizedBox(
                  width: 20,
                ),
                Stack(
                  alignment: Alignment.center,
                  children: const [
                    SizedBox(
                      width: 50,
                      height: 50,
                      child: CircularProgressIndicator(
                        value: 100,
                        color: Colors.grey,
                      ),
                    ),
                    Text(
                      '22',
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
