import 'package:flutter/material.dart';
import 'package:openvalley_smart_agriculture/components/base_info_tab.dart';
import 'package:openvalley_smart_agriculture/models/base_type_model.dart';

class BaseContent extends StatefulWidget {
  final BaseType baseType;

  const BaseContent(this.baseType, {super.key});

  @override
  State<StatefulWidget> createState() => _BaseContentState();
}

class _BaseContentState extends State<BaseContent> {
  @override
  Widget build(BuildContext context) {
    BaseTypeInfo info = getBaseTypeInfo(widget.baseType);
    return Container(
      color: const Color(0xffF9F9F9),
      child: Row(
        children: [
          Expanded(
              child: Container(
            padding: const EdgeInsets.only(
                left: 100, right: 100, top: 30, bottom: 50),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Expanded(
                      child: AspectRatio(
                        aspectRatio: 3 / 2,
                        child: Image.asset(
                          info.imageAsset,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ],
                ),
                Image.asset("assets/images/base_direction.png")
              ],
            ),
          )),
          BaseInfoTab(widget.baseType),
        ],
      ),
    );
  }
}
