import 'package:flutter/material.dart';

class LoadingPage extends StatelessWidget {
  const LoadingPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const [
        CircularProgressIndicator(),
        Padding(
          padding: EdgeInsets.all(24.0),
          child: Text(
            "数据加载中...",
            style: TextStyle(color: Color(0xff666666), fontSize: 16),
          ),
        )
      ],
    );
  }
}
