import 'package:flutter/cupertino.dart';

class LoadingFailed extends StatelessWidget {
  const LoadingFailed({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset("assets/images/ic_network.png"),
        const Padding(
          padding: EdgeInsets.all(24.0),
          child: Text(
            "网络已断开，请连接网络",
            style: TextStyle(color: Color(0xff666666), fontSize: 16),
          ),
        )
      ],
    );
  }
}
