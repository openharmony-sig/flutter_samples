import 'package:flutter/material.dart';

import '../models/news_type_model.dart';
import '../utils/Config.dart';

class NewsList extends StatefulWidget {
  NewsInfo newsInfo;

  NewsList(this.newsInfo, {super.key});

  @override
  State<StatefulWidget> createState() => _NewsListState();
}

class _NewsListState extends State<NewsList> {
  int _selectedIndex = -1;

  // 是否显示更多，用于测试
  bool showMoreTest = isGlobalListMoreTest;

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        padding: const EdgeInsets.only(left: 20, right: 20),
        itemBuilder: (BuildContext context, int index) {
          int trueIndex = index % (widget.newsInfo.newsItems?.length ?? 0);
          NewsItem? newsItem = widget.newsInfo.newsItems?[trueIndex];
          bool isSelected = _selectedIndex == index;
          return GestureDetector(
            child: Container(
              color: isSelected
                  ? const Color.fromARGB(255, 230, 230, 230)
                  : Colors.white,
              padding: const EdgeInsets.only(
                  left: 10, right: 10, top: 20, bottom: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "${newsItem?.title}",
                    style: const TextStyle(
                        fontSize: 16,
                        color: Color(0xff000000),
                        fontWeight: FontWeight.bold),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        newsItem?.location == null
                            ? Container()
                            : Padding(
                                padding: const EdgeInsets.only(right: 8),
                                child: Text("${newsItem?.location}",
                                    style: const TextStyle(
                                        fontSize: 10,
                                        color: Color(0x66000000))),
                              ),
                        Padding(
                          padding: const EdgeInsets.only(top: 2),
                          child: Text(
                            "${newsItem?.date}",
                            style: const TextStyle(
                                fontSize: 10, color: Color(0x66000000)),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            onTap: () {
              if (index != _selectedIndex) {
                setState(() {
                  _selectedIndex = index;
                });
              }
            },
          );
        },
        separatorBuilder: (BuildContext context, int index) => const Divider(
              thickness: 1,
              height: 1,
            ),
        itemCount: showMoreTest
            ? (widget.newsInfo.newsItems?.length ?? 0) * 30
            : (widget.newsInfo.newsItems?.length ?? 0));
  }
}
