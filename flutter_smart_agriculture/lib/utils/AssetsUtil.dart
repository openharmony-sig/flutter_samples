import 'package:flutter/services.dart' show rootBundle;

Future<String> loadAsset() async {
  return await rootBundle.loadString('assets/my_text_file.txt');
}

String getWeatherAsset(String? type) {
  String image = "";
  switch (type) {
    case "多云":
      image = "assets/images/weather_cloudy.png";
      break;
    case "浓云":
      image = "assets/images/weather_congestus.png";
      break;
    case "雾":
      image = "assets/images/weather_fog.png";
      break;
    case "冰雹":
      image = "assets/images/weather_hail.png";
      break;
    case "霾":
      image = "assets/images/weather_haze.png";
      break;
    case "雨":
    case "中雨":
      image = "assets/images/weather_hrain.png";
      break;
    case "大雪":
    case "中雪":
    case "小雪":
      image = "assets/images/weather_hsnow.png";
      break;
    case "阴":
      image = "assets/images/weather_overcast.png";
      break;
    case "暴雨":
    case "大雨":
      image = "assets/images/weather_rainstorm.png";
      break;
    case "龙卷风":
      image = "assets/images/weather_sandstorm.png";
      break;
    case "阵雨":
      image = "assets/images/weather_shower.png";
      break;
    case "雨夹雪":
      image = "assets/images/weather_snow.png";
      break;
    case "小雨":
      image = "assets/images/weather_srain.png";
      break;
    case "晴":
    case "少云":
      image = "assets/images/weather_sunday.png";
      break;
  }
  return image;
}

