import 'dart:convert' as convert;

import 'package:openvalley_smart_agriculture/models/json/weather_24_hours_api.dart';
import 'package:openvalley_smart_agriculture/models/json/weather_7_days_api.dart';
import 'package:openvalley_smart_agriculture/models/json/weather_now_api.dart';

import '../models/json/suggestion_api.dart';
import '../models/json/weather_7_days_suggestion_api.dart';

T? fromJson<T>(String json) {
  var jsonObj = convert.jsonDecode(json) as Map<String, dynamic>;
  switch (T) {
    case SuggestionApi:
      return SuggestionApi.fromJson(jsonObj) as T;
    case Weather7DaysApi:
      return Weather7DaysApi.fromJson(jsonObj) as T;
    case Weather24HoursApi:
      return Weather24HoursApi.fromJson(jsonObj) as T;
    case Weather7DaysSuggestionApi:
      return Weather7DaysSuggestionApi.fromJson(jsonObj) as T;
    case WeatherNowApi:
      return WeatherNowApi.fromJson(jsonObj) as T;
  }
  return null;
}
