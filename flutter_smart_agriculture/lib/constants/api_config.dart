import 'package:openvalley_smart_agriculture/models/json/suggestion_api.dart';
import 'package:openvalley_smart_agriculture/models/json/weather_now_api.dart';

import '../models/json/weather_24_hours_api.dart';
import '../models/json/weather_7_days_api.dart';
import '../models/json/weather_7_days_suggestion_api.dart';

class ApiConfig {
  static const String API_HOST = "api.seniverse.com";

  static const String PATH_SUGGESTION =
      "api.seniverse.com/v3/life/suggestion.json";
}

class ConfigData {
  String apiName = "";
  bool isHttps = true;
  String host = "";
  String path = "";
  Map<String, dynamic> defaultParams = {};

  ConfigData(
      this.apiName, this.isHttps, this.host, this.path, this.defaultParams);
}

ConfigData getConfigData<T>() {
  switch (T) {
    case SuggestionApi:
      return ConfigData("SuggestionApi", true, "api.seniverse.com",
          "/v3/life/suggestion.json", {
        "key": "SIaMsEE6HsgnlG03a",
        "location": "changsha",
        "language": "zh-Hans",
        "days": "1",
      });
    case Weather7DaysApi:
      return ConfigData(
          "Weather7DaysApi",
          true,
          "devapi.qweather.com",
          "/v7/weather/7d",
          {"location": "101250105", "key": "0d1d0b45323047918219d86832733403"});
    case WeatherNowApi:
      return ConfigData(
          "WeatherNowApi",
          true,
          "devapi.qweather.com",
          "/v7/weather/now",
          {"location": "101250105", "key": "0d1d0b45323047918219d86832733403"});
    case Weather7DaysSuggestionApi:
      return ConfigData("Weather7DaysSuggestionApi", false,
          "t.weather.sojson.com", "/api/weather/city/101250101", {});
    case Weather24HoursApi:
      return ConfigData("Weather24HoursApi", true, "devapi.qweather.com",
          "/v7/grid-weather/24h", {
        "location": "112.94,28.23",
        "key": "0d1d0b45323047918219d86832733403",
      });
  }
  return ConfigData("undefined", true, "", "", {});
}
