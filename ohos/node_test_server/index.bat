@echo off
chcp 65001
cd ./bats
echo 测试sqflite库
start sqfliteTest.bat
timeout /t 130

echo 测试pathProvide库
start pathProvideTest.bat
timeout /t 130

echo 测试keyChannel库
start keyChannelTest.bat
timeout /t 130

echo 生成表格
start createxlsx.bat
