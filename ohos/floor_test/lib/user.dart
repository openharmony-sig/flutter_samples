import 'package:floor/floor.dart';

@entity
class User {
  @primaryKey
  final int? id; // 如果你希望数据库自动生成ID，可以将其设置为null

  final String name;
  final int age;

  User(this.id, this.name, this.age);
}
