import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';

final log = Logger('ExampleLogger');

class ExampleTestPage extends StatefulWidget {
  ExampleTestPage({required this.title});

  String title;

  @override
  State<StatefulWidget> createState() => _ExampleTestPageState();

}

class _ExampleTestPageState extends State<ExampleTestPage> {
  String loggingContent = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Logging_test'),
      ),
      body: Center(
        child: Column(
          children: [
            Container(
                height: 100,
                margin: const EdgeInsets.only(bottom: 20),
                child: SingleChildScrollView(child: Text(
                  loggingContent,
                  textAlign: TextAlign.center,
                  style: const TextStyle(fontSize: 20),
                ))),
            Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      _commonButton(context, 'Level()', () {
                        Level level = const Level('level', 1);
                        loggingContent =
                        'Level.name: ${level.name}\nLevel.value: ${level
                            .value}';
                        setState(() {});
                      }),
                      _commonButton(context, 'Level.compareTo()', () {
                        Level levelA = const Level('level', 1);
                        Level levelB = const Level('levell', 1);
                        Level levelC = const Level('level', 2);
                        Level levelD = const Level('llevel', 1);
                        loggingContent =
                        'levelA.compareTo(levelB): ${levelA.compareTo(
                            levelB)}\n'
                            'levelA.compareTo(levelC): ${levelA.compareTo(
                            levelC)}\n'
                            'levelA.compareTo(levelD): ${levelA.compareTo(
                            levelD)}';
                        setState(() {});
                      }),
                      _commonButton(context, 'Logger()', () {
                        Logger logger = Logger('logger');
                        loggingContent =
                        'Logger(): $logger\nLogger().name: ${logger.name}';
                        setState(() {});
                      }),
                      _commonButton(context, 'Logger.detached()', () {
                        Logger logger = Logger.detached('logger');
                        loggingContent =
                        'Logger.detached(): $logger\nLogger.detached().name: ${logger
                            .name}';
                        setState(() {});
                      }),
                      _commonButton(context, 'Logger().children', () {
                        Logger logger = Logger('parent.children');
                        loggingContent = 'Logger().name: ${logger
                            .name}\nLogger().children: ${logger.children}';
                        setState(() {});
                      }),
                      _commonButton(context, 'Logger().fullName', () {
                        Logger logger = Logger('one.two.three');
                        loggingContent = 'Logger().name: ${logger
                            .name}\nLogger().fullName: ${logger.fullName}';
                        setState(() {});
                      }),
                      _commonButton(context, 'Logger().level', () {
                        Logger logger = Logger('level');
                        loggingContent =
                        'Logger().name: ${logger.name}\nLogger().level: ${logger
                            .level}';
                        setState(() {});
                      }),
                      _commonButton(context, 'Logger().name', () {
                        Logger logger = Logger('name');
                        loggingContent = 'Logger().name: ${logger.name}';
                        setState(() {});
                      }),
                      _commonButton(context, 'Logger.root.onLevelChanged', () {
                        final root = Logger.root;
                        recordStackTraceAtLevel = Level.OFF;
                        root.clearListeners();
                        final levels = <Level?>[];
                        root.level = Level.ALL;
                        root.onLevelChanged.listen(levels.add);
                        root.level = Level.SEVERE;
                        root.level = Level.WARNING;
                        loggingContent = levels.toString();
                        setState(() {});
                      }),
                      _commonButton(context, 'Logger.root.onRecord', () {
                        final root = Logger.root;
                        recordStackTraceAtLevel = Level.OFF;
                        root.clearListeners();
                        final records = <LogRecord>[];
                        root.onRecord.listen(records.add);
                        root.severe('hello');
                        root.warning('hello');
                        loggingContent = '$records\n'
                            '${records[0].stackTrace}\n'
                            '${records[1].stackTrace}';
                        setState(() {});
                      }),
                      _commonButton(context, 'Logger().parent', () {
                        Logger logger = Logger('parent.children');
                        loggingContent = 'Logger().name: ${logger
                            .name}\nLogger().parent.name: ${logger.parent
                            ?.name}';
                        setState(() {});
                      }),
                      _commonButton(
                          context, 'Logger.root.clearListeners()', () {
                        final root = Logger.root;
                        hierarchicalLoggingEnabled = true;
                        root.level = Level.INFO;
                        root.clearListeners();
                        hierarchicalLoggingEnabled = false;
                        root.level = Level.INFO;
                        loggingContent =
                        'Logger.root.clearListeners()之后Logger.root.level: ${root
                            .level}';
                        setState(() {});
                      }),
                      _commonButton(context, 'Logger.root.config()', () {
                        final root = Logger.root;
                        hierarchicalLoggingEnabled = true;
                        root.level = Level.INFO;
                        root.clearListeners();
                        hierarchicalLoggingEnabled = false;
                        root.level = Level.INFO;
                        root.level = Level.ALL;
                        final rootMessages = [];
                        root.onRecord.listen((record) {
                          rootMessages.add(
                              '${record.level}: ${record.message}');
                        });
                        root.config('4');
                        loggingContent = rootMessages.toString();
                        setState(() {});
                      }),
                      _commonButton(context, 'Logger.root.fine()', () {
                        final root = Logger.root;
                        hierarchicalLoggingEnabled = true;
                        root.level = Level.INFO;
                        root.clearListeners();
                        hierarchicalLoggingEnabled = false;
                        root.level = Level.INFO;
                        root.level = Level.ALL;
                        final rootMessages = [];
                        root.onRecord.listen((record) {
                          rootMessages.add(
                              '${record.level}: ${record.message}');
                        });
                        root.fine('3');
                        loggingContent = rootMessages.toString();
                        setState(() {});
                      }),
                      _commonButton(context, 'Logger.root.finer()', () {
                        final root = Logger.root;
                        hierarchicalLoggingEnabled = true;
                        root.level = Level.INFO;
                        root.clearListeners();
                        hierarchicalLoggingEnabled = false;
                        root.level = Level.INFO;
                        root.level = Level.ALL;
                        final rootMessages = [];
                        root.onRecord.listen((record) {
                          rootMessages.add(
                              '${record.level}: ${record.message}');
                        });
                        root.finer('2');
                        loggingContent = rootMessages.toString();
                        setState(() {});
                      }),
                      _commonButton(context, 'Logger.root.finest()', () {
                        final root = Logger.root;
                        hierarchicalLoggingEnabled = true;
                        root.level = Level.INFO;
                        root.clearListeners();
                        hierarchicalLoggingEnabled = false;
                        root.level = Level.INFO;
                        root.level = Level.ALL;
                        final rootMessages = [];
                        root.onRecord.listen((record) {
                          rootMessages.add(
                              '${record.level}: ${record.message}');
                        });
                        root.finest('1');
                        loggingContent = rootMessages.toString();
                        setState(() {});
                      }),
                      _commonButton(context, 'Logger.root.info()', () {
                        final root = Logger.root;
                        hierarchicalLoggingEnabled = true;
                        root.level = Level.INFO;
                        root.clearListeners();
                        hierarchicalLoggingEnabled = false;
                        root.level = Level.INFO;
                        root.level = Level.ALL;
                        final rootMessages = [];
                        root.onRecord.listen((record) {
                          rootMessages.add(
                              '${record.level}: ${record.message}');
                        });
                        root.info('5');
                        loggingContent = rootMessages.toString();
                        setState(() {});
                      }),
                      _commonButton(context, 'Logger.root.isLoggable', () {
                        final root = Logger.root;
                        hierarchicalLoggingEnabled = true;
                        root.level = Level.INFO;
                        hierarchicalLoggingEnabled = false;
                        root.level = Level.INFO;
                        hierarchicalLoggingEnabled = true;
                        root.level = Level.SEVERE;
                        loggingContent =
                        'Logger.root.isLoggable(Level.SHOUT): ${root.isLoggable(
                            Level.SHOUT)}\n'
                            'Logger.root.isLoggable(Level.SEVERE): ${root
                            .isLoggable(Level.SEVERE)}\n'
                            'Logger.root.isLoggable(Level.WARNING): ${root
                            .isLoggable(Level.WARNING)}\n';
                        setState(() {});
                      }),
                      _commonButton(context, 'Logger.root.log', () {
                        Logger.root.level = Level.INFO;
                        final records = <LogRecord>[];
                        final sub = Logger.root.onRecord.listen(records.add);
                        try {
                          throw UnsupportedError('test exception');
                        } catch (error, stack) {
                          Logger.root.log(Level.SEVERE, 'severe', error, stack);
                          Logger.root.warning('warning', error, stack);
                        }
                        Logger.root.log(Level.SHOUT, 'shout');
                        sub.cancel();
                        loggingContent = records.toString();
                        setState(() {});
                      }),
                      _commonButton(context, 'Logger.root.severe()', () {
                        final root = Logger.root;
                        hierarchicalLoggingEnabled = true;
                        root.level = Level.INFO;
                        root.clearListeners();
                        hierarchicalLoggingEnabled = false;
                        root.level = Level.INFO;
                        root.level = Level.ALL;
                        final rootMessages = [];
                        root.onRecord.listen((record) {
                          rootMessages.add(
                              '${record.level}: ${record.message}');
                        });
                        root.severe('7');
                        loggingContent = rootMessages.toString();
                        setState(() {});
                      }),
                      _commonButton(context, 'Logger.root.shout()', () {
                        final root = Logger.root;
                        hierarchicalLoggingEnabled = true;
                        root.level = Level.INFO;
                        root.clearListeners();
                        hierarchicalLoggingEnabled = false;
                        root.level = Level.INFO;
                        root.level = Level.ALL;
                        final rootMessages = [];
                        root.onRecord.listen((record) {
                          rootMessages.add(
                              '${record.level}: ${record.message}');
                        });
                        root.shout('8');
                        loggingContent = rootMessages.toString();
                        setState(() {});
                      }),
                      _commonButton(context, 'Logger.root.warning()', () {
                        final root = Logger.root;
                        hierarchicalLoggingEnabled = true;
                        root.level = Level.INFO;
                        root.clearListeners();
                        hierarchicalLoggingEnabled = false;
                        root.level = Level.INFO;
                        root.level = Level.ALL;
                        final rootMessages = [];
                        root.onRecord.listen((record) {
                          rootMessages.add(
                              '${record.level}: ${record.message}');
                        });
                        root.warning('6');
                        loggingContent = rootMessages.toString();
                        setState(() {});
                      }),
                      _commonButton(context, 'Logger.attachedLoggers', () {
                        loggingContent =
                        'Logger.attachedLoggers: ${Logger.attachedLoggers}';
                        setState(() {});
                      }),
                      _commonButton(context, 'Logger.root', () {
                        final root = Logger.root;
                        loggingContent =
                        'Logger.root: $root\nLogger.root.name: ${root
                            .name}\nLogger.root.level: ${root.level}';
                        setState(() {});
                      }),
                      _commonButton(context, 'LogRecord()', () {
                        LogRecord logRecord = LogRecord(
                            Level.ALL, 'This is logRecord', 'logRecord');
                        loggingContent = 'LogRecord(): $logRecord';
                        setState(() {});
                      }),
                      _commonButton(context, 'LogRecord().error', () {
                        LogRecord logRecord = LogRecord(
                            Level.ALL, 'This is logRecord', 'logRecord');
                        loggingContent =
                        'LogRecord().error: ${logRecord.error}';
                        setState(() {});
                      }),
                      _commonButton(context, 'LogRecord().level', () {
                        LogRecord logRecord = LogRecord(
                            Level.ALL, 'This is logRecord', 'logRecord');
                        loggingContent =
                        'LogRecord().level: ${logRecord.level}';
                        setState(() {});
                      }),
                      _commonButton(context, 'LogRecord().loggerName', () {
                        LogRecord logRecord = LogRecord(
                            Level.ALL, 'This is logRecord', 'logRecord');
                        loggingContent =
                        'LogRecord().loggerName: ${logRecord.loggerName}';
                        setState(() {});
                      }),
                      _commonButton(context, 'LogRecord().message', () {
                        LogRecord logRecord = LogRecord(
                            Level.ALL, 'This is logRecord', 'logRecord');
                        loggingContent =
                        'LogRecord().message: ${logRecord.message}';
                        setState(() {});
                      }),
                      _commonButton(context, 'LogRecord().object', () {
                        LogRecord logRecord = LogRecord(
                            Level.ALL, 'This is logRecord', 'logRecord');
                        loggingContent =
                        'LogRecord().object: ${logRecord.object}';
                        setState(() {});
                      }),
                      _commonButton(context, 'LogRecord().sequenceNumber', () {
                        LogRecord logRecord = LogRecord(
                            Level.ALL, 'This is logRecord', 'logRecord');
                        loggingContent =
                        'LogRecord().sequenceNumber: ${logRecord
                            .sequenceNumber}';
                        setState(() {});
                      }),
                      _commonButton(context, 'LogRecord().stackTrace', () {
                        LogRecord logRecord = LogRecord(
                            Level.ALL, 'This is logRecord', 'logRecord');
                        loggingContent =
                        'LogRecord().stackTrace: ${logRecord.stackTrace}';
                        setState(() {});
                      }),
                      _commonButton(context, 'LogRecord().time', () {
                        LogRecord logRecord = LogRecord(
                            Level.ALL, 'This is logRecord', 'logRecord');
                        loggingContent = 'LogRecord().time: ${logRecord.time}';
                        setState(() {});
                      }),
                      _commonButton(context, 'LogRecord().zone', () {
                        LogRecord logRecord = LogRecord(
                            Level.ALL, 'This is logRecord', 'logRecord');
                        loggingContent = 'LogRecord().zone: ${logRecord.zone}';
                        setState(() {});
                      }),
                      _commonButton(context, 'hierarchicalLoggingEnabled', () {
                        loggingContent =
                        'hierarchicalLoggingEnabled值为：$hierarchicalLoggingEnabled';
                        setState(() {});
                      }),
                      _commonButton(context, 'recordStackTraceAtLevel', () {
                        loggingContent =
                        'recordStackTraceAtLevel值为：$recordStackTraceAtLevel';
                        setState(() {});
                      }),
                    ],
                  ),
                )
            )
          ],
        ),
      ),
    );
  }
}

Widget _commonButton(BuildContext context, String title, Function() onTap) {
  return Container(
      width: MediaQuery.of(context).size.width * 0.7,
      margin: const EdgeInsets.only(bottom: 10),
      child: MaterialButton(
        onPressed: onTap,
        color: Colors.blue,
        child: Text(title),
      )
  );
}