import 'package:flutter/material.dart';

import 'base_page.dart';
import 'test_route.dart';

/// 基础app框架
class TestModelApp extends StatefulWidget {
  TestModelApp({super.key, required this.appName, required this.data}) {
    GlobalData.appName = appName;
  }

  /// 测试包名称
  final String appName;

  /// 路由数据
  final TestRoute data;

  @override
  State<StatefulWidget> createState() => TestModelState();
}

class TestModelState extends State<TestModelApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: widget.appName,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.blue),
        appBarTheme: const AppBarTheme(backgroundColor: Colors.blue),
        primarySwatch: Colors.blue,
        useMaterial3: true,
      ),
      routes: widget.data.routes,
      initialRoute: '/',
    );
  }
}
