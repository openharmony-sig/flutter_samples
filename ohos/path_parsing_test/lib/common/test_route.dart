import 'package:flutter/cupertino.dart';

import 'base_page.dart';

class MainItem {
  /// Main item.
  MainItem(this.title, this.description, {this.route});

  /// Title.
  String title;

  /// Description.
  String description;

  /// Page route.
  String? route;
}

class TestRoute {
  TestRoute({required Map<String, WidgetBuilder> routes, required this.items}) {
    if (routes.containsKey('/')) {
      throw Exception('不允许传入 / 路由');
    }

    this.routes.addAll({
      '/': (BuildContext context) => BasePage(data: items),
    });
    this.routes.addAll(routes);
  }

  Map<String, WidgetBuilder> routes = <String, WidgetBuilder>{};

  List<MainItem> items = [];
}
