import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../common/test_page.dart';

/// Item widget.
class ItemWidget extends StatefulWidget {
  /// Item widget.
  const ItemWidget(
      {required this.item,
      required this.index,
      required this.getGroupRange,
      required this.runGroup,
      required this.onTap,
      this.summary,
      Key? key})
      : super(key: key);

  /// item summary.
  final String? summary;

  /// item data.
  final Item item;

  /// 当前下标
  final int index;

  /// 获取对应的组信息
  final GroupRange Function() getGroupRange;

  /// 获取对应的组信息
  final void Function(int start, int end) runGroup;

  /// Action when pressed (typically run).
  final void Function(Item item) onTap;

  @override
  ItemWidgetState createState() => ItemWidgetState();
}

class ItemWidgetState extends State<ItemWidget> {
  @override
  Widget build(BuildContext context) {
    IconData? icon;
    Color? color;

    switch (widget.item.state) {
      case ItemState.none:
        icon = Icons.arrow_forward_ios;
        break;
      case ItemState.running:
        icon = Icons.more_horiz;
        break;
      case ItemState.success:
        icon = Icons.check;
        color = Colors.green;
        break;
      case ItemState.failure:
        icon = Icons.close;
        color = Colors.red;
        break;
    }

    final Widget listTile = ListTile(
        leading: SizedBox(
            child: IconButton(
          icon: Icon(icon, color: color),
          onPressed: null,
        )),
        title: Text(widget.item.name),
        subtitle: widget.summary != null ? Text(widget.summary!) : null,
        onTap: () {
          widget.onTap(widget.item);
        });

    final data = widget.getGroupRange();

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (data.groupName.isNotEmpty && data.startIndex == widget.index)
          GestureDetector(
            onTap: () {},
            child: Container(
              height: 35,
              decoration: BoxDecoration(color: CupertinoColors.extraLightBackgroundGray),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                      child: Text(
                    '测试组: ${data.groupName}',
                    style: TextStyle(fontSize: 18),
                    overflow: TextOverflow.ellipsis,
                  )),
                  // FilledButton(
                  //     onPressed: () => widget.runGroup(data.startIndex, data.startIndex),
                  //     child: Text(
                  //       '整组测试',
                  //       style: TextStyle(fontSize: 16),
                  //     ))
                ],
              ),
            ),
          ),
        Container(
          margin: data.groupName.isNotEmpty && data.startIndex == widget.index ? EdgeInsets.only(bottom: 10) : null,
          decoration: BoxDecoration(
            border: data.groupName.isNotEmpty && data.endIndex == widget.index
                ? Border(bottom: BorderSide(color: Colors.grey))
                : null,
          ),
          child: Padding(
            padding: data.groupName.isNotEmpty && data.startIndex <= widget.index && data.endIndex >= widget.index
                ? EdgeInsets.only(left: 35)
                : EdgeInsets.zero,
            child: listTile,
          ),
        )
      ],
    );
  }
}
