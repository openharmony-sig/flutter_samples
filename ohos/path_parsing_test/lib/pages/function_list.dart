import 'package:flutter/material.dart';

import 'example.dart';

class FunctionList extends StatelessWidget {
  final functionList = ["解析和处理SVG路径数据"];
  final pages = <Widget>[
    PathParsingPage(),
  ];

  FunctionList({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('path_parsing功能演示'),
      ),
      body: ListView.builder(
        itemCount: functionList.length,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (_) {
                return pages[index];
              }));
            },
            child: ListTile(
              title: Text(functionList[index]),
            ),
          );
        },
      ),
    );
  }
}
