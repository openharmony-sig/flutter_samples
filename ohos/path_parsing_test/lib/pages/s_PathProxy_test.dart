import 'package:flutter/cupertino.dart';
import 'package:path_parsing/path_parsing.dart';
import '../common/test_page.dart';
import 'example.dart';

class SPathProxyTestPage extends TestPage {
  SPathProxyTestPage(String title, {Key? key}) : super(title, key: key) {
    PathPrinter path = PathPrinter();
//-------------构造方法----------------------
    group('Constructors', () {
      test('PathProxy()', () {
        expect('PathProxy为抽象类，不能直接创建', null);
      });
    });

//-------------属性----------------------
    group('Properties', () {
      test('.hashCode → int', () {
        expect(path.hashCode, null);
      });
      test('.runtimeType → Type', () {
        expect(path.runtimeType, null);
      });
    });

//-------------对象方法----------------------
    group('Methods', () {
      test('close() → void', () {
        path.close();
        expect('', null);
      });

      test('cubicTo(double x1, double y1, double x2, double y2, double x3, double y3) → void', () {
        path.cubicTo(1, 2, 3, 4, 5, 6);
        expect('', null);
      });

      test(' lineTo(double x, double y) → void', () {
        path.lineTo(
          1,
          2,
        );
        expect('', null);
      });

      test('moveTo(double x, double y) → void', () {
        path.moveTo(
          1,
          2,
        );
        expect('', null);
      });

      test('toString() → String', () {
        expect(path.toString.runtimeType, null);
        expect(path.toString, null);
      });
    });
  }
}
