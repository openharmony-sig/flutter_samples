import 'package:flutter/cupertino.dart';
import 'package:path_parsing/path_parsing.dart';
import '../common/test_page.dart';

class SPathSegmentDataTestPage extends TestPage {
  SPathSegmentDataTestPage(String title, {Key? key}) : super(title, key: key) {
    String svg = 'M50 0 Q70 20, 50 40, Q30 20, 50 0 M50 0 Q70 -20, 50 -40, Q30 -20, 50 0 M50 40 V100';
    final SvgPathStringSource parser = SvgPathStringSource(svg);
    PathSegmentData pathSD = parser.parseSegments().first;

//-------------构造方法----------------------
    group('Constructors', () {
      test('PathSegmentData()', () {
        expect(PathSegmentData().runtimeType, null);
        expect(PathSegmentData(), null);
      });
    });

//-------------属性----------------------
    group('Properties', () {
      test('.arcAngle ↔ double', () {
        expect(pathSD.arcAngle.runtimeType, null);
        expect(pathSD.arcAngle, null);
      });

      test('.arcLarge ↔ bool', () {
        expect(pathSD.arcLarge.runtimeType, null);
        expect(pathSD.arcLarge, null);
      });

      test('.arcRadii → _PathOffset', () {
        expect(pathSD.arcRadii.runtimeType, null);
        expect(pathSD.arcRadii, null);
      });

      test('.arcSweep ↔ bool', () {
        expect(pathSD.arcSweep.runtimeType, null);
        expect(pathSD.arcSweep, null);
      });

      test('.command ↔ SvgPathSegType', () {
        expect(pathSD.command.runtimeType, null);
        expect(pathSD.command, null);
      });

      test('.largeArcFlag → bool', () {
        expect(pathSD.largeArcFlag.runtimeType, null);
        expect(pathSD.largeArcFlag, null);
      });

      test('.point1 ↔ _PathOffset', () {
        expect(pathSD.point1.runtimeType, null);
        expect(pathSD.point1, null);
      });

      test('.point2 ↔ _PathOffset', () {
        expect(pathSD.point2.runtimeType, null);
        expect(pathSD.point2, null);
      });

      test('.r1 → double', () {
        expect(pathSD.r1.runtimeType, null);
        expect(pathSD.r1, null);
      });

      test('.r2 → double', () {
        expect(pathSD.r2.runtimeType, null);
        expect(pathSD.r2, null);
      });

      test('.sweepFlag → bool', () {
        expect(pathSD.sweepFlag.runtimeType, null);
        expect(pathSD.sweepFlag, null);
      });

      test('.targetPoint ↔ _PathOffset', () {
        expect(pathSD.targetPoint.runtimeType, null);
        expect(pathSD.targetPoint, null);
      });

      test('.x → double', () {
        expect(pathSD.x.runtimeType, null);
        expect(pathSD.x, null);
      });

      test('.x1 → double', () {
        expect(pathSD.x1.runtimeType, null);
        expect(pathSD.x1, null);
      });

      test('.x2 → double', () {
        expect(pathSD.x2.runtimeType, null);
        expect(pathSD.x2, null);
      });

      test('.y → double', () {
        expect(pathSD.y.runtimeType, null);
        expect(pathSD.y, null);
      });

      test('.y1 → double', () {
        expect(pathSD.y1.runtimeType, null);
        expect(pathSD.y1, null);
      });

      test('.y2 → double', () {
        expect(pathSD.y2.runtimeType, null);
        expect(pathSD.y2, null);
      });
    });

//-------------对象方法----------------------
    group('Methods', () {
      test('toString() → String', () {
        expect(pathSD.toString(), null);
      });
    });
  }
}
