import 'package:flutter/material.dart';
import 'package:path_parsing/path_parsing.dart';

class PathParsingPage extends StatelessWidget {
  const PathParsingPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Path Parsing Example'),
      ),
      body: Center(
        child: CustomPaint(
          size: const Size(40, 40),
          painter: TopDecoration(context),
        ),
      ),
    );
  }
}

class PathPrinter extends PathProxy {
  Path path = Path();
  @override
  void close() {
    path.close();
  }

  @override
  void moveTo(double x, double y) {
    path.moveTo(x, y);
  }

  @override
  void cubicTo(double x1, double y1, double x2, double y2, double x3, double y3) {
    path.cubicTo(x1, y1, x2, y2, x3, y3);
  }

  @override
  void lineTo(double x, double y) {
    path.lineTo(x, y);
  }
}

class TopDecoration extends CustomPainter {
  final BuildContext context;
  TopDecoration(this.context);

  void showAlertDialog(BuildContext context, String text) {
    showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            content: SingleChildScrollView(
              child: Column(
                children: [
                  Text(
                    text,
                    maxLines: 100,
                    overflow: TextOverflow.ellipsis,
                  )
                ],
              ),
            ),
            actions: <Widget>[
              MaterialButton(
                child: const Text('确定'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = Colors.blue
      ..style = PaintingStyle.stroke
      ..strokeWidth = 2.0;
    PathPrinter path = PathPrinter();

    const String pathData = 'M22.1595 3.80852C19.6789 1.35254 16.3807 -4.80966e-07 12.8727 '
        '-4.80966e-07C9.36452 -4.80966e-07 6.06642 1.35254 3.58579 '
        '3.80852C1.77297 5.60333 0.53896 7.8599 0.0171889 10.3343C-0.0738999 '
        '10.7666 0.206109 11.1901 0.64265 11.2803C1.07908 11.3706 1.50711 11.0934 '
        '1.5982 10.661C2.05552 8.49195 3.13775 6.51338 4.72783 4.9391C9.21893 '
        '0.492838 16.5262 0.492728 21.0173 4.9391C25.5082 9.38548 25.5082 16.6202 '
        '21.0173 21.0667C16.5265 25.5132 9.21893 25.5133 4.72805 21.0669C3.17644 '
        '19.5307 2.10538 17.6035 1.63081 15.4937C1.53386 15.0627 1.10252 14.7908 '
        '0.66697 14.887C0.231645 14.983 -0.0427272 15.4103 0.0542205 '
        '15.8413C0.595668 18.2481 1.81686 20.4461 3.5859 22.1976C6.14623 '
        '24.7325 9.50955 26 12.8727 26C16.236 26 19.5991 24.7326 22.1595 '
        '22.1976C27.2802 17.1277 27.2802 8.87841 22.1595 3.80852Z';

    // const String pathData = 'M50 0 Q70 20, 50 40, Q30 20, 50 0 M50 0 Q70 -20, 50 -40, Q30 -20, 50 0 M50 40 V100';
    // const String pathData = 'M7.6,7C0.4';
    try {
      writeSvgPathDataToPath(pathData, path);
      canvas.drawPath(path.path, paint);
    } catch (e) {
      print('路径错误:${e.toString()}');
      // CustomDialog(title: '发生错误', message: e.toString());
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return false;
  }
}

class CustomDialog extends StatelessWidget {
  final String title;
  final String message;

  CustomDialog({required this.title, required this.message});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: _buildDialogContent(context),
    );
  }

  Widget _buildDialogContent(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(16),
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,
            offset: const Offset(0.0, 10.0),
          ),
        ],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 16),
          Text(
            message,
            style: TextStyle(fontSize: 16),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 24),
          ElevatedButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text('关闭'),
          ),
        ],
      ),
    );
  }
}
