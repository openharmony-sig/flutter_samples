import 'package:flutter/cupertino.dart';
import 'package:path_parsing/path_parsing.dart';
import '../common/test_page.dart';
import 'example.dart';

class SSvgPathStringSourceTestPage extends TestPage {
  SSvgPathStringSourceTestPage(String title, {Key? key}) : super(title, key: key) {
    String svg = 'M50 0 Q70 20, 50 40, Q30 20, 50 0 M50 0 Q70 -20, 50 -40, Q30 -20, 50 0 M50 40 V100';
    final SvgPathStringSource pathStringSource = SvgPathStringSource(svg);
//-------------构造方法----------------------
    group('Constructors', () {
      test('SvgPathStringSource(String _string)', () {
        expect(
            SvgPathStringSource('M50 0 Q70 20, 50 40, Q30 20, 50 0 M50 0 Q70 -20, 50 -40, Q30 -20, 50 0 M50 40 V100').runtimeType,
            null);
        expect(SvgPathStringSource('M50 0 Q70 20, 50 40, Q30 20, 50 0 M50 0 Q70 -20, 50 -40, Q30 -20, 50 0 M50 40 V100'), null);
      });
    });

//-------------属性---------------
    group('Properties', () {
      test('.hasMoreData → bool', () {
        expect(pathStringSource.hasMoreData.runtimeType, null);
        expect(pathStringSource.hasMoreData, null);
      });

      test('.hashCode → int', () {
        expect(pathStringSource.hashCode.runtimeType, null);
        expect(pathStringSource.hashCode, null);
      });
    });

//-------------对象方法----------------------
    group('Methods', () {
      test('parseSegment() → PathSegmentData', () {
        String svg = 'M50 0 Q70 20, 50 40, Q30 20, 50 0 M50 0 Q70 -20, 50 -40, Q30 -20, 50 0 M50 40 V100';
        final SvgPathStringSource pathStringSource = SvgPathStringSource(svg);
        expect(pathStringSource.parseSegment(), null);
      });
      test('parseSegments() → Iterable<PathSegmentData>', () {
        String svg = 'M50 0 Q70 20, 50 40, Q30 20, 50 0 M50 0 Q70 -20, 50 -40, Q30 -20, 50 0 M50 40 V100';
        final SvgPathStringSource pathStringSource = SvgPathStringSource(svg);
        expect(pathStringSource.parseSegments(), null);
      });

      test('toString() → String', () {
        expect(pathStringSource.toString.runtimeType, null);
        expect(pathStringSource.toString, null);
      });
    });
  }
}
