import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:scrollview_demo/sliver_flexible_header.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const CustomScrollViewPage(),
    );
  }
}

class CustomScrollViewPage extends StatefulWidget {
  const CustomScrollViewPage({super.key});

  @override
  State<CustomScrollViewPage> createState() => _CustomScrollViewPageState();
}

class _CustomScrollViewPageState extends State<CustomScrollViewPage> {
  Widget buildSliverList([int count = 5]) {
    return SliverFixedExtentList(
      itemExtent: 50,
      delegate: SliverChildBuilderDelegate(
        (context, index) {
          return Text('$index');
        },
        childCount: count,
      ),
    );
  }

  bool _onNotification(ScrollUpdateNotification notification) {
    print('==========dragDetail = ${notification.dragDetails}  '
        '==========scrollDelta = ${notification.scrollDelta}  ');
    return false;
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return NotificationListener<ScrollUpdateNotification>(
      onNotification: _onNotification,
      child: CustomScrollView(
        // 指定widget.physics
        physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
        slivers: [
          SliverFlexibleHeader(
            visibleExtent: 200,
            builder: (context, availableHeight, direction) {
              return GestureDetector(
                onTap: () => debugPrint('tap'),
                child: LayoutBuilder(builder: (context, cons) {
                  return Image(
                    image: const AssetImage("images/1.png"),
                    width: 50.0,
                    height: availableHeight,
                    alignment: Alignment.bottomCenter,
                    fit: BoxFit.cover,
                  );
                }),
              );
            },
          ),

          SliverFixedExtentList(
            itemExtent: 50.0,
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                return Container(
                  alignment: Alignment.center,
                  color: Colors.lightBlue[100 * (index % 10)],
                  child: Text('$index'),
                );
              },
              childCount: 100,
            ),
          ),
        ],
      ),
    );
  }
}
