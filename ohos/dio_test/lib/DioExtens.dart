import 'package:dio/dio.dart';
import 'package:dio/io.dart';

Dio getDio() {
  Dio dio = Dio();
  (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate = (client) {
    client.badCertificateCallback = (cert, host, post) {
      return true;
    };
  };

  return dio;
}