import 'package:dio/dio.dart';

import '../common/test_page.dart';

class MimetypeTestPage extends TestPage {
  MimetypeTestPage(super.title){
    test('Transformer.isJsonMimeType(application/json)', () {
      expect(Transformer.isJsonMimeType('application/json'), 'isTrue');
    });

    test('Transformer.isJsonMimeType(text/json)', () {
      expect(Transformer.isJsonMimeType('text/json'), 'isTrue');
    });

    test('Transformer.isJsonMimeType(application/vnd.example.com+json)', () {
      expect(
        Transformer.isJsonMimeType('application/vnd.example.com+json'),
        'isTrue',
      );
    });
  }

}