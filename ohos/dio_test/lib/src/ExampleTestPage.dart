import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import '../DioExtens.dart';


class ExampleTestPage extends StatefulWidget {
  const ExampleTestPage({key, required this.title});

  final String title;

  @override
  State<ExampleTestPage> createState() => _ExampleTestPageState();
}

double currentProgress =0.0;

class _ExampleTestPageState extends State<ExampleTestPage> {

  final dio = getDio();

  String dioGetContent = '';
  String dioPostContent = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Center(
        child: ListView(
          children: <Widget>[
            Text('dio.get: $dioGetContent'),
            MaterialButton(
              color: Colors.blue,
              onPressed: () async {
                dioGetContent = (await dio.get('https://www.baidu.com/')).headers.toString();
                setState(() {});
              },
              child: Text('Dio.get'),
            ),
            Text('dio.post: $dioPostContent'),
            MaterialButton(
              color: Colors.blue,
              onPressed: () async {
                // 设置请求头
                Map<String, String> headers = {
                  'Content-Type': 'application/json',
                };
                // 设置请求体
                Map<String, dynamic> data = {
                  'title': 'post请求测试',
                  'data': 'dio.post测试，正常post请求成功返回值'
                };
                Response response = await dio.post(
                  'https://httpbin.org/post',
                  data: data,
                  options: Options(headers: headers),
                );

                // 处理响应数据
                if (response.statusCode == 200) {
                  dioPostContent = response.data.toString();
                } else {
                  print('请求失败');
                }

                setState(() {});
              },
              child: Text('Dio.post'),
            ),
          ],
        ),
      ),
    );
  }
}
