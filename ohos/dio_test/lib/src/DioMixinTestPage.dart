import 'package:dio/dio.dart';

import '../common/test_page.dart';

class DioMixinTestPage extends TestPage {
  DioMixinTestPage(super.title){
    test('DioMixin.assureResponse<int?>()', () {
      final requestOptions = RequestOptions(path: '');
      final untypedResponse = Response<dynamic>(
        requestOptions: requestOptions,
        data: null,
      );
      expect(untypedResponse is Response<int>, 'isFalse');

      final typedResponse = DioMixin.assureResponse<int>(
        untypedResponse,
        requestOptions,
      );
      expect(typedResponse.data, 'isNull');
    });

    test('_TestDioMixin().download(a, b)', () {
      expect(() => _TestDioMixin().download('a', 'b'), '',
      );
    });
  }

}

class _TestDioMixin with DioMixin implements Dio {}