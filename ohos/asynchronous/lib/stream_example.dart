import 'dart:async';

import 'package:flutter/material.dart';

class StreamExample extends StatefulWidget {
  const StreamExample({Key? key}) : super(key: key);

  @override
  _StreamExampleState createState() => _StreamExampleState();
}

/*
  在这个示例中，我们创建了一个 Stream，每秒钟发出一个新的整数。我们使用 StreamSubscription 来手动监听这个 Stream，并在数据到达时更新 UI。

  步骤：
    1、创建一个 Stream，模拟每秒钟发送一个整数。
    2、手动订阅这个 Stream，并在数据到达时更新 UI。
 */
class _StreamExampleState extends State<StreamExample> {
  late StreamSubscription<int> _subscription;
  String _status = "Waiting...";

  // 创建一个 Stream，模拟每秒钟发送一个整数
  Stream<int> numberStream() async* {
    int count = 0;
    while (true) {
      await Future.delayed(const Duration(seconds: 1)); // 模拟延迟
      yield count++; // 每秒钟发出一个新的整数
    }
  }

  @override
  void initState() {
    super.initState();

    // 手动订阅 Stream
    _subscription = numberStream().listen(
      (data) {
        setState(() {
          _status = "Number: $data"; // 更新状态显示最新的数字
        });
      },
      onError: (error) {
        setState(() {
          _status = "Error: $error"; // 错误处理
        });
      },
      onDone: () {
        setState(() {
          _status = "Stream closed"; // Stream 完成
        });
      },
    );
  }

  @override
  void dispose() {
    // 取消订阅
    _subscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Stream Example')),
      body: Center(
        child: Text(_status, style: const TextStyle(fontSize: 48)),
      ),
    );
  }
}
