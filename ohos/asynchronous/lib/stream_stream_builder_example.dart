import 'package:flutter/material.dart';

class StreamStreamBuilderExample extends StatefulWidget {
  const StreamStreamBuilderExample({Key? key}) : super(key: key);

  @override
  _StreamStreamBuilderExampleState createState() => _StreamStreamBuilderExampleState();
}

/*
  假设我们有一个应用程序，它接收一系列的数据（例如，模拟的传感器数据），并通过 Stream 持续传递这些数据项。在这个例子中，我们将模拟一个持续发出整数的 Stream，并通过 StreamBuilder 在 Flutter UI 中显示这些数据。

  步骤：
    1、创建一个 Stream，它每隔一秒钟发出一个整数。
    2、使用 StreamBuilder 在 Flutter UI 中动态展示接收到的数据。
 */
class _StreamStreamBuilderExampleState extends State<StreamStreamBuilderExample> {
  // 创建一个 Stream，模拟每秒钟发送一个整数
  Stream<int> numberStream() async* {
    int count = 0;
    while (true) {
      await Future.delayed(const Duration(seconds: 1)); // 模拟延迟
      yield count++; // 每秒钟发出一个新的整数
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Stream与StreamBuilder的结合使用')),
      body: StreamBuilder<int>(
        stream: numberStream(), // 监听 Stream
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            // 等待 Stream 数据
            return const Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError) {
            // 错误处理
            return Center(child: Text('Error: ${snapshot.error}'));
          } else if (!snapshot.hasData) {
            // 数据为空
            return const Center(child: Text('No data'));
          } else {
            // 数据更新，显示 Stream 中的最新整数
            return Center(
              child: Text('Number: ${snapshot.data}', style: const TextStyle(fontSize: 48)),
            );
          }
        },
      ),
    );
  }
}
