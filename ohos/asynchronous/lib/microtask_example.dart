import 'dart:async';

import 'package:flutter/material.dart';

class MicrotaskExample extends StatefulWidget {
  const MicrotaskExample({Key? key}) : super(key: key);

  @override
  _MicrotaskExampleState createState() => _MicrotaskExampleState();
}

/*
  Microtasks 是 Dart 语言中的一种特殊任务类型,它们会在当前事件循环的末尾执行。与普通的异步任务(Future)不同,Microtasks 具有以下特点:

  1、优先级高于 Future: Microtasks 的执行优先级高于 Future,也就是说,即使有一个正在执行的 Future,如果有 Microtasks 需要执行,它们也会先于 Future 执行。
  2、立即执行: 当 Microtasks 被添加到 event queue 中时,它们会立即执行,而不会等待当前事件循环结束。
  3、同步执行: 与 Future 不同,Microtasks 是同步执行的,它们会在当前事件循环的末尾,一个接一个地执行完毕。
 */
class _MicrotaskExampleState extends State<MicrotaskExample> {
  String _status = "Waiting...";
//按钮点击事件
  void _onPressed() {
    _status = "Waiting...";
    _status = '$_status\n Start';
    // 添加一个 Microtask
    scheduleMicrotask(() {
      _status = '$_status\n Microtask 1';
    });

    // 添加一个 Future
    Future(() {
      setState(() {
        _status = '$_status\n Future';
      });
    });

    // 添加另一个 Microtask
    scheduleMicrotask(() {
      _status = '$_status\n Microtask 2';
    });
    _status = '$_status\n End';
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Microtask Example')),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(_status),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
                _onPressed();
                // // 点击按钮后，使用一个 Microtask 更新状态
                // scheduleMicrotask(() {
                //   setState(() {
                //     _status = "Microtask triggered by button!";
                //   });
                // });
              },
              child: const Text('Trigger Microtask'),
            ),
          ],
        ),
      ),
    );
  }
}
