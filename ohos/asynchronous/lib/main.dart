import 'package:flutter/material.dart';

import 'async_await.dart';
import 'completer_future_example.dart';
import 'future_builder_example.dart';
import 'future_wait_example.dart';
import 'microtask_example.dart';
import 'stream_example.dart';
import 'stream_stream_builder_example.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  HomePage({super.key});

  final titleList = [
    "async 和 await",
    "FutureBuilder",
    "Future.wait",
    "Completer自定义 Future",
    "microtask",
    "Stream",
    "Stream与StreamBuilder的结合使用"
  ];

  final widgetList = [
    const AsyncAwaitExample(),
    const FutureBuilderExample(),
    const FutureWaitExample(),
    const CompleterFutureExample(),
    const MicrotaskExample(),
    const StreamExample(),
    const StreamStreamBuilderExample()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("异步编程"),
      ),
      body: ListView.builder(
        itemCount: titleList.length,
        itemBuilder: (context, index) {
          return Card(
            margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
            elevation: 5,
            shadowColor: Colors.grey.withOpacity(0.2), // 设置阴影颜色
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10), // 圆角效果
            ),
            child: ListTile(
              contentPadding: const EdgeInsets.all(15),
              title: Text(titleList[index]),
              onTap: () {
                // 点击时跳转到不同的页面
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => widgetList[index]),
                );
              },
            ),
          );
        },
      ),
    );
  }
}
