import 'package:flutter/material.dart';

class FutureWaitExample extends StatefulWidget {
  const FutureWaitExample({Key? key}) : super(key: key);

  @override
  _FutureWaitExampleState createState() => _FutureWaitExampleState();
}

/*
  假设我们有三个独立的网络请求，分别获取用户的个人信息、账户信息和通知信息，最后在所有信息都获取完后一起显示。

  步骤：
    1、模拟三个不同的异步操作，每个操作返回不同的数据。
    2、使用 Future.wait 等待所有 Future 完成，统一处理结果。
    3、在 FutureBuilder 中展示所有请求的结果。
 */
class _FutureWaitExampleState extends State<FutureWaitExample> {
  // 模拟获取用户个人信息的异步方法
  Future<String> fetchUserProfile() async {
    await Future.delayed(const Duration(seconds: 2));
    return "John Doe, 28 years old";
  }

// 模拟获取账户信息的异步方法
  Future<String> fetchAccountInfo() async {
    await Future.delayed(const Duration(seconds: 2));
    return "Account: #123456789";
  }

// 模拟获取通知信息的异步方法
  Future<String> fetchNotifications() async {
    await Future.delayed(const Duration(seconds: 2));
    return "You have 5 new notifications.";
  }

  // 使用 Future.wait 并行执行多个异步操作
  Future<List<String>> fetchAllData() async {
    try {
      // 使用 Future.wait 等待所有异步操作完成
      var results = await Future.wait([
        fetchUserProfile(), // 获取用户信息
        fetchAccountInfo(), // 获取账户信息
        fetchNotifications(), // 获取通知信息
      ]);
      return results; // 返回所有结果
    } catch (e) {
      throw Exception('Failed to fetch data: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Future.wait Example')),
      body: FutureBuilder<List<String>>(
        future: fetchAllData(), // 调用 Future.wait 获取所有数据
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            // 数据加载中
            return const Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError) {
            // 错误处理
            return Center(child: Text('Error: ${snapshot.error}'));
          } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
            // 数据为空
            return const Center(child: Text('No data available.'));
          } else {
            // 数据加载成功
            List<String> results = snapshot.data!;
            return ListView(
              children: [
                ListTile(title: Text('User Profile: ${results[0]}')),
                ListTile(title: Text('Account Info: ${results[1]}')),
                ListTile(title: Text('Notifications: ${results[2]}')),
              ],
            );
          }
        },
      ),
    );
  }
}
