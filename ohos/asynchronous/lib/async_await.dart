import 'package:flutter/material.dart';

class AsyncAwaitExample extends StatefulWidget {
  const AsyncAwaitExample({Key? key}) : super(key: key);

  @override
  _AsyncAwaitExampleState createState() => _AsyncAwaitExampleState();
}

class _AsyncAwaitExampleState extends State<AsyncAwaitExample> {
  List<Student> _students = []; // 存储学生信息的列表

  @override
  void initState() {
    super.initState();
    // 调用异步函数并更新 UI
    fetchStudentsData();
  }

  // 模拟一个异步网络请求，返回学生数据（30个学生）
  Future<List<Student>> fetchStudentsFromNetwork() async {
    await Future.delayed(const Duration(seconds: 2)); // 模拟网络延迟

    // 模拟返回的学生信息（30个学生）
    List<Map<String, dynamic>> studentsJson = List.generate(30, (index) {
      return {
        'name': 'Student ${index + 1}',
        'age': 18 + (index % 5), // 随机年龄范围：18-22
        'grade': 'Grade ${['A', 'B', 'C', 'D', 'F'][index % 5]}', // 随机成绩
      };
    });

    // 将 JSON 转化为 Student 对象并返回
    return studentsJson.map((json) => Student.fromJson(json)).toList();
  }

  // 使用 async 和 await 获取数据并更新 UI
  Future<void> fetchStudentsData() async {
    try {
      List<Student> students = await fetchStudentsFromNetwork(); // 等待学生信息的返回
      setState(() {
        _students = students; // 更新学生列表
      });
    } catch (e) {
      setState(() {
        _students = []; // 错误发生时清空学生列表
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Student List Example')),
      body: _students.isEmpty
          ? const Center(child: CircularProgressIndicator()) // 如果列表为空，显示加载指示器
          : ListView.builder(
              itemCount: _students.length, // 列表项数量
              itemBuilder: (context, index) {
                final student = _students[index];
                return Card(
                  margin: const EdgeInsets.all(8.0),
                  elevation: 5,
                  shadowColor: Colors.grey.withOpacity(0.2), // 设置阴影颜色
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10), // 圆角效果
                  ),
                  child: ListTile(
                    title: Text(student.name),
                    subtitle: Text('Age: ${student.age}\nGrade: ${student.grade}'),
                  ),
                );
              },
            ),
    );
  }
}

// 学生模型类
class Student {
  final String name;
  final int age;
  final String grade;

  Student({required this.name, required this.age, required this.grade});

  // 从 JSON 数据构造 Student 对象
  factory Student.fromJson(Map<String, dynamic> json) {
    return Student(
      name: json['name'],
      age: json['age'],
      grade: json['grade'],
    );
  }
}
