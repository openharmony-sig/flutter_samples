import 'package:flutter/material.dart';
import 'package:tuple_test/src/ExamplePage.dart';
import 'package:tuple_test/src/TupleTestPage.dart';

import 'common/test_model_app.dart';
import 'common/test_route.dart';

void main() {
  final app = [
    MainItem('example_test', const ExamplePage(title: 'example_test')),
    MainItem('tuple_test', TupleTestPage('tuple_test')),
  ];

  runApp(TestModelApp(
      appName: 'tuple',
      data: app));
}