import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChatB extends StatelessWidget {
  const ChatB({
    Key? key,
    required this.text,
  }) : super(key: key);
  final String text;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(10.0, 2, 6.0, 2,),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Flexible(
            child: DecoratedBox(
              decoration: BoxDecoration(
                color: const Color.fromARGB(255, 115, 179, 117),
                borderRadius: BorderRadius.circular(16),
              ),
              child: Padding(
                padding: const EdgeInsets.all(12),
                child: Text(
                  text,
                  style: const TextStyle(
                    fontSize: 14,
                    color: Color.fromARGB(255, 48, 14, 43),
                  ),
                ),
              ),
            ),
          ),
          const Padding(
            padding: EdgeInsets.fromLTRB(2, 0, 2, 0),
          ),
          const Flexible(
            child: CircleAvatar(
                radius: 25,
                backgroundColor: Colors.white,
                backgroundImage: AssetImage(
                  "assets/images/chat_b.png",
                )
            ),
          ),
        ],
      ),
    );
  }
}