

import 'package:flutter/material.dart';
import 'package:testchat/pages/chat_page.dart';
import 'dart:io';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "test chat",
        theme: ThemeData(primaryColor: Colors.blue),
        home: const Scaffold(
                body: ChatPage(),
        ));
  }
}
