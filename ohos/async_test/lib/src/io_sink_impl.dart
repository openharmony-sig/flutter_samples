@Deprecated('Tests deprecated functionality')
library io_sink_impl;

import 'dart:io';

import 'package:async/async.dart';

/// This class isn't used, it's just used to verify that [IOSinkBase] produces a
/// valid implementation of [IOSink].
class IOSinkImpl extends IOSinkBase implements IOSink {
  @override
  void onAdd(List<int> data) {}

  @override
  void onError(Object error, [StackTrace? stackTrace]) {}

  @override
  void onClose() {}

  @override
  Future<void> onFlush() => Future.value();
}
