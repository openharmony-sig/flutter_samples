import 'dart:typed_data';

import 'package:path_drawing_test/src/path_to_image.dart';

import '../common/test_page.dart';


class RenderPathTestPage extends TestPage {
  RenderPathTestPage(super.title) {
    test('Path rendering matches golden files', () async {
      for (int i = 0; i < renderingPaths.length; i++) {
        final Uint8List bytes = await getPathPngBytes(renderingPaths[i]);
        expect(bytes, null);
      }
    });
  }

}