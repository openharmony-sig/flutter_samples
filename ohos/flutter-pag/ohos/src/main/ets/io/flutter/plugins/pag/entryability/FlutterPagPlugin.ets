import { PAGFile } from '@tencent/libpag';
import  { FlutterPlugin,
  FlutterPluginBinding } from '@ohos/flutter_ohos/src/main/ets/embedding/engine/plugins/FlutterPlugin';
import StandardMessageCodec from '@ohos/flutter_ohos/src/main/ets/plugin/common/StandardMessageCodec';
import { PagFactory } from './PagFactory';
import MethodChannel, {
  MethodCallHandler,
  MethodResult
} from '@ohos/flutter_ohos/src/main/ets/plugin/common/MethodChannel';
import MethodCall from '@ohos/flutter_ohos/src/main/ets/plugin/common/MethodCall';
import StandardMethodCodec from '@ohos/flutter_ohos/src/main/ets/plugin/common/StandardMethodCodec';
import fs from '@ohos.file.fs';
import common from '@ohos.app.ability.common';
import emitter from '@ohos.events.emitter';

AppStorage.setOrCreate('urlList', [])

interface pagConfig {
  textureId: number|undefined,
  isUsed: boolean,
  assetName?: string,
  url: string|ArrayBuffer,
  type: string,
  autoPlay: boolean,
  initProgress: number,
  repeatCount: number
}


// 原生接口
let _nativeInit: String = "initPag";
let _getSize: String = "getSize";
let _nativeRelease: String = "release";
let _nativeStart: String = "start";
let _nativeStop: String = "stop";
let _nativePause:String = "pause";
let _nativeSetProgress:String = "setProgress";
let _nativeGetPointLayer:String = "getLayersUnderPoint";

enum enumsActions {
  'start' = "onAnimationStart",
  'stop' = "onAnimationEnd",
  'pause' =  "onAnimationCancel",
  'repeat'= "onAnimationRepeat",
  'update'= "onAnimationUpdate"
}
export class FlutterPagPlugin implements FlutterPlugin, MethodCallHandler {
  private methodChannel: MethodChannel | null = null;
  private binding: FlutterPluginBinding | null = null;
  getUniqueClassName(): string {
    return 'FlutterPagPlugin';
  }

  onAttachedToEngine(binding: FlutterPluginBinding): void {
    this.binding = binding;
    this.methodChannel = new MethodChannel(binding.getBinaryMessenger(), `flutter_pag_plugin`, StandardMethodCodec.INSTANCE);
    this.methodChannel.setMethodCallHandler(this);
    binding.getPlatformViewRegistry()?.
    registerViewFactory('flutter_pag_plugin', new PagFactory(binding.getBinaryMessenger(), StandardMessageCodec.INSTANCE));
    this.addEventlisener()
  }

  onMethodCall(call: MethodCall, result: MethodResult): void {
    // 接受Dart侧发来的消息
    let method: string = call.method;
    switch (method) {
      case _nativeInit:
        this.initPag(call, result)
        break;
      case _nativeStart:
      case _nativeStop:
      case _nativePause:
      case _nativeSetProgress:
      case _nativeRelease:
        this.setState(method, call);
        result.success('')
        break;
      case _nativeGetPointLayer:
        this.setState(method, call, result);
        break;
      default:
        result.notImplemented();
        break;
    }
  }
  async initPag(call: MethodCall, result: MethodResult) {
    let assetName: string = call.argument('assetName');
    let bytes: Uint8Array = call.argument('bytesData');
    let url: string = call.argument('url');
    let repeatCount: number = call.argument('repeatCount');
    let initProgress: number = call.argument('initProgress');
    let autoPlay: boolean = call.argument('autoPlay');
    let context = getContext(this) as common.UIAbilityContext;
    let dir = context.filesDir;
    const textureId = this.binding?.getTextureRegistry().getTextureId();
    let filePath: string = dir + `pag_${textureId}.pag`;
    let type = url != null ? 'url' : bytes != null ? 'bytes' : 'assetName'
    let data = url != null ? url : bytes != null ? bytes : assetName;

    let _urlList: SubscribedAbstractProperty<pagConfig[]> = AppStorage.link('urlList');
    const urlList = _urlList.get()
    urlList.push({
      url: data,
      type: type,
      autoPlay: autoPlay,
      initProgress: initProgress,
      repeatCount: repeatCount,
      textureId: textureId,
      isUsed: false,
    })
    _urlList.set(urlList);

    let file = await this.getFileDetail(type, data);
    if (file) {
      result.success({
        'width': file.width(),
        'height': file.height(),
        'textureId': textureId
      });
    }
  }

  async getFileDetail(type: string, value: string|ArrayBuffer): Promise<PAGFile | null>  {
    if (type == 'url') {
      return await PAGFile.LoadFromPathAsync(value as string);
    } else if (type == 'bytes') {
      return await PAGFile.LoadFromBytes(new Uint8Array(value as ArrayBuffer).buffer);
    }
    let manager = getContext(this).resourceManager;
    return await PAGFile.LoadFromAssets(manager, 'flutter_assets/' + value as string);
  }

  setState(state: string, call:MethodCall, result?: MethodResult) {
    const textureId = this.getTextureId(call)
    const progress: number|undefined = call.argument('progress')
    const x: number|undefined = call.argument('x')
    const y: number|undefined = call.argument('y')
    let eventData: emitter.EventData = {
      data: {
        'type': 'action',
        "pagPlayState": state,
        'progress': progress,
        'x': x,
        'y': y,
      }
    }
    let innerEvent: emitter.InnerEvent = { eventId: textureId, }
    emitter.emit(innerEvent, eventData)

    let innerEventOn: emitter.InnerEvent = { eventId: textureId }
    emitter.on(innerEventOn, data => {
      if (data?.data?.type === _nativeGetPointLayer) {
        if (data?.data.list) {
          result?.success(data?.data.list);
        }
      }
    })
  }

  getTextureId(call:MethodCall):number {
    return call.argument('textureId');
  }
  addEventlisener() {
    let innerEvent: emitter.InnerEvent = { eventId: 0 }
    emitter.on(innerEvent, data => {
      if (data?.data?.type === 'callback') {
        this.methodChannel?.invokeMethod('PAGCallback', {
          textureId: data?.data?.textureId,
          PAGEvent: data?.data?.PAGEvent,
        })
      }
    })
  }

  onDetachedFromEngine(binding: FlutterPluginBinding): void {}
}