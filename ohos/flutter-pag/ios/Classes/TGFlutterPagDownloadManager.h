#ifndef PLATFORM_VIEW_OHOS_NAPI_H
#define PLATFORM_VIEW_OHOS_NAPI_H
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
 Pag文件下载工具
 */
@interface TGFlutterPagDownloadManager : NSObject

///下载pag文件
+(void)download:(NSString *)urlStr completionHandler:(void (^)(NSData * data, NSError * error))handler;

@end

NS_ASSUME_NONNULL_END
#endif
