# flutter_pag_plugin_example

Demonstrates how to utilize the flutter_pag_plugin effectively.

## Getting Started

对于初学者，我们提供了一些Flutter项目启动资源：

- [实验：编写你的第一个Flutter应用](https://flutter.dev/docs/get-started/codelab)
- [食谱：实用的Flutter示例](https://flutter.dev/docs/cookbook)

要开始使用Flutter，可以查阅我们的
[在线文档](https://flutter.dev/docs)，其中提供了教程，
示例，移动开发指导，以及完整的API参考。
