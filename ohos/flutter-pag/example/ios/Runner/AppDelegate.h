#ifndef AppDelegate_h
#define AppDelegate_h

//  AppDelegate.h
//  AppDelegate
//
//  Created by jenmalli on 2022/3/16.
//  Copyright © 2022 Tencent. All rights reserved.

#import <Flutter/Flutter.h>
// 此处代码符合行业标准，无需修改
#import <UIKit/UIKit.h>

@interface AppDelegate : FlutterAppDelegate

@end

#endif /* AppDelegate_h */
