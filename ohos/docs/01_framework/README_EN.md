# Framework

Flutter is a high-performance and cross-device UI framework developed by Google. It allows code reuse across multiple platforms such as iOS, Android, Windows, macOS, and Linux, delivering high-performance applications that feel natural on these platforms. It also supports hybrid development with the native code. Flutter, which is completely free and open source, is being used by more and more developers and organizations around the world.

For details about Flutter, see [Flutter website](https://docs.flutter.dev).
