# ohos Code Development

## Determining Whether the Current Platform is ohos in Dart Code

```dart
import 'package:flutter/foundation.dart';

bool isOhos() {
  return defaultTargetPlatform == TargetPlatform.ohos;
}
```


## Execution of flutter run, flutter build har and flutter attach Fail if Platform.isOhos Exists in the Code
Symptom:
**Platform.isOhos** exists in the Flutter code, as shown in the following:
```
if (Platform.isAndroid || Platform.isOhos) {
  print("test");
}
```
When the engine product of the server is depended without specifying the local engine product, the **flutter run**, **flutter build har** and **flutter attach** commands fail to be executed.
Error log:
![](../media/08/code1.png)

Solution:
Change **Platform.isOhos** to **defaultTargetPlatform == TargetPlatform.ohos**.


## Native OpenHarmony Applications Fail to Obtain the Image Resources from Flutter
Question:<br>When a plugin is used, the object **binding: FlutterPluginBinding** is returned. A native OpenHarmony application cannot use **binding.getFlutterAssets().getAssetFilePathByName('xxxx')** of this object to obtain the image resources from the Flutter library while it can directly use **Image(this.img)** to obtain on the OpenHarmony platform. Is there any other way to obtain the resource?

Answer:<br>**binding.getFlutterAssets().getAssetFilePathByName('xxxx')** is used to obtain the resource path. To obtain native image resources, see the code as follows:
![](../media/08/code2_EN.png)
```
import { image } from '@kit.ImageKit';
@Component
export struct DemoComponent {
  @Prop params: Params
  viewManager: DemoView = this.params.platformView as DemoView
  image?: string
  @State imageSource:image.ImageSource|null=null

  async aboutToAppear() {
    let args: HashMap<string, object | string> = this.viewManager.args as HashMap<string, object>
    this.image = args.get('src') as string
    let rmg = DemoPluginAssetPlugin.binding.getApplicationContext().  resourceManager;
    let rawfile = await rmg.getRawFileContent("flutter_assets/${this.image}");
    let buffer = rawfile.buffer.slice(0);
    this.imageSource = image.createImageSource(buffer);
  }

  build() {
    Column(){
      if(this.imageSource){
        Image(this.imageSource.createPixelMapSync())
      }
    }
  }
  
  // aboutToAppear(): void {
  // let args: HashMap<string, object | string> = this.viewManager.args as   HashMap<string, object>
  // this.image = args.get('src') as string
  // }
  
  // build() {
  // // Todo: critical point
  // // Image(this.image)
  // Image(DemoPluginAssetPlugin.binding.getFlutterAssets().getAssetFilePathByName  (this.image))
  // // Image(DemoPluginAssetPlugin.binding.getFlutterAssets().  getAssetFilePathBySubpath(this.image))
  // }
}
```
Question:<br>Why the **build** method is directly triggered when I add a breakpoint to **let rawfile = await rmg.getRawFileContent("flutter_assets/"+this.image );** and continue to execute the breakpoint?

Answer:<br>**let rawfile = await rmg.getRawFileContent("flutter_assets/"+this.image );** is a time-consuming operation. During debugging, the remaining code of the current method is not executed until the time-consuming operation returns a result. The **build** method is for rendering only.

Reference:
[demo](https://onebox.huawei.com/p/b3e5806b9ce3683c8c13b237ec319294)

Execute files in **demo_plugin_asset\example\ohos** to display pages. Images are displayed in the Flutter image display page while no image is displayed in the native image display page. Related code is stored in **demo_plugin_asset\example\lib\main.dart** and **demo_plugin_asset\ohos\src\main\ets\components\plugin\DemoPluginAssetPlugin.ets**.
