# 开发package

可参考 [开发纯Dart的packages](https://flutter.cn/docs/packages-and-plugins/developing-packages#dart)

## 1. 创建 package

```sh
flutter create --template=package hello
```

## 2. 实现 package

对于纯 Dart 库的 package，只要在 lib/<package name>.dart 文件中添加功能实现，或在 lib 目录中的多个文件中添加功能实现。
