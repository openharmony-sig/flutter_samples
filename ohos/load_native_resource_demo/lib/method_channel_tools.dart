import 'package:flutter/services.dart';

class MethodChannelTools {
  static const _platform = MethodChannel("com.example.dev/loadNativeResource");

  ///通过视频名称获取该视频在原生资源中的路径
  static Future<String> loadVideo(String videoName) async {
    try {
      final String videoPath = await _platform.invokeMethod("getVideo", {"resourceName": videoName});
      return videoPath;
    } on PlatformException catch (e) {
      return "Failed to load video:'${e.message}'";
    }
  }
}