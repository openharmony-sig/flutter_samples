import 'package:flutter/material.dart';

import 'load_native_rawfile_image.dart';
import 'load_native_media_image.dart';
import 'load_native_media_video.dart';
import 'load_native_rawfile_video.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Load Native Resource Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Load Native Resource'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextButton(onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const LoadNativeRawfileImage();
              }));
            }, child: const Text('LoadNativeRawfileImage')),
            const SizedBox(height: 30),
            TextButton(onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const LoadNativeRawfileVideo();
              }));
            }, child: const Text('LoadNativeRawfileVideo')),
            const SizedBox(height: 30),
            TextButton(onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const LoadNativeMediaImage();
              }));
            }, child: const Text('LoadNativeMediaImage')),
            const SizedBox(height: 30),
            TextButton(onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const LoadNativeMediaVideo();
              }));
            }, child: const Text('LoadNativeMediaVideo')),
          ],
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
