import 'package:flutter/material.dart';
import 'method_channel_tools.dart';

class LoadNativeRawfileImage extends StatelessWidget {
  const LoadNativeRawfileImage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("Native Rawfile Image Loader")),
        body: Center(
          child: Image.asset('landscape.jpg'),
          // child: FutureBuilder<String>(
          //   future: MethodChannelTools.loadImage('landscape.jpg'), // 请求图片资源
          //   builder: (context, snapshot) {
          //     if (snapshot.connectionState == ConnectionState.waiting) {
          //       return const CircularProgressIndicator();
          //     } else if (snapshot.hasError) {
          //       return Text('Error: ${snapshot.error}');
          //     } else {
          //       return Image.asset(snapshot.data!);
          //     }
          //   },
          // ),
        ),
    );
  }
}
