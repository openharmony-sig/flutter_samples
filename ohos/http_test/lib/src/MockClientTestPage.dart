import 'dart:convert';

import 'package:http/testing.dart';
import 'package:http/http.dart' as http;

import '../common/test_page.dart';

class MockClientTestPage extends TestPage {
  MockClientTestPage(super.title) {
    test('http.post处理请求', () async {
      var client = MockClient((request) async => http.Response(
          json.encode(request.bodyFields), 200,
          request: request, headers: {'content-type': 'application/json'}));

      var response = await client.post(Uri.http('example.com', '/foo'),
          body: {'field1': 'value1', 'field2': 'value2'});
      expect(
          response.body, {'field1': 'value1', 'field2': 'value2'});
    });

    test('http.post处理流式请求', () async {
      var client = MockClient.streaming((request, bodyStream) async {
        var bodyString = await bodyStream.bytesToString();
        var stream = Stream.fromIterable(['Request body was "$bodyString"'.codeUnits]);
        return http.StreamedResponse(stream, 200);
      });

      var uri = Uri.http('example.com', '/foo');
      var request = http.Request('POST', uri)..body = 'hello, world';
      var streamedResponse = await client.send(request);
      var response = await http.Response.fromStream(streamedResponse);
      expect(response.body, 'Request body was "hello, world"');
    });

    test('MockClient((_) async => http.Response("you did it", 200)).read(Uri.http("example.com", "/foo"))', () async {
      var client = MockClient((_) async => http.Response('you did it', 200));

      expect(await client.read(Uri.http('example.com', '/foo')),
          'you did it');
    });
  }

}