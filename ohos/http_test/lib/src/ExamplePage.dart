import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:http/retry.dart';

import 'IoTestPage.dart';

class ExamplePage extends StatefulWidget {
  const ExamplePage({super.key, required this.title});

  final String title;

  @override
  State<StatefulWidget> createState() => _ExamplePageState();

}

class _ExamplePageState extends State<ExamplePage> {

  String httpsGetResponse = '';
  String httpsPostResponse = '';
  String retryResponse = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SingleChildScrollView(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                    'http.get: $httpsGetResponse', style: const TextStyle(fontSize: 20, color: Colors.black)),
                MaterialButton(
                  onPressed: () async {
                    final response = await getClient().get(Uri.parse('https://www.baidu.com/'));
                    setState(() {
                      if (response.statusCode == 200) {
                        httpsGetResponse = response.body;
                      } else {
                        httpsGetResponse = 'Failed to load data';
                      }
                    });},
                  color: Colors.blue,
                  child: const Text('http.get'),
                ),
                Text(
                    'http.post: $httpsPostResponse', style: const TextStyle(fontSize: 20, color: Colors.black)),
                MaterialButton(
                  onPressed: () async {
                    Response response = await getClient().post(
                      Uri.parse('https://httpbin.org/post'),
                      body: {
                        'title': 'http.post',
                        'data': 'http.post请求,请求成功返回值',
                      },
                    );
                    setState(() {
                      httpsPostResponse = response.body;
                    });},
                  color: Colors.blue,
                  child: const Text('http.post'),
                ),
                Text(
                    'RetryClient.get: $retryResponse',
                    style: const TextStyle(fontSize: 20, color: Colors.black)),
                MaterialButton(
                  onPressed: () async {
                    // 创建重试客户端
                    RetryClient retryClient = RetryClient(
                      getClient(), // 使用默认的HttpClient
                      retries: 3, // 设置最大重试次数
                    );
                    // 发起HTTP请求
                    try {
                      final response = await retryClient.get(Uri.parse('https://www.baidu.com/'));
                      if (response.statusCode == 200) {
                        // 请求成功，处理响应数据
                        retryResponse = response.body;
                      } else {
                        // 请求失败，处理错误
                        retryResponse = 'Request failed with status code: $response.statusCode';
                      }
                    } on HttpException catch (e) {
                      // 处理网络异常
                      retryResponse = 'Network error occurred: $e';
                    } catch (e) {
                      // 处理其他异常
                      retryResponse = 'An error occurred: $e';
                    }
                    setState(() {
                    });
                    },
                  color: Colors.blue,
                  child: const Text('RetryClient.get'),
                ),
              ],
            ),
          )
      ),
    );
  }
}