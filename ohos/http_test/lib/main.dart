import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http_test/src/ExamplePage.dart';
import 'package:http_test/src/HttpRetryTestPage.dart';
import 'package:http_test/src/IoTestPage.dart';
import 'package:http_test/src/MockClientTestPage.dart';
import 'package:http_test/src/MultipartTestPage.dart';
import 'package:http_test/src/NoDefaultHttpClientTestPage.dart';
import 'package:http_test/src/RequestTestPage.dart';
import 'package:http_test/src/ResponseTestPage.dart';
import 'package:http_test/src/StreamedRequestTestPage.dart';

import 'common/test_model_app.dart';
import 'common/test_route.dart';

void main() {
  HttpOverrides.global = GlobalHttpOverrides();

  final app = [
    MainItem('example_test', const ExamplePage(title: 'example_test')),
    MainItem('io_test', IoTestPage('io_test')),
    MainItem('http_retry_test', HttpRetryTestPage('http_retry_test')),
    MainItem('mock_client_test', MockClientTestPage('mock_client_test')),
    MainItem('multipart_test', MultipartTestPage('multipart_test')),
    MainItem('no_default_http_client_test', NoDefaultHttpClientTestPage('no_default_http_client_test')),
    MainItem('request_test', RequestTestPage('request_test')),
    MainItem('response_test', ResponseTestPage('response_test')),
    MainItem('streamed_request_test', StreamedRequestTestPage('streamed_request_test')),
  ];

  runApp(TestModelApp(
      appName: 'http',
      data: app));
}


class GlobalHttpOverrides extends HttpOverrides {
  @override
  HttpClient creatHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}