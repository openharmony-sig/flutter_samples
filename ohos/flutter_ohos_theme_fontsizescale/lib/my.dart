import 'package:flutter/material.dart';
import 'MyContents/info.dart';
import 'MyContents/wallet.dart';
import 'settings.dart';

class MyView extends StatefulWidget{
  const MyView({super.key});

  @override
  MyViewState createState() => MyViewState();
}
class MyViewState extends State{
  @override
  Widget build(BuildContext context) {

    Brightness sysColorMode = MediaQuery.of(context).platformBrightness;

    return MaterialApp(
      title: "",
      theme: ThemeData(
        primaryColor: sysColorMode == Brightness.dark? Colors.white : Colors.black,
        brightness: sysColorMode   // system theme settings 
      ),
      home:Scaffold(
      body: ListView(
        children: <Widget>[
          Container(
            padding:const EdgeInsets.only(top: 20.0),
            // color: Colors.grey[200],
            child: Container(
              height: 80.0,
              // color: Colors.white,
              child: ListTile(
                leading: (sysColorMode == Brightness.light)? 
                  Image.asset("images/a001.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover,) : Image.asset("images/a001.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover, color: Colors.brown ,colorBlendMode: BlendMode.color,),
                title: const Text("xzj"),
                subtitle: const Text("账号：HW_CBG_2D"),
                trailing: const Icon(Icons.fullscreen),
                onTap: (){
                  Navigator.of(context).push(
                      MaterialPageRoute(
                          builder: (context){
                            return const InfoView();
                          }
                      )
                  );
                },
              ),
            ),
          ),
          Container(
            padding:const EdgeInsets.only(top: 20.0),
            // color: Colors.grey[200],
            child: Container(
              // color: Colors.white,
              height: 50.0,
              child: ListTile(
                leading: const Icon(Icons.call_to_action),
                title: const Text("钱包（可点击）"),
                onTap: (){
                  Navigator.of(context).push(
                      MaterialPageRoute(
                          builder: (context){
                            return const WalletView();
                          }
                      )
                  );
                },
              ),
            ),
          ),
          Container(
            padding:const EdgeInsets.only(top: 20.0),
            // color: Colors.grey[200],
            child: Container(
              // color: Colors.white,
              height: 50.0,
              child: const ListTile(
                leading: Icon(Icons.dashboard),
                title: Text("收藏"),
              ),
            ),
          ),
          Container(
            // color: Colors.white,
            height: 50.0,
            child: const ListTile(
              leading: Icon(Icons.photo),
              title: Text("相册"),
            ),
          ),
          Container(
            // color: Colors.white,
            height: 50.0,
            child: const ListTile(
              leading: Icon(Icons.credit_card),
              title: Text("卡包"),
            ),
          ),
          Container(
            // color: Colors.white,
            height: 50.0,
            child: const ListTile(
              leading: Icon(Icons.tag_faces),
              title: Text("表情"),
            ),
          ),
          Container(
            padding:const EdgeInsets.only(top: 20.0),
            // color: Colors.grey[200],
            child: Container(
              // color: Colors.white,
              height: 50.0,
              child: ListTile(
                leading: const Icon(Icons.settings),
                title: const Text("设置（可点击）"),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => SettingsPage()));
                },
              ),
            ),
          ),
        ],
      )
    )
    );
  }
}



class SecondPage extends StatelessWidget {
  const SecondPage({super.key});

  @override
  Widget build(BuildContext context) {

    double systemFontSizeScale = MediaQuery.of(context).textScaleFactor;

    return MaterialApp(
      title: "设置界面",
      theme: ThemeData(
        primaryColor: Colors.blue,
      ),
      home: Scaffold(
          body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
                // ignore: prefer_const_constructors
                Text(
                "当前系统文字大小",
                // style: TextStyle(fontSize: 14 * systemFontSizeScale),
                ),
                // ignore: prefer_const_constructors
                Text(
                "当前系统文字大小",
                style: TextStyle(fontSize: 14 * systemFontSizeScale),
              ),
            ],
          ),
        ),
          
      ),
    );
  }

}