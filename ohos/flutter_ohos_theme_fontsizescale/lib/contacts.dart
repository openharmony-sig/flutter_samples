import 'package:flutter/material.dart';

class ContactView extends StatefulWidget{
  const ContactView({super.key});

  @override
  // ignore: library_private_types_in_public_api
  ContactViewState createState() => ContactViewState();
}
class ContactViewState extends State{
  @override
  Widget build(BuildContext context) {

    Brightness sysColorMode = MediaQuery.of(context).platformBrightness;

    return MaterialApp(
        theme: ThemeData(
          primaryColor: sysColorMode == Brightness.dark? Colors.white : Colors.black,
          brightness: sysColorMode
        ),
        home: Scaffold(
          body: _contactListView(sysColorMode)
        ) 
    );
  }
}

ListView _contactListView(sysColorMode) {
    return ListView(
        children: <Widget>[
          Container(
            // color: Colors.grey[200],
            padding: const EdgeInsets.only(top: 10.0),
            child: Container(
              height: 50.0,
              // color: Colors.white,
              child: const ListTile(
                title: Text("新的朋友"),
                leading: Icon(Icons.add),
              ),
            ),
          ),
          Container(
            height: 50.0,
            // color: Colors.white,
            child: const ListTile(
              title: Text("群聊"),
              leading: Icon(Icons.group),
            ),
          ),
          Container(
            height: 50.0,
            // color: Colors.white,
            child: const ListTile(
              title: Text("标签"),
              leading: Icon(Icons.label),
            ),
          ),
          // ------------------------------------ 联系人信息 ---------------------------------------------
          Container(
            // color: Colors.grey[200],
            padding: const EdgeInsets.only(top: 20.0),
            child: Container(
              height: 50.0,
              // color: Colors.white,
              child: ListTile(
                title: const Text("aaa"),
                leading: (sysColorMode == Brightness.light)? 
                    Image.asset("images/xk.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover,) : Image.asset("images/xk.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover, color: Colors.brown ,colorBlendMode: BlendMode.color,),
              ),
            ),
          ),
          Container(
            height: 50.0,
            // color: Colors.white,
            child: ListTile(
              title: const Text("bbb"),
              leading: (sysColorMode == Brightness.light)? 
                    Image.asset("images/a002.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover,) : Image.asset("images/a002.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover, color: Colors.brown ,colorBlendMode: BlendMode.color,),
            ),
          ),
          Container(
            height: 50.0,
            // color: Colors.white,
            child: ListTile(
              title: const Text("ccc"),
              leading: (sysColorMode == Brightness.light)? 
                    Image.asset("images/a001.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover,) : Image.asset("images/a001.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover, color: Colors.brown ,colorBlendMode: BlendMode.color,),
            ),
          ),
          Container(
            height: 50.0,
            // color: Colors.white,
            child: ListTile(
              title: const Text("ddd"),
              leading: (sysColorMode == Brightness.light)? 
                    Image.asset("images/a006.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover,) : Image.asset("images/a006.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover, color: Colors.brown ,colorBlendMode: BlendMode.color,),
            ),
          ),
          Container(
            height: 50.0,
            // color: Colors.white,
            child: ListTile(
              title: const Text("eee"),
              leading: (sysColorMode == Brightness.light)? 
                    Image.asset("images/a004.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover,) : Image.asset("images/a004.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover, color: Colors.brown ,colorBlendMode: BlendMode.color,),
            ),
          ),
          Container(
            height: 50.0,
            // color: Colors.white,
            child: ListTile(
              title: const Text("fff"),
              leading: (sysColorMode == Brightness.light)? 
                    Image.asset("images/a005.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover,) : Image.asset("images/a005.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover, color: Colors.brown ,colorBlendMode: BlendMode.color,),
            ),
          ),
          Container(
            height: 50.0,
            // color: Colors.white,
            child: ListTile(
              title: const Text("ggg"),
              leading: (sysColorMode == Brightness.light)? 
                    Image.asset("images/img.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover,) : Image.asset("images/img.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover, color: Colors.brown ,colorBlendMode: BlendMode.color,),
            ),
          ),
          Container(
            height: 50.0,
            // color: Colors.white,
            child: ListTile(
              title: const Text("qqq"),
              leading: (sysColorMode == Brightness.light)? 
                    Image.asset("images/a003.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover,) : Image.asset("images/a003.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover, color: Colors.brown ,colorBlendMode: BlendMode.color,),
            ),
          ),
          Container(
            height: 50.0,
            // color: Colors.white,
            child: ListTile(
              title: const Text("www"),
              leading: (sysColorMode == Brightness.light)? 
                    Image.asset("images/a001.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover,) : Image.asset("images/a001.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover, color: Colors.brown ,colorBlendMode: BlendMode.color,),
            ),
          ),
          Container(
            height: 50.0,
            // color: Colors.white,
            child: ListTile(
              title: const Text("ooo"),
              leading: (sysColorMode == Brightness.light)? 
                    Image.asset("images/a002.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover,) : Image.asset("images/a002.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover, color: Colors.brown ,colorBlendMode: BlendMode.color,),
            ),
          ),
          Container(
            height: 50.0,
            // color: Colors.white,
            child: ListTile(
              title: const Text("123"),
              leading: (sysColorMode == Brightness.light)? 
                    Image.asset("images/a006.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover,) : Image.asset("images/a006.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover, color: Colors.brown ,colorBlendMode: BlendMode.color,),
            ),
          ),
          Container(
            height: 50.0,
            // color: Colors.white,
            child: ListTile(
              title: const Text("789"),
              leading: (sysColorMode == Brightness.light)? 
                    Image.asset("images/a001.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover,) : Image.asset("images/a001.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover, color: Colors.brown ,colorBlendMode: BlendMode.color,),
            ),
          ),
          Container(
            height: 50.0,
            // color: Colors.white,
            child: ListTile(
              title: const Text("666"),
              leading: (sysColorMode == Brightness.light)? 
                    Image.asset("images/a004.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover,) : Image.asset("images/a004.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover, color: Colors.brown ,colorBlendMode: BlendMode.color,),
            ),
          ),
          Container(
            height: 50.0,
            // color: Colors.white,
            child: ListTile(
              title: const Text("hehe"),
              leading: (sysColorMode == Brightness.light)? 
                    Image.asset("images/a003.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover,) : Image.asset("images/a003.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover, color: Colors.brown ,colorBlendMode: BlendMode.color,),
            ),
          ),
          Container(
            height: 50.0,
            // color: Colors.white,
            child: ListTile(
              title: const Text("harmonyos"),
              leading: (sysColorMode == Brightness.light)? 
                    Image.asset("images/a002.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover,) : Image.asset("images/a002.jpg",width: 40.0,height: 40.0,fit: BoxFit.cover, color: Colors.brown ,colorBlendMode: BlendMode.color,),
            ),
          ),
        ],
      );
}