import 'package:flutter/material.dart';
import 'package:platform_test/src/ExampleTestPage.dart';
import 'package:platform_test/src/FakePlatformTestPage.dart';

import 'common/test_model_app.dart';
import 'common/test_route.dart';

void main() {
  final app = [
    MainItem('example_test', const ExampleTestPage(title: 'example_test')),
    MainItem('fake_platform_test', FakePlatFormTestPage('fake_platform_test')),
  ];

  runApp(TestModelApp(
      appName: 'platform',
      data: app));
}
