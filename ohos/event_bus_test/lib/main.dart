import 'package:event_bus_test/src/EventBusHierarchicalTestPage.dart';
import 'package:event_bus_test/src/EventBusTestPage.dart';
import 'package:event_bus_test/src/ExampleTestPage.dart';
import 'package:flutter/material.dart';

import 'common/test_model_app.dart';
import 'common/test_route.dart';

void main() {
  final app = [
    MainItem('example_test', ExampleTestPage(title: 'example_test')),
    MainItem('event_bus_hierarchical_test', EventBusHierarchicalTestPage('event_bus_hierarchical_test')),
    MainItem('event_bus_test', EventBusTestPage('event_bus_test')),
  ];

  runApp(TestModelApp(
      appName: 'event_bus',
      data: app));
}
