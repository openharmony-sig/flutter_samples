import 'package:event_bus/event_bus.dart';

import '../common/test_page.dart';

class EventA extends SuperEvent {
  String text;

  EventA(this.text);
}

class EventB extends SuperEvent {
  String text;

  EventB(this.text);
}

class SuperEvent {}

class EventBusHierarchicalTestPage extends TestPage {
  EventBusHierarchicalTestPage(super.title) {
    group('[EventBus] (hierarchical)', () {
      test('EventBus().fire(), EventBus().on<EventA>()接收同一个事件', () {
        // given
        EventBus eventBus = EventBus();
        Future f = eventBus.on<EventA>().toList();

        // when
        eventBus.fire(EventA('a1'));
        eventBus.fire(EventB('b1'));
        eventBus.destroy();

        // then
        return f.then((events) {
          expect(events.length, 1);
        });
      });

      test('EventBus().fire(), EventBus().on<SuperEvent>()接收各自事件', () {
        // given
        EventBus eventBus = EventBus();
        Future f = eventBus.on<SuperEvent>().toList();

        // when
        eventBus.fire(EventA('a1'));
        eventBus.fire(EventB('b1'));
        eventBus.destroy();

        // then
        return f.then((events) {
          expect(events.length, 2);
        });
      });
    });
  }

}