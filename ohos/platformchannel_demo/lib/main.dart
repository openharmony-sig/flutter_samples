import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'clipboard.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String _showContent = "";

  @override
  void initState() {
    super.initState();

    // 设置系统初始化UI模式
    // SystemChrome.setEnabledSystemUIMode(SystemUiMode.leanBack);
    // SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    //   statusBarColor: Colors.blue, // 设置状态栏背景色为蓝色
    //   // statusBarBrightness: Brightness.dark,
    //   // statusBarIconBrightness: Brightness.light,
    //   // systemStatusBarContrastEnforced: true,
    //   systemNavigationBarColor: Colors.red,
    //   // systemNavigationBarDividerColor: Colors.black,
    //   // systemNavigationBarIconBrightness: Brightness.dark,
    //   // systemNavigationBarContrastEnforced: true,
    // ));
    print("----- initState before------");
    // SystemChrome.setSystemUIChangeCallback((systemOverlaysAreVisible) async {
    //   await Future.delayed(const Duration(seconds: 1));
    //   SystemChrome.restoreSystemUIOverlays();
    //   print("----- 样式还原 ------");
    // });
    print("----- initState after------");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('PlatformChannel Demo'),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            // Text('SystemUiMode: $_systemUiMode'),
            Text('当前状态: $_showContent'),

            // SystemUiMode.leanBack模式
            const SizedBox(height: 20),
            ElevatedButton(
              child: const Text('SystemUiMode.leanBack'),
              onPressed: () {
                SystemChrome.setEnabledSystemUIMode(SystemUiMode.leanBack);
                setStateName("SystemUiMode.leanBack");
              },
            ),

            // SystemUiMode.immersive模式
            const SizedBox(height: 20),
            ElevatedButton(
              child: const Text('SystemUiMode.immersive'),
              onPressed: () {
                SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersive);
                setStateName("SystemUiMode.immersive");
              },
            ),

            // SystemUiMode.immersiveSticky模式
            const SizedBox(height: 20),
            ElevatedButton(
              child: const Text('SystemUiMode.immersiveSticky'),
              onPressed: () {
                SystemChrome.setEnabledSystemUIMode(
                    SystemUiMode.immersiveSticky);
                setStateName("SystemUiMode.immersiveSticky");
              },
            ),

            // SystemUiMode.edgeToEdge模式
            const SizedBox(height: 10),
            ElevatedButton(
              child: const Text('SystemUiMode.edgeToEdge'),
              onPressed: () {
                SystemChrome.setEnabledSystemUIMode(SystemUiMode.edgeToEdge);
                setStateName("SystemUiMode.edgeToEdge");
              },
            ),

            // manual模式
            const SizedBox(height: 10),
            ElevatedButton(
              child: const Text('Manual: SystemUiOverlay.top'),
              onPressed: () {
                SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,
                    overlays: [SystemUiOverlay.top]);
                setStateName("SystemUiOverlay.top");
              },
            ),
            const SizedBox(height: 10),
            ElevatedButton(
              child: const Text('Manual: SystemUiOverlay.bottom'),
              onPressed: () {
                SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,
                    overlays: [SystemUiOverlay.bottom]);
                setStateName("SystemUiOverlay.bottom");
              },
            ),
            const SizedBox(height: 10),
            ElevatedButton(
              child: const Text('Manual: SystemUiOverlay.top + bottom'),
              onPressed: () {
                SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,
                    overlays: [SystemUiOverlay.top, SystemUiOverlay.bottom]);
                setStateName("SystemUiOverlay.top + bottom");
              },
            ),

            // 振动功能
            const SizedBox(height: 20),
            ElevatedButton(
              child: const Text('Hapticfeedback.vibrate'),
              onPressed: () {
                HapticFeedback.vibrate();
                setStateName("Hapticfeedback.vibrate");
              },
            ),

            // SystemSound播放声音（os未提供接口）
            const SizedBox(height: 20),
            ElevatedButton(
              child: const Text('SystemSound.play'),
              onPressed: () {
                SystemSound.play(SystemSoundType.click);
                setStateName("播放功能os暂未支持");
              },
            ),

            // setSystemUIOverlayStyle设置状态栏、导航栏的样式
            const SizedBox(height: 20),
            ElevatedButton(
              child: const Text('SystemChrome.setSystemUIOverlayStyle'),
              onPressed: () {
                SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
                    statusBarColor: Colors.cyan,
                    systemNavigationBarColor: Colors.black));
                setStateName("setSystemUIOverlayStyle -> 状态栏cyan色");
              },
            ),

            // restoreSystemUIOverlay
            // const SizedBox(height: 20),
            // ElevatedButton(
            //   child: const Text('SystemChrome.restoreSystemUIOverlay'),
            //   onPressed: () {
            //     SystemChrome.restoreSystemUIOverlays();
            //     setStateName("SystemChrome.restoreSystemUIOverlay");
            //   },
            // ),

            // 退出app操作
            const SizedBox(height: 20),
            ElevatedButton(
              child: const Text('SystemNavigator.pop'), // 退出app
              onPressed: () {
                SystemNavigator.pop();
              },
            ),

            // clipboard功能demo，点击跳转到clipboard页面
            const SizedBox(height: 20),
            ElevatedButton(
              child: const Text('Clipboard页面路由'),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const ClipboardDemo()));
              },
            ),

            // 屏幕左转
            const SizedBox(height: 20),
            ElevatedButton(
              child: const Text('DeviceOrientation.landscapeLeft'),
              onPressed: () {
                SystemChrome.setPreferredOrientations(
                    [DeviceOrientation.landscapeLeft]);
                setStateName("DeviceOrientation.landscapeLeft");
              },
            ),

            // 屏幕右转
            const SizedBox(height: 20),
            ElevatedButton(
              child: const Text('DeviceOrientation.landscapeRight'),
              onPressed: () {
                SystemChrome.setPreferredOrientations(
                    [DeviceOrientation.landscapeRight]);
                setStateName("DeviceOrientation.landscapeRight");
              },
            ),

            // 屏幕下转
            const SizedBox(height: 20),
            ElevatedButton(
              child: const Text('DeviceOrientation.portraitDown'),
              onPressed: () {
                SystemChrome.setPreferredOrientations(
                    [DeviceOrientation.portraitDown]);
                setStateName("DeviceOrientation.portraitDown");
              },
            ),

            // 屏幕上转
            const SizedBox(height: 20),
            ElevatedButton(
              child: const Text('DeviceOrientation.portraitUp'),
              onPressed: () {
                SystemChrome.setPreferredOrientations(
                    [DeviceOrientation.portraitUp]);
                setStateName("DeviceOrientation.portraitUp");
              },
            ),

            // 设置应用程序切换器描述
            const SizedBox(height: 20),
            ElevatedButton(
              child:
                  const Text('SystemChrome.setApplicationSwitcherDescription'),
              onPressed: () {
                SystemChrome.setApplicationSwitcherDescription(
                    ApplicationSwitcherDescription(
                        label: 'My Flutter App', // 任务管理器中的标签
                        primaryColor: 0xFF0000FF.toInt() // 应用的主要颜色，用于生成启动器图标
                        ));
                setStateName("SystemChrome.setApplicationSwitcherDescription");
              },
            ),

            // systemUiChangeListener监听事件
            const SizedBox(height: 20),
            ElevatedButton(
              child: const Text('SystemChrome.setSystemUIChangeCallback'),
              onPressed: () {
                SystemChrome.setSystemUIChangeCallback(
                    (systemOverlaysAreVisible) async {
                  await Future.delayed(const Duration(seconds: 1));
                  SystemChrome.restoreSystemUIOverlays();
                });
                setStateName("SystemChrome.setSystemUIChangeCallback");
              },
            ),
          ],
        ),
      ),
    );
  }

  void setStateName(String str) {
    setState(() {
      _showContent = str;
    });
  }
}
