import 'package:flutter/material.dart';
import 'package:list_view_demo/data_service.dart';
import 'package:list_view_demo/product.dart';

/// 数据管理类：管理商品数据
class DataProvider extends ChangeNotifier {
  final DataService dataService;
  int _currentPage = 0; // 当前分页数
  final int pageSize; // 每页条目数
  final List<Product> _products = []; // 商品数据缓存
  bool _isLoading = false; // 是否正在加载
  bool _hasMore = true; // 是否还有更多数据

  DataProvider({required this.dataService, this.pageSize = 20});

  // 公开的属性
  List<Product> get products => _products;
  bool get isLoading => _isLoading;
  bool get hasMore => _hasMore;

  /// 加载下一页数据
  Future<void> loadNextPage() async {
    if (_isLoading || !_hasMore) return; // 防止重复加载
    _isLoading = true;
    notifyListeners();

    _currentPage++;
    final newProducts = await dataService.fetchPage(_currentPage, pageSize);

    if (newProducts.isEmpty) {
      _hasMore = false; // 没有更多数据了
    } else {
      _products.addAll(newProducts);
    }

    _isLoading = false;
    notifyListeners();
  }
}
