import 'package:list_view_demo/product.dart';

/// 数据服务类：模拟远程数据源
class DataService {
  // 模拟从服务器获取分页数据（商品列表）
  Future<List<Product>> fetchPage(int page, int pageSize) async {
    await Future.delayed(const Duration(seconds: 1)); // 模拟网络延迟
    if (page == 10) {
      return [];
    }
    return List.generate(
      pageSize,
      (index) => Product(
        id: (page - 1) * pageSize + index + 1,
        name: '商品 ${(page - 1) * pageSize + index + 1}',
        price: (index + 1) * 10.0,
        description: '这是商品 ${(page - 1) * pageSize + index + 1} 的描述信息。',
      ),
    );
  }
}
