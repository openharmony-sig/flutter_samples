import 'package:vector_math/vector_math.dart';
import 'package:vector_math_test/src/test_utils.dart';

import '../common/test_page.dart';

class PlaneTestPage extends TestPage{
  PlaneTestPage(super.title) {
    group('Plane', () {
      test('Plane.normalconstant(Vector3 normal_, this.constant)和plane.normalize()', testPlaneNormalize);
      test('plane.distanceToVector3(Vector3 point)', testPlaneDistanceToVector3);
      test('plane.intersection(Plane a, Plane b, Plane c, Vector3 result)', testPlaneIntersection);
    });
  }

  void testPlaneNormalize() {
    final plane = Plane.normalconstant($v3(2.0, 0.0, 0.0), 2.0);

    plane.normalize();

    expect(plane.normal.x, 1.0);
    expect(plane.normal.y, 0.0);
    expect(plane.normal.z, 0.0);
    expect(plane.normal.length, 1.0);
    expect(plane.constant, 1.0);
  }

  void testPlaneDistanceToVector3() {
    final plane = Plane.normalconstant($v3(2.0, 0.0, 0.0), -2.0);

    plane.normalize();

    expect(plane.distanceToVector3($v3(4.0, 0.0, 0.0)), 3.0);
    expect(plane.distanceToVector3($v3(1.0, 0.0, 0.0)), 0.0);
  }

  void testPlaneIntersection() {
    final plane1 = Plane.normalconstant($v3(1.0, 0.0, 0.0), -2.0);
    final plane2 = Plane.normalconstant($v3(0.0, 1.0, 0.0), -3.0);
    final plane3 = Plane.normalconstant($v3(0.0, 0.0, 1.0), -4.0);

    plane1.normalize();
    plane2.normalize();
    plane3.normalize();

    final point = Vector3.zero();

    Plane.intersection(plane1, plane2, plane3, point);

    expect(point.x, 2.0);
    expect(point.y, 3.0);
    expect(point.z, 4.0);
  }

}