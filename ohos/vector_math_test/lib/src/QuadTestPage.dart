import 'package:vector_math/vector_math.dart';
import 'package:vector_math_test/src/test_utils.dart';

import '../common/test_page.dart';

class QuadTestPage extends TestPage{
  QuadTestPage(super.title) {
    group('Quad', () {
      test('Quad.copy(Quad other)', testQuadCopy);
      test('Quad.points(Vector3 point0, Vector3 point1, Vector3 point2, Vector3 point3)和copyNormalInto(Vector3 normal)', testQuadCopyNormalInto);
      test('Quad.copyTriangles(Triangle triangle0, Triangle triangle1)', testQuadCopyTriangles);
    });
  }

  void testQuadCopy() {
    final quad = Quad.points(Vector3(1.0, 0.0, 1.0), Vector3(0.0, 2.0, 1.0),
        Vector3(1.0, 0.0, 0.0), Vector3(0.0, 2.0, 0.0));
    final quadCopy = Quad.copy(quad);

    relativeTest(quadCopy.point0, quad.point0);
    relativeTest(quadCopy.point1, quad.point1);
    relativeTest(quadCopy.point2, quad.point2);
    relativeTest(quadCopy.point3, quad.point3);
  }

  void testQuadCopyNormalInto() {
    final quad = Quad.points(Vector3(1.0, 0.0, 1.0), Vector3(0.0, 2.0, 1.0),
        Vector3(1.0, 0.0, 0.0), Vector3(0.0, 2.0, 0.0));
    final normal = Vector3.zero();

    quad.copyNormalInto(normal);

    relativeTest(normal, Vector3(-0.8944271802902222, -0.4472135901451111, 0.0));
  }

  void testQuadCopyTriangles() {
    final quad = Quad.points(Vector3(1.0, 0.0, 1.0), Vector3(0.0, 2.0, 1.0),
        Vector3(1.0, 0.0, 0.0), Vector3(0.0, 2.0, 0.0));
    final t1 = Triangle();
    final t2 = Triangle();
    final normal = Vector3.zero();
    final t1Normal = Vector3.zero();
    final t2Normal = Vector3.zero();

    quad.copyNormalInto(normal);

    quad.copyTriangles(t1, t2);
    t1.copyNormalInto(t1Normal);
    t2.copyNormalInto(t2Normal);

    relativeTest(t1Normal, normal);
    relativeTest(t2Normal, normal);
  }

}