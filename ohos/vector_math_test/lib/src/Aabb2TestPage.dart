import 'dart:math' as math;

import 'package:vector_math/vector_math.dart';
import 'package:vector_math_test/src/test_utils.dart';

import '../common/test_page.dart';

class Aabb2TestPage extends TestPage{
  Aabb2TestPage(super.title, {super.key}) {
    group('Aabb2', () {
      test('Aabb2.minMax.center', testAabb2Center);
      test('Aabb2.minMax.copyCenterAndHalfExtents', testAabb2CenterAndHalfExtents);
      test('Aabb2.centerAndHalfExtents', testAabb2CopyCenterAndHalfExtents);
      test('Aabb2.setCenterAndHalfExtents', testAabb2SetCenterAndHalfExtents);
      test('Aabb2.containsAabb2', testAabb2ContainsAabb2);
      test('Aabb2.containsVector2', testAabb2ContainsVector2);
      test('Aabb2.intersectsWithAabb2', testAabb2IntersectionAabb2);
      test('Aabb2.intersectsWithVector2', testAabb2IntersectionVector2);
      test('Aabb2.hull', testAabb2Hull);
      test('Aabb2.hullPoint', testAabb2HullPoint);
      test('Aabb2..rotate', testAabb2Rotate);
      test('Aabb2..transform', testAabb2Transform);
    });
  }

  void testAabb2Center() {
    final aabb = Aabb2.minMax($v2(1.0, 2.0), $v2(8.0, 16.0));
    final center = aabb.center;

    expect(center, '');
  }

  void testAabb2CopyCenterAndHalfExtents() {
    final a1 = Aabb2.minMax($v2(10.0, 20.0), $v2(20.0, 40.0));
    final a2 = Aabb2.minMax($v2(-10.0, -20.0), $v2(0.0, 0.0));

    final center = Vector2.zero();
    final halfExtents = Vector2.zero();

    a1.copyCenterAndHalfExtents(center, halfExtents);

    relativeTest(center, $v2(15.0, 30.0));
    relativeTest(halfExtents, $v2(5.0, 10.0));

    a2.copyCenterAndHalfExtents(center, halfExtents);

    relativeTest(center, $v2(-5.0, -10.0));
    relativeTest(halfExtents, $v2(5.0, 10.0));
  }

  void testAabb2CenterAndHalfExtents() {
    final a1 = Aabb2.centerAndHalfExtents($v2(0.0, 0.0), $v2(10.0, 20.0));
    final a2 = Aabb2.centerAndHalfExtents($v2(-10.0, -20.0), $v2(10.0, 20.0));

    relativeTest(a1.min, $v2(-10.0, -20.0));
    relativeTest(a1.max, $v2(10.0, 20.0));

    relativeTest(a2.min, $v2(-20.0, -40.0));
    relativeTest(a2.max, $v2(0.0, 0.0));
  }

  void testAabb2SetCenterAndHalfExtents() {
    final a1 = Aabb2();
    final a2 = Aabb2();

    a1.setCenterAndHalfExtents($v2(0.0, 0.0), $v2(10.0, 20.0));

    relativeTest(a1.min, $v2(-10.0, -20.0));
    relativeTest(a1.max, $v2(10.0, 20.0));

    a2.setCenterAndHalfExtents($v2(-10.0, -20.0), $v2(10.0, 20.0));

    relativeTest(a2.min, $v2(-20.0, -40.0));
    relativeTest(a2.max, $v2(0.0, 0.0));
  }

  void testAabb2ContainsAabb2() {
    final parent = Aabb2.minMax($v2(1.0, 1.0), $v2(8.0, 8.0));
    final child = Aabb2.minMax($v2(2.0, 2.0), $v2(7.0, 7.0));
    final cutting = Aabb2.minMax($v2(0.0, 0.0), $v2(5.0, 5.0));
    final outside = Aabb2.minMax($v2(10.0, 10.0), $v2(20.0, 20.0));
    final grandParent = Aabb2.minMax($v2(0.0, 0.0), $v2(10.0, 10.0));

    expect(parent.containsAabb2(child), '');
    expect(parent.containsAabb2(parent), '');
    expect(parent.containsAabb2(cutting), '');
    expect(parent.containsAabb2(outside), '');
    expect(parent.containsAabb2(grandParent), '');
  }

  void testAabb2ContainsVector2() {
    final parent = Aabb2.minMax($v2(1.0, 1.0), $v2(8.0, 8.0));
    final child = $v2(2.0, 2.0);
    final cutting = $v2(1.0, 8.0);
    final outside = $v2(-1.0, 0.0);

    expect(parent.containsVector2(child), '');
    expect(parent.containsVector2(cutting), '');
    expect(parent.containsVector2(outside), '');
  }

  void testAabb2IntersectionAabb2() {
    final parent = Aabb2.minMax($v2(1.0, 1.0), $v2(8.0, 8.0));
    final child = Aabb2.minMax($v2(2.0, 2.0), $v2(7.0, 7.0));
    final cutting = Aabb2.minMax($v2(0.0, 0.0), $v2(5.0, 5.0));
    final outside = Aabb2.minMax($v2(10.0, 10.0), $v2(20.0, 20.0));
    final grandParent = Aabb2.minMax($v2(0.0, 0.0), $v2(10.0, 10.0));

    final siblingOne = Aabb2.minMax($v2(0.0, 0.0), $v2(3.0, 3.0));
    final siblingTwo = Aabb2.minMax($v2(3.0, 0.0), $v2(6.0, 3.0));
    final siblingThree = Aabb2.minMax($v2(3.0, 3.0), $v2(6.0, 6.0));

    expect(parent.intersectsWithAabb2(child), '');
    expect(child.intersectsWithAabb2(parent), '');

    expect(parent.intersectsWithAabb2(parent), '');

    expect(parent.intersectsWithAabb2(cutting), '');
    expect(cutting.intersectsWithAabb2(parent), '');

    expect(parent.intersectsWithAabb2(outside), '');
    expect(outside.intersectsWithAabb2(parent), '');

    expect(parent.intersectsWithAabb2(grandParent), '');
    expect(grandParent.intersectsWithAabb2(parent), '');

    expect(siblingOne.intersectsWithAabb2(siblingTwo), '');
    expect(siblingOne.intersectsWithAabb2(siblingThree), '');
  }

  void testAabb2IntersectionVector2() {
    final parent = Aabb2.minMax($v2(1.0, 1.0), $v2(8.0, 8.0));
    final child = $v2(2.0, 2.0);
    final cutting = $v2(1.0, 8.0);
    final outside = $v2(-1.0, 0.0);

    expect(parent.intersectsWithVector2(child), '');
    expect(parent.intersectsWithVector2(cutting), '');
    expect(parent.intersectsWithVector2(outside), '');
  }

  void testAabb2Hull() {
    final a = Aabb2.minMax($v2(1.0, 1.0), $v2(3.0, 4.0));
    final b = Aabb2.minMax($v2(3.0, 2.0), $v2(6.0, 2.0));

    a.hull(b);

    expect(a.min, a.max);
  }

  void testAabb2HullPoint() {
    final a = Aabb2.minMax($v2(1.0, 1.0), $v2(3.0, 4.0));
    final b = $v2(6.0, 2.0);

    a.hullPoint(b);

    expect(a.min, a.max);

    final c = $v2(0.0, 1.0);

    a.hullPoint(c);

    expect(a.min, a.max);
  }

  void testAabb2Rotate() {
    final rotation = Matrix3.rotationZ(math.pi / 4);
    final input = Aabb2.minMax($v2(1.0, 1.0), $v2(3.0, 3.0));

    final result = input..rotate(rotation);

    relativeTest(result.min.x, 2 - math.sqrt(2));
    relativeTest(result.min.y, 2 - math.sqrt(2));
    relativeTest(result.max.x, 2 + math.sqrt(2));
    relativeTest(result.max.y, 2 + math.sqrt(2));
    relativeTest(result.center.x, 2.0);
    relativeTest(result.center.y, 2.0);
  }

  void testAabb2Transform() {
    final rotation = Matrix3.rotationZ(math.pi / 4);
    final input = Aabb2.minMax($v2(1.0, 1.0), $v2(3.0, 3.0));

    final result = input..transform(rotation);
    final newCenterY = math.sqrt(8);

    relativeTest(result.min.x, -math.sqrt(2));
    relativeTest(result.min.y, newCenterY - math.sqrt(2));
    relativeTest(result.max.x, math.sqrt(2));
    relativeTest(result.max.y, newCenterY + math.sqrt(2));
    relativeTest(result.center.x, 0.0);
    relativeTest(result.center.y, newCenterY);
  }

}