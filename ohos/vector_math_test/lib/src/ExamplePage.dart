import 'package:flutter/material.dart';

import 'package:vector_math/vector_math.dart' as v;

class ExamplePage extends StatefulWidget {
  const ExamplePage({super.key, required this.title});

  final String title;

  @override
  State<ExamplePage> createState() => _ExamplePageState();
}

class _ExamplePageState extends State<ExamplePage> with SingleTickerProviderStateMixin {

  double x = 0;
  double y = 0;
  double z = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Column(
              children: [
                Container(
                    alignment: Alignment.topLeft,
                    height: 100,
                    child: CustomPaint(
                      painter: MyPainter(),
                    )
                ),
                Expanded(
                    child: Center(
                      child: Transform(
                        transform: Matrix4(
                          1,0,0,0,
                          0,1,0,0,
                          0,0,1,0,
                          0,0,0,1,
                        )..rotateX(x)..rotateY(y)..rotateZ(z),
                        alignment: FractionalOffset.center,
                        child: GestureDetector(
                          onPanUpdate: (details) {
                            setState(() {
                              y = y - details.delta.dx / 100;
                              x = x + details.delta.dy / 100;
                            });
                          },
                          child: Container(
                            color: Colors.red,
                            height: 200.0,
                            width: 200.0,
                          ),
                        ),
                      ),
                    )
                )
              ]),
          // const Center(child: SwordLoading(loadColor: Colors.black, size: 128)),
    );
  }
}

class MyPainter extends CustomPainter {

  v.Vector2 firstPoint = v.Vector2(0, 20);
  v.Vector2 secondPoint = v.Vector2(50, 20);

  v.Vector2 threePoint = v.Vector2(100, 50);
  v.Vector2 fourPoint = v.Vector2(200, 50);

  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..isAntiAlias = true
      ..color = Colors.red;
    canvas.drawLine(Offset(firstPoint.x, firstPoint.y), Offset(secondPoint.x, secondPoint.y), paint..strokeCap..strokeWidth = 8.0);
    canvas.drawLine(Offset(threePoint.x, threePoint.y), Offset(fourPoint.x, fourPoint.y), paint..strokeCap..strokeWidth = 16.0);

  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => false;
}