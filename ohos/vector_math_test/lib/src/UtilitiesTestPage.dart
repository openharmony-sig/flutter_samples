import 'dart:math' as math;

import 'package:vector_math/vector_math.dart';
import 'package:vector_math_test/src/test_utils.dart';

import '../common/test_page.dart';

class UtilitiesTestPage extends TestPage{
  UtilitiesTestPage(super.title) {
    group('Utilities', () {
      test('degrees(double radians)', testDegrees);
      test('radians(double degrees)', testRadians);
      test('mix(double min, double max, double a)', testMix);
      test('smoothStep(double edge0, double edge1, double amount)', testSmoothStep);
      test('catmullRom(double edge0, double edge1, double edge2, double edge3, double amount)', testCatmullRom);
    });
  }

  void testDegrees() {
    relativeTest(degrees(math.pi), 180.0);
  }

  void testRadians() {
    relativeTest(radians(90.0), math.pi / 2.0);
  }

  void testMix() {
    relativeTest(mix(2.5, 3.0, 1.0), 3.0);
    relativeTest(mix(1.0, 3.0, 0.5), 2.0);
    relativeTest(mix(2.5, 3.0, 0.0), 2.5);
    relativeTest(mix(-1.0, 0.0, 2.0), 1.0);
  }

  void testSmoothStep() {
    relativeTest(smoothStep(2.5, 3.0, 2.5), 0.0);
    relativeTest(smoothStep(2.5, 3.0, 2.75), 0.5);
    relativeTest(smoothStep(2.5, 3.0, 3.5), 1.0);
  }

  void testCatmullRom() {
    relativeTest(catmullRom(2.5, 3.0, 1.0, 3.0, 1.0), 1.0);
    relativeTest(catmullRom(1.0, 3.0, 1.0, 3.0, 0.5), 2.0);
    relativeTest(catmullRom(2.5, 3.0, 1.0, 3.0, 0.0), 3.0);
    relativeTest(catmullRom(-1.0, 0.0, 1.0, 0.0, 2.0), -2.0);
  }

}