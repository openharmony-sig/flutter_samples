import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:math' as math;

class CameraPage extends StatefulWidget {
  const CameraPage({super.key});
  @override
  _CameraPageState createState() => _CameraPageState();
}

class _CameraPageState extends State<CameraPage> {
  final MethodChannel _channel = MethodChannel('CameraControlChannel');

  int textureId = -1;

  @override
  void initState() {
    super.initState();

    newTexture();
    startCamera();
  }

  @override
  void dispose() {
    super.dispose();
    if (textureId >= 0) {
      _channel.invokeMethod('unregisterTexture', {'textureId': textureId});
    }
  }

  void startCamera() async {
    await _channel.invokeMethod('startCamera');
  }

  void newTexture() async {
    int id = await _channel.invokeMethod('registerTexture');
    setState(() {
      this.textureId = id;
    });
  }

  Widget getTextureBody(BuildContext context) {
    return Container(
      width: 500,
      height: 500,
      child: Texture(
        textureId: textureId,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget body = textureId >= 0 ? getTextureBody(context) : Text('loading...');
    print('build textureId :$textureId');

    return Scaffold(
      appBar: AppBar(
        title: Text("daex_texture"),
      ),
      body: Container(
        color: Colors.white,
        height: 500,
        child: Center(
          child: body,
        ),
      ),
    );
  }
}
