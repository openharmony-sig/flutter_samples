import 'dart:math' as math;

import 'package:string_scanner/string_scanner.dart';
import 'package:string_scanner_test/common/test_page.dart';

class ExamplePage extends TestPage {
  ExamplePage(super.title) {
    test('example', () {
      List<String> args = ["1", "1.5", "-3"];
      for(int i = 0; i < args.length; i++) {
        expect(parseNumber(args[i]), null);
      }
    });
  }

  num parseNumber(String source) {
    final scanner = StringScanner(source);

    final negative = scanner.scan('-');

    scanner.expect(RegExp(r'\d+'));

    var number = num.parse(scanner.lastMatch![0]!);

    if (scanner.scan('.')) {
      scanner.expect(RegExp(r'\d+'));
      final decimal = scanner.lastMatch![0]!;
      number += int.parse(decimal) / math.pow(10, decimal.length);
    }

    scanner.expectDone();

    return (negative ? -1 : 1) * number;
  }
}