import 'package:source_span/source_span.dart';
import 'package:string_scanner/string_scanner.dart';

import '../common/test_page.dart';

class SpanScannerTestPage extends TestPage {
  SpanScannerTestPage(super.title) {
    testForImplementation(
        'lazy',
            ([String? string]) =>
            SpanScanner(string ?? 'foo\nbar\nbaz', sourceUrl: 'source'));

    testForImplementation(
        'eager',
            ([String? string]) =>
            SpanScanner.eager(string ?? 'foo\nbar\nbaz', sourceUrl: 'source'));

    group('within', () {
      const text = 'first\nbefore: foo\nbar\nbaz :after\nlast';
      final startOffset = text.indexOf('foo');

      test('测试SpanScanner.within(file.span(startOffset, text.indexOf(" :after"))).string', () {
        final file = SourceFile.fromString(text, url: 'source');
        SpanScanner scanner = SpanScanner.within(file.span(startOffset, text.indexOf(' :after')));
        expect(scanner.string, 'foo\nbar\nbaz');
      });

      test('测试SpanScanner.within(file.span(startOffset, text.indexOf(" :after"))).scan("foo").scan("\n")', () {
        final file = SourceFile.fromString(text, url: 'source');
        SpanScanner scanner = SpanScanner.within(file.span(startOffset, text.indexOf(' :after')));
        expect(scanner.line, 0);
        expect(scanner.column, 0);

        scanner.scan('foo');
        expect(scanner.line, 0);
        expect(scanner.column, 3);

        scanner.scan('\n');
        expect(scanner.line, 1);
        expect(scanner.column, 0);
      });

      test('测试SpanScanner.within(file.span(startOffset, text.indexOf(" :after"))).scan("fo").scan("o\nba").lastSpan', () {
        final file = SourceFile.fromString(text, url: 'source');
        SpanScanner scanner = SpanScanner.within(file.span(startOffset, text.indexOf(' :after')));
        scanner.scan('fo');
        scanner.scan('o\nba');

        final span = scanner.lastSpan!;
        expect(span.start.offset, (startOffset + 2));
        expect(span.start.line, 1);
        expect(span.start.column, 10);
        expect(span.start.sourceUrl, Uri.parse('source'));

        expect(span.end.offset, startOffset + 6);
        expect(span.end.line, 2);
        expect(span.end.column, 2);
        expect(span.start.sourceUrl, Uri.parse('source'));

        expect(span.text, 'o\nba');
      });

      test('测试SpanScanner.within(file.span(startOffset, text.indexOf(" :after"))).scan("fo").scan("o\nba").scan("r\nba").spanFrom(scanner.state)', () {
        final file = SourceFile.fromString(text, url: 'source');
        SpanScanner scanner = SpanScanner.within(file.span(startOffset, text.indexOf(' :after')));
        scanner.scan('fo');
        final state = scanner.state;
        scanner.scan('o\nba');
        scanner.scan('r\nba');

        final span = scanner.spanFrom(state);
        expect(span.text, 'o\nbar\nba');
      });

      test('测试SpanScanner.within(file.span(startOffset, text.indexOf(" :after"))).scan("foo\nba").emptySpan', () {
        final file = SourceFile.fromString(text, url: 'source');
        SpanScanner scanner = SpanScanner.within(file.span(startOffset, text.indexOf(' :after')));
        scanner.scan('foo\nba');

        final span = scanner.emptySpan;
        expect(span.start.offset, startOffset + 6);
        expect(span.start.line, 2);
        expect(span.start.column, 2);
        expect(span.start.sourceUrl, Uri.parse('source'));

        expect(span.end.offset, startOffset + 6);
        expect(span.end.line, 2);
        expect(span.end.column, 2);
        expect(span.start.sourceUrl, Uri.parse('source'));

        expect(span.text, '');
      });

      test('测试SpanScanner.within(file.span(startOffset, text.indexOf(" :after"))).expect("foo").error("oh no!")', () {
        final file = SourceFile.fromString(text, url: 'source');
        SpanScanner scanner = SpanScanner.within(file.span(startOffset, text.indexOf(' :after')));
        scanner.expect('foo');
        expect(
                () => scanner.error('oh no!'), 'foo');
      });

      test('测试SpanScanner.within(file.span(startOffset, text.indexOf(" :after"))).expect("foo\nbar\nbaz").isDone', () {
        final file = SourceFile.fromString(text, url: 'source');
        SpanScanner scanner = SpanScanner.within(file.span(startOffset, text.indexOf(' :after')));
        scanner.expect('foo\nbar\nbaz');
        expect(scanner.isDone, 'isTrue');
      });
    });
  }

  void testForImplementation(
      String name, SpanScanner Function([String string]) create) {
    group('for a $name scanner', () {

      test('${create}测试scanner.lastSpan的范围', () {
        SpanScanner scanner = create();
        scanner.scan('fo');
        scanner.scan('o\nba');

        final span = scanner.lastSpan!;
        expect(span.start.offset, 2);
        expect(span.start.line, 0);
        expect(span.start.column, 2);
        expect(span.start.sourceUrl, Uri.parse('source'));

        expect(span.end.offset, 6);
        expect(span.end.line, 1);
        expect(span.end.column, 2);
        expect(span.start.sourceUrl, Uri.parse('source'));

        expect(span.text, 'o\nba');
      });

      test('${create}测试scanner.spanFrom(scanner.state)返回上一级状态的范围', () {
        SpanScanner scanner = create();
        scanner.scan('fo');
        final state = scanner.state;
        scanner.scan('o\nba');
        scanner.scan('r\nba');

        final span = scanner.spanFrom(state);
        expect(span.text, 'o\nbar\nba');
      });

      test('${create}测试scanner.emptySpan()返回当前位置的空范围', () {
        SpanScanner scanner = create();
        scanner.scan('foo\nba');

        final span = scanner.emptySpan;
        expect(span.start.offset, 6);
        expect(span.start.line, 1);
        expect(span.start.column, 2);
        expect(span.start.sourceUrl, Uri.parse('source'));

        expect(span.end.offset, 6);
        expect(span.end.line, 1);
        expect(span.end.column, 2);
        expect(span.start.sourceUrl, Uri.parse('source'));

        expect(span.text, '');
      });

      group('before a surrogate pair', () {
        final codePoint = '\uD83D\uDC6D'.runes.first;
        const highSurrogate = 0xD83D;

        test('测试${create}.scan("foo: ").readChar()情况', () {
          SpanScanner scanner = create('foo: \uD83D\uDC6D bar');
          expect(scanner.scan('foo: '), 'isTrue');
          expect(scanner.readChar(), highSurrogate);
          expect(scanner.line, 0);
          expect(scanner.column, 6);
          expect(scanner.position, 6);
        });

        test('测试${create}.scan("foo: ").readCodePoint()情况', () {
          SpanScanner scanner = create('foo: \uD83D\uDC6D bar');
          expect(scanner.scan('foo: '), 'isTrue');
          expect(scanner.readCodePoint(), codePoint);
          expect(scanner.line, 0);
          expect(scanner.column, 7);
          expect(scanner.position, 7);
        });

        test('测试${create}.scan("foo: ").scanChar(0xD83D)情况', () {
          SpanScanner scanner = create('foo: \uD83D\uDC6D bar');
          expect(scanner.scan('foo: '), 'isTrue');
          expect(scanner.scanChar(highSurrogate), 'isTrue');
          expect(scanner.line, 0);
          expect(scanner.column, 6);
          expect(scanner.position, 6);
        });

        test('测试${create}.scan("foo: ").scanChar("\uD83D\uDC6D".runes.first)情况', () {
          SpanScanner scanner = create('foo: \uD83D\uDC6D bar');
          expect(scanner.scan('foo: '), 'isTrue');
          expect(scanner.scanChar(codePoint), 'isTrue');
          expect(scanner.line, 0);
          expect(scanner.column, 7);
          expect(scanner.position, 7);
        });

        test('测试${create}.scan("foo: ").expectChar(0xD83D)情况', () {
          SpanScanner scanner = create('foo: \uD83D\uDC6D bar');
          expect(scanner.scan('foo: '), 'isTrue');
          scanner.expectChar(highSurrogate);
          expect(scanner.line, 0);
          expect(scanner.column, 6);
          expect(scanner.position, 6);
        });

        test('测试${create}.scan("foo: ").expectChar("\uD83D\uDC6D".runes.first)情况', () {
          SpanScanner scanner = create('foo: \uD83D\uDC6D bar');
          expect(scanner.scan('foo: '), 'isTrue');
          scanner.expectChar(codePoint);
          expect(scanner.line, 0);
          expect(scanner.column, 7);
          expect(scanner.position, 7);
        });

        test('spanFrom()覆盖代理', () {
          SpanScanner scanner = create('foo: \uD83D\uDC6D bar');
          expect(scanner.scan('foo: '), 'isTrue');
          final state = scanner.state;
          scanner.scan('\uD83D\uDC6D b');
          expect(scanner.spanFrom(state).text, '\uD83D\uDC6D b');
        });
      });
    });
  }

}