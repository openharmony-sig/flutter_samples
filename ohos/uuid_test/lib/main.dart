import 'package:flutter/material.dart';
import 'package:uuid_test/src/UuidOtherTestPage.dart';
import 'package:uuid_test/src/UuidV1TestPage.dart';
import 'package:uuid_test/src/UuidV4TestPage.dart';
import 'package:uuid_test/src/UuidV5TestPage.dart';
import 'package:uuid_test/src/UuidV6TestPage.dart';
import 'package:uuid_test/src/UuidV7TestPage.dart';
import 'package:uuid_test/src/UuidV8TestPage.dart';

import 'src/ExamplePage.dart';
import 'common/test_model_app.dart';
import 'common/test_route.dart';

void main() {
  final app = [
    MainItem('uuid_v1_test', UuidV1TestPage('uuid_v1_test')),
    MainItem('uuid_v4_test', UuidV4TestPage('uuid_v4_test')),
    MainItem('uuid_v5_test', UuidV5TestPage('uuid_v5_test')),
    MainItem('uuid_v6_test', UuidV6TestPage('uuid_v6_test')),
    MainItem('uuid_v7_test', UuidV7TestPage('uuid_v7_test')),
    MainItem('uuid_v8_test', UuidV8TestPage('uuid_v8_test')),
    MainItem('uuid_other_test', UuidOtherTestPage('uuid_other_test')),
    MainItem('example_test', ExamplePage('example_test')),
  ];

  runApp(TestModelApp(
      appName: 'uuid',
      data: app));
}
