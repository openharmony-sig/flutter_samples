import 'dart:math';
import 'dart:typed_data';

import 'package:uuid/data.dart';
import 'package:uuid/rng.dart';
import 'package:uuid/uuid.dart';

import '../common/test_page.dart';

class UuidV8TestPage extends TestPage {
  var uuid = const Uuid();

  UuidV8TestPage(super.title) {
    group('[UUID8]', () {
      for (final testCase in {
        'Tuesday, February 22, 2022 2:22:22.222000 PM GMT-05:00': [
          DateTime.fromMillisecondsSinceEpoch(1645557742222).toUtc(),
          '20220222-1922-8422-B222-B3CDC899A04D'
        ],
      }.entries) {
        test('Uuid().v8(config: V8Options(testCase.value[0] as DateTime, MathRNG(seed: 1).generate()))', () {
          final rand = MathRNG(seed: 1).generate();
          final uuid = Uuid().v8(config: V8Options(testCase.value[0] as DateTime, rand));
          expect(uuid.toUpperCase(), (testCase.value[1]));
        });
      }
    });

    test('uuid.v8buffer()', () {
      var buffer = Uint8List(16);
      var v8buffer = uuid.v8buffer(buffer);
      expect(v8buffer, '');
    });

    test('uuid.v8obj()', () {
      final rand = MathRNG(seed: 1).generate();
      var v8obj = uuid.v8obj(config: V8Options(DateTime.now(), rand));
      expect(v8obj, '');
    });
  }

}