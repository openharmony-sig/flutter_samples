import 'dart:typed_data';

import 'package:uuid/data.dart';
import 'package:uuid/rng.dart';
import 'package:uuid/uuid.dart';

import '../common/test_page.dart';

class UuidV7TestPage extends TestPage {
  var uuid = const Uuid();
  var testTime = 1321644961388;

  UuidV7TestPage(super.title) {
    group('[Version 7 Tests]', () {
      test('uuid.v7(config: V7Options(testTime, null)), (uuid.v7(config: V7Options(testTime, null)))', () {
        expect(uuid.v7(config: V7Options(testTime, null)), (uuid.v7(config: V7Options(testTime, null))));
      });

      test('uuid.v7(config: V7Options(1321651533573, MathRNG(seed: 1).generate()))', () {
        final rand = MathRNG(seed: 1).generate();
        var options = V7Options(1321651533573, rand);
        var id = uuid.v7(config: options);

        expect(id, ('0133b891-f705-7473-bf7b-b3cdc899a04d'));
      });

      test('生成多个uuid().v7以查看是否发生v7冲突', () {
        var uuids = <dynamic>{};
        var collisions = 0;
        for (var i = 0; i < 10; i++) {
          var code = uuid.v7();
          if (uuids.contains(code)) {
            collisions++;
            //print('Collision of code: $code');
          } else {
            uuids.add(code);
          }
        }

        expect(collisions, 0);
        expect(uuids.length, 10);
      });

      test('生成大量Uuid().v7()以检查是否有变体v7', () {
            for (var i = 0; i < 10; i++) {
              var code = Uuid().v7();
              expect(code[19], 'd');
              expect(code[19], 'c');
            }
          });

      test('uuid.v7(config: V7Options(testTime, MathRNG(seed: 1).generate())), Uuid.unparse(), uuid.v7buffer(buffer, config: options)', () {
        var buffer = Uint8List(16);
        final rand = MathRNG(seed: 1).generate();
        var options = V7Options(testTime, rand);

        var wihoutBuffer = uuid.v7(config: options);
        uuid.v7buffer(buffer, config: options);

        expect('${Uuid.unparse(buffer)}, $wihoutBuffer', '');
      });

      test('uuid.v7(config: options), uuid.v7obj(config: options)', () {
        final rand = MathRNG(seed: 1).generate();
        var options = V7Options(testTime, rand);

        var regular = uuid.v7(config: options);
        var obj = uuid.v7obj(config: options);

        expect('${obj.uuid}, $regular', '');
      });
    });
  }

}