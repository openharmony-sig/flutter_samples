import 'dart:typed_data';

import 'package:uuid/uuid.dart';

import '../common/test_page.dart';


class UuidV5TestPage extends TestPage {
  UuidV5TestPage(super.title) {
    var uuid = const Uuid();

    group('[Version 5 Tests]', () {
      test('uuid.v5(Uuid.NAMESPACE_URL, "www.google.com")', () {
        var u0 = uuid.v5(Uuid.NAMESPACE_URL, 'www.google.com');
        var u1 = uuid.v5(Uuid.NAMESPACE_URL, 'www.google.com');

        expect(u0, u1);
      });

      test('uuid.v5(null, "www.google.com")', () {
        var u0 = uuid.v5(null, 'www.google.com');
        var u1 = uuid.v5(null, 'www.google.com');

        expect(u0, ((u1)));
      });

      test('Uuid.unparse(buffer), uuid.v5(null, "www.google.com", options: {"randomNamespace": false})', () {
        var buffer = Uint8List(16);
        var wihoutBuffer = uuid
            .v5(null, 'www.google.com', options: {'randomNamespace': false});
        uuid.v5buffer(null, 'www.google.com', buffer,
            options: {'randomNamespace': false});

        expect('${Uuid.unparse(buffer)}, $wihoutBuffer', '');
      });

      test('uuid.v5(null, "www.google.com", options: {"randomNamespace": false}), uuid.v5obj(null, "www.google.com", options: {"randomNamespace": false})', () {
        var regular = uuid
            .v5(null, 'www.google.com', options: {'randomNamespace': false});
        var obj = uuid
            .v5obj(null, 'www.google.com', options: {'randomNamespace': false});

        expect('${obj.uuid}, $regular', '');
      });

      test('uuid.v5buffer()', () {
        var buffer = Uint8List(16);
        var v5buffer = uuid.v5buffer(null, 'www.google.com', buffer);
        expect(v5buffer, '');
      });
    });
  }

}