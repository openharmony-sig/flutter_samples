import 'dart:typed_data';

import 'package:uuid/rng.dart';
import 'package:uuid/uuid.dart';

import '../common/test_page.dart';


class UuidV4TestPage extends TestPage {
  var uuid = const Uuid();

  UuidV4TestPage(super.title) {
    group('[Version 4 Tests]', () {
      test('uuid.v4(options: {"rng": MathRNG(seed: 1),})', () {
        var u0 = uuid.v4(options: {
          'rng': MathRNG(seed: 1),
        });
        var u1 = 'a473ff7b-b3cd-4899-a04d-ea0fbd30a72e';
        expect(u0, u1);
      });

      test('uuid.v4buffer(buffer, options: {"rng": MathRNG(seed: 1),})', () {
        var buffer = Uint8List(16);
        uuid.v4buffer(buffer, options: {
          'rng': MathRNG(seed: 1),
        });

        var u1 = 'a473ff7b-b3cd-4899-a04d-ea0fbd30a72e';
        expect(Uuid.unparse(buffer), u1);
      });

      test('uuid.v4(options: {"rng": MathRNG(seed: 1),}), uuid.v4obj(options: {"rng": MathRNG(seed: 1),})', () {
        var regular = uuid.v4(options: {
          'rng': MathRNG(seed: 1),
        });
        var obj = uuid.v4obj(options: {
          'rng': MathRNG(seed: 1),
        });

        expect('${obj.uuid}, $regular', '');
      });

      test('uuid.v4(options: {"random": [] })', () {
        var u0 = uuid.v4(options: {
          'random': [
            0x10,
            0x91,
            0x56,
            0xbe,
            0xc4,
            0xfb,
            0xc1,
            0xea,
            0x71,
            0xb4,
            0xef,
            0xe1,
            0x67,
            0x1c,
            0x58,
            0x36
          ]
        });
        var u1 = '109156be-c4fb-41ea-b1b4-efe1671c5836';
        expect(u0, u1);
      });

      test('List.filled(1000, null).map((something) => uuid.v4()).toList()',
              () {
            var list = List.filled(1000, null).map((something) => uuid.v4()).toList();
            var setList = list.toSet();
            expect(list.length, (setList.length));
          });

      test('测试uuid.v4以确保它不会在大量条目上产生重复', () {
            const numToGenerate = 3 * 100 * 10;
            final values = <String>{}; // set of strings
            var generator = Uuid();

            var numDuplicates = 0;
            for (var i = 0; i < numToGenerate; i++) {
              final uuid = generator.v4();

              if (!values.contains(uuid)) {
                values.add(uuid);
              } else {
                numDuplicates++;
              }
            }

            expect(numDuplicates, 0);
          });

      test('Uuid.isValidUUID(fromString: guidString)', () {
        var guidString = '2400ee73-282c-4334-e153-08d8f922d1f9';

        var isValidDefault = Uuid.isValidUUID(fromString: guidString);
        expect(isValidDefault, false);

        var isValidRFC = Uuid.isValidUUID(
            fromString: guidString,
            validationMode: ValidationMode.strictRFC4122);
        expect(isValidRFC, false);

        var isValidNonStrict = Uuid.isValidUUID(
            fromString: guidString, validationMode: ValidationMode.nonStrict);
        expect(isValidNonStrict, true);
      });
    });
  }

}