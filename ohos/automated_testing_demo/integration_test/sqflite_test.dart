import 'dart:io';
import 'dart:io' as io;

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart' hide test;
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:test/test.dart' show test;
import 'dart:math';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  Database? _database;
  final String _tableName = "users";
  test('open-data', () async {
    var _dbName = "${Random().nextInt(36000)}.db";
    try {
      _database = await openDatabase(_dbName);
      final path = await getDatabasesPath();
      print("====open-data-success:$path");
    } catch (error) {
      print('====open-data-error' + error.toString());
    }
  });

  test('create', () async {
    if (_database != null) {
      try {
        await _database!.execute(
            "CREATE TABLE $_tableName(id INTEGER PRIMARY KEY, name TEXT, age INTEGER)");
        print('====create-success');
      } catch (error) {
        print('====create-error:' + error.toString());
      }
    }
  });
  test('insert-data', () async {
    if (_database != null) {
      try {
        final id =
            await _database!.insert(_tableName, {'name': 'John', 'age': 25});
        print("====insert-data-success:id=$id");
      } catch (error) {
        print("====insert-data-error:id=" + error.toString());
      }
    }
  });
  test('check-data', () async {
    if (_database != null) {
      final maps = await _database!.query(_tableName);
      print('====check-data-success:' +
          maps.map((map) => map.toString()).join("\n").toString());
    }
  });
  test('update', () async {
    if (_database != null) {
      try {
        final changeRows = await _database!.update(
            _tableName, {'name': 'Jack', 'age': 25},
            where: 'id = ?', whereArgs: [2]);
        print("====update-success:$changeRows");
      } catch (error) {
        print("====update-error:" + error.toString());
      }
    }
  });
  test('update-raw', () async {
    if (_database != null) {
      try {
        final changeRows = await _database!.rawUpdate(
            'UPDATE $_tableName SET name = ?, age = ? WHERE id = ?',
            ['Jack', 25, 1]);
        print("====update-raw-success:$changeRows");
      } catch (error) {
        print("====update-raw-error:" + error.toString());
      }
    }
  });
  test('update-raw-1', () async {
    if (_database != null) {
      try {
        final changeRows = await _database!.rawUpdate(
            'UPDATE OR ROLLBACK $_tableName Set name = ?, age = ? WHERE name = ?',
            ['Marco', 25, "Jack"]);
        print("====update-raw-1-success:$changeRows");
      } catch (error) {
        print("====update-raw-1-error:" + error.toString());
      }
    }
  });

  test('delete-raw', () async {
    if (_database != null) {
      try {
        int count = await _database!
            .rawDelete("DELETE FROM $_tableName WHERE id = ?", [2]);
        print("====delete-raw-success:$count");
      } catch (e) {
        print("====delete-raw-error:" + e.toString());
      }
    }
  });
}
