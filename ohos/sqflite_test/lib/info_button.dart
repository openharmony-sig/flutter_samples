import 'package:flutter/material.dart';

class InfoButton extends StatelessWidget {
  const InfoButton({required this.title, this.info, this.onTap, super.key});

  final String title;
  final String? info;
  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        decoration: const BoxDecoration(
          color: Color.fromARGB(255, 214, 214, 214),
          borderRadius: BorderRadius.all(Radius.circular(5)),
        ),
        margin: const EdgeInsets.all(10),
        padding: const EdgeInsets.symmetric(vertical: 15),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(title),
            Visibility(
                visible: info != null,
                child: const SizedBox(
                  height: 6,
                )),
            Visibility(
              visible: info != null,
              child: Text(info ?? ''),
            ),
          ],
        ),
      ),
    );
  }
}
