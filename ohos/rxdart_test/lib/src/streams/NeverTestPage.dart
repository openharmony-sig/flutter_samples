import 'package:rxdart/rxdart.dart';

import '../../common/test_page.dart';

class NeverTestPage extends TestPage {
  NeverTestPage(super.title) {
    test('NeverStream', () async {
      var onDataCalled = false, onDoneCalled = false, onErrorCalled = false;

      final stream = NeverStream<Never>();

      final subscription = stream.listen(
          (_) {onDataCalled = true;},
          onError: (Object e, StackTrace s) {
            onErrorCalled = false;
          },
          onDone: () {
            onDataCalled = true;
          }
      );

      await Future<void>.delayed(Duration(milliseconds: 10));

      await subscription.cancel();

      // We do not expect onData, onDone, nor onError to be called, as [never]
      // streams emit no items or errors, and they do not terminate
      expect(onDataCalled);
      expect(onDoneCalled);
      expect(onErrorCalled);
    });

    test('NeverStream.single.subscription', () async {
      final stream = NeverStream<void>();

      stream.listen(null);
      expect(() => stream.listen(null));
    });

    test('Rx.never', () async {
      var onDataCalled = false, onDoneCalled = false, onErrorCalled = false;

      final stream = Rx.never<Never>();

      final subscription = stream.listen(
          (_) {
            onDataCalled = true;
          },
          onError: (Object e, StackTrace s) {
            onErrorCalled = false;
          },
          onDone: () {
            onDataCalled = true;
          });

      await Future<void>.delayed(Duration(milliseconds: 10));

      await subscription.cancel();

      // We do not expect onData, onDone, nor onError to be called, as [never]
      // streams emit no items or errors, and they do not terminate
      expect(onDataCalled);
      expect(onDoneCalled);
      expect(onErrorCalled);
    });
  }

}