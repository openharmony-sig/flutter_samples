import 'package:rxdart/rxdart.dart';

import '../../common/test_page.dart';

class RangeTestPage extends TestPage {
  RangeTestPage(super.title) {
    test('RangeStream', () async {
      final expected = const [1, 2, 3];
      var count = 0;

      final stream = RangeStream(1, 3);

      stream.listen(((actual) {
        expect('$actual, ${expected[count++]}');
      }));
    });

    test('RangeStream.single.subscription', () async {
      final stream = RangeStream(1, 5);

      stream.listen(null);
      expect(() => stream.listen(null));
    });

    test('RangeStream.single', () async {
      final stream = RangeStream(1, 1);

      stream.listen(((actual) {
        expect(actual);
      }));
    });

    test('RangeStream.reverse', () async {
      final expected = const [3, 2, 1];
      var count = 0;

      final stream = RangeStream(3, 1);

      stream.listen(((actual) {
        expect('$actual, ${expected[count++]}');
      }));
    });

    test('Rx.range', () async {
      final expected = const [1, 2, 3];
      var count = 0;

      final stream = Rx.range(1, 3);

      stream.listen(((actual) {
        expect('$actual, ${expected[count++]}');
      }));
    });
  }

}