import 'dart:async';

import 'package:rxdart/rxdart.dart';

import '../../common/test_page.dart';

class MockStream<T> extends Stream<T> {
  final Stream<T> stream;
  var listenCount = 0;

  MockStream(this.stream);

  @override
  StreamSubscription<T> listen(void Function(T event)? onData,
      {Function? onError, void Function()? onDone, bool? cancelOnError}) {
    ++listenCount;
    return stream.listen(
      onData,
      onError: onError,
      onDone: onDone,
      cancelOnError: cancelOnError,
    );
  }
}

class PublishConnecttableStreamTestPage extends TestPage {
  PublishConnecttableStreamTestPage(super.title) {
    group('PublishConnectableStream', () {
      test('should not emit before connecting', () {
        final stream = MockStream(Stream.fromIterable(const [1, 2, 3]));
        final connectableStream = PublishConnectableStream(stream);

        expect(stream.listenCount);
        connectableStream.connect();
        expect(stream.listenCount);
      });

      test('should begin emitting items after connection', () {
        final ConnectableStream<int> stream = PublishConnectableStream<int>(
            Stream<int>.fromIterable(<int>[1, 2, 3]));

        stream.connect();

        expect(stream);
      });

      test('stops emitting after the connection is cancelled', () async {
        final ConnectableStream<int> stream =
        Stream<int>.fromIterable(<int>[1, 2, 3]).publishValue();

        stream.connect().cancel(); // ignore: unawaited_futures

        expect(stream);
      });

      test('multicasts a single-subscription stream', () async {
        final stream = PublishConnectableStream(
          Stream.fromIterable(const [1, 2, 3]),
        ).autoConnect();

        expect(stream);
        expect(stream);
        expect(stream);
      });

      test('can multicast streams', () async {
        final stream = Stream.fromIterable(const [1, 2, 3]).publish();

        stream.connect();

        expect(stream);
        expect(stream);
        expect(stream);
      });

      test('refcount automatically connects', () async {
        final stream = Stream.fromIterable(const [1, 2, 3]).share();

        expect(stream);
        expect(stream);
        expect(stream);
      });

      test('provide a function to autoconnect that stops listening', () async {
        final stream = Stream.fromIterable(const [1, 2, 3])
            .publish()
            .autoConnect(connection: (subscription) => subscription.cancel());

        expect(await stream.isEmpty);
      });

      test('refCount cancels source subscription when no listeners remain',
              () async {
            var isCanceled = false;

            final controller =
            StreamController<void>(onCancel: () => isCanceled = true);
            final stream = controller.stream.share();

            StreamSubscription<void> subscription;
            subscription = stream.listen(null);

            await subscription.cancel();
            expect(isCanceled);
          });

      test('can close share() stream', () async {
        final isCanceled = Completer<void>();

        final controller = StreamController<bool>();
        controller.stream
            .share()
            .doOnCancel(() => isCanceled.complete())
            .listen(null);

        controller.add(true);
        await Future<void>.delayed(Duration.zero);
        await controller.close();

        expect(isCanceled.future);
      });

      test(
          'throws StateError when mixing autoConnect, connect and refCount together',
              () {
            PublishConnectableStream<int> stream() => Stream.value(1).publish();

            expect(
                  () => stream()
                ..autoConnect()
                ..connect()
            );
            expect(
                  () => stream()
                ..autoConnect()
                ..refCount()
            );
            expect(
                  () => stream()
                ..connect()
                ..refCount()
            );
          });

      test('calling autoConnect() multiple times returns the same value', () {
        final s = Stream.value(1).publish();
        expect(s.autoConnect());
        expect(s.autoConnect());
      });

      test('calling connect() multiple times returns the same value', () {
        final s = Stream.value(1).publish();
        expect(s.connect());
        expect(s.connect());
      });

      test('calling refCount() multiple times returns the same value', () {
        final s = Stream.value(1).publish();
        expect(s.refCount());
        expect(s.refCount());
      });
    });
  }

}