import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class ExamplePage extends StatefulWidget {
  ExamplePage(this.title);

  String title;

  @override
  State<ExamplePage> createState() => _ExamplePageMainState();

}

class _ExamplePageMainState extends State<ExamplePage> {
  final publishSubject = PublishSubject();
  final behaviorSubject = BehaviorSubject();
  final replaySubject = ReplaySubject();
  
  TextEditingController subjectController = TextEditingController();

  String publishValue = '';
  String behaviorValue = '';
  String replayValue = '';

  int count = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: ListView(
        children: <Widget>[
          Container(height: 10),
          Text('PublishSubject接收消息情况'),
          Text(publishValue),
          Container(height: 10),
          Text('BehaviorSubject接收消息情况'),
          Text(behaviorValue),
          Container(height: 10),
          Text('ReplaySubject接收消息情况'),
          Text(replayValue),
          MaterialButton(
            color: Colors.blue,
            onPressed: () async {
              String value = 'The first "Hello World!!!"';
              publishValue = '';
              behaviorValue = '';
              behaviorValue = '';
              publishSubject.stream.listen((value) { publishValue = '$publishValue\npublish->${value.toString()}, ${value.runtimeType}'; });
              behaviorSubject.stream.listen((value) { behaviorValue = '$behaviorValue\nbehavior->${value.toString()}, ${value.runtimeType}'; });
              replaySubject.stream.listen((value) { replayValue = '$replayValue\nreplay->${value.toString()}, ${value.runtimeType}'; });
              publishSubject.add(value);
              behaviorSubject.add(value);
              replaySubject.add(value);
              setState(() {});
            },
            child: Text('发送String类型'),
          ),
          MaterialButton(
            color: Colors.blue,
            onPressed: () async {
              int value = 1;
              publishValue = '';
              behaviorValue = '';
              behaviorValue = '';
              publishSubject.stream.listen((value) { publishValue = '$publishValue\npublish->${value.toString()}, ${value.runtimeType}'; });
              behaviorSubject.stream.listen((value) { behaviorValue = '$behaviorValue\nbehavior->${value.toString()}, ${value.runtimeType}'; });
              replaySubject.stream.listen((value) { replayValue = '$replayValue\nreplay->${value.toString()}, ${value.runtimeType}'; });
              publishSubject.add(value);
              behaviorSubject.add(value);
              replaySubject.add(value);
              setState(() {});
            },
            child: Text('发送int类型'),
          ),
          MaterialButton(
            color: Colors.blue,
            onPressed: () async {
              Map value = {"name": "hangman", "age" : 18};
              publishValue = '';
              behaviorValue = '';
              behaviorValue = '';
              publishSubject.stream.listen((value) { publishValue = '$publishValue\npublish->${value.toString()}, ${value.runtimeType}'; });
              behaviorSubject.stream.listen((value) { behaviorValue = '$behaviorValue\nbehavior->${value.toString()}, ${value.runtimeType}'; });
              replaySubject.stream.listen((value) { replayValue = '$replayValue\nreplay->${value.toString()}, ${value.runtimeType}'; });
              publishSubject.add(value);
              behaviorSubject.add(value);
              replaySubject.add(value);
              setState(() {});
            },
            child: Text('发送Map类型'),
          ),
          MaterialButton(
            color: Colors.blue,
            onPressed: () async {
              List value = [1, 2, 3, 4];
              publishValue = '';
              behaviorValue = '';
              behaviorValue = '';
              publishSubject.stream.listen((value) { publishValue = '$publishValue\npublish->${value.toString()}, ${value.runtimeType}'; });
              behaviorSubject.stream.listen((value) { behaviorValue = '$behaviorValue\nbehavior->${value.toString()}, ${value.runtimeType}'; });
              replaySubject.stream.listen((value) { replayValue = '$replayValue\nreplay->${value.toString()}, ${value.runtimeType}'; });
              publishSubject.add(value);
              behaviorSubject.add(value);
              replaySubject.add(value);
              setState(() {});
            },
            child: Text('发送List类型'),
          ),
          MaterialButton(
            color: Colors.blue,
            onPressed: () async {
              var value;
              publishValue = '';
              behaviorValue = '';
              behaviorValue = '';
              publishSubject.stream.listen((value) { publishValue = '$publishValue\npublish->${value.toString()}, ${value.runtimeType}'; });
              behaviorSubject.stream.listen((value) { behaviorValue = '$behaviorValue\nbehavior->${value.toString()}, ${value.runtimeType}'; });
              replaySubject.stream.listen((value) { replayValue = '$replayValue\nreplay->${value.toString()}, ${value.runtimeType}'; });
              publishSubject.add(value);
              behaviorSubject.add(value);
              replaySubject.add(value);
              setState(() {});
            },
            child: Text('发送null值'),
          ),
        ],
      ),
    );
  }

}