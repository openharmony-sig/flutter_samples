import '../../common/test_page.dart';

class JoinTestPage extends TestPage {
  JoinTestPage(super.title) {
    test('Rx.join', () async {
      final joined = await Stream.fromIterable(const ['h', 'i']).join('+');

      expect(joined);
    });
  }

}