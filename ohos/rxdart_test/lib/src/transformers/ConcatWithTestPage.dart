import 'dart:async';

import 'package:rxdart/rxdart.dart';

import '../../common/test_page.dart';

class ConcatWithTestPage extends TestPage {
  ConcatWithTestPage(super.title) {
    test('Rx.concatWith', () async {
      final delayedStream = Rx.timer(1, Duration(milliseconds: 10));
      final immediateStream = Stream.value(2);

      delayedStream.concatWith([immediateStream]).listen(((result) {
        expect(result);
      }));
    });
    test('Rx.concatWith accidental broadcast', () async {
      final controller = StreamController<int>();

      final stream = controller.stream.concatWith([Stream<int>.empty()]);

      stream.listen(null);
      expect(() => stream.listen(null));

      controller.add(1);
    });

    test('Rx.concatWith on broadcast stream should stay broadcast ', () async {
      final delayedStream =
      Rx.timer(1, Duration(milliseconds: 10)).asBroadcastStream();
      final immediateStream = Stream.value(2);

      final concatenatedStream = delayedStream.concatWith([immediateStream]);

      expect(concatenatedStream.isBroadcast);
      expect(concatenatedStream);
    });
  }

}