import 'package:flutter/material.dart';

import 'PicturePage.dart';

class PullListPage extends StatefulWidget {
  const PullListPage({super.key, required this.title});
  final String title;

  @override
  _PullListPageState createState() => _PullListPageState();
}

class _PullListPageState extends State<PullListPage> {
  List<PicBean> list = []; //列表要展示的数据
  List<String> picNams = [
    "assets/images/1.png",
    "assets/images/2.jpeg",
    "assets/images/3.jpeg",
    "assets/images/4.jpg",
    "assets/images/6.jpg",
  ];
  final ScrollController _scrollController = ScrollController(); //listview的控制器
  bool isLoading = false; //是否正在加载数据

  _createPicBean(int i) {
    return PicBean(picNams[i % picNams.length], 0).setIndex(i);
  }

  @override
  void initState() {
    super.initState();
    getData();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        // 滑动到了最底部
        _getMore();
      }
    });
  }

  /// 初始化list数据 加延时模仿网络请求
  Future getData() async {
    await Future.delayed(const Duration(seconds: 2), () {
      setState(() {
        list = List.generate(15, (i) => _createPicBean(i));
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: RefreshIndicator(
        onRefresh: _onRefresh,
        child: ListView.builder(
          itemBuilder: _renderRow,
          itemCount: list.length + 1,
          controller: _scrollController,
        ),
      ),
    );
  }

  Widget _renderRow(BuildContext context, int index) {
    if (index < list.length) {
      return Column(
        children: [
          Text(list[index].name),
          PictureWidget(bean: list[index]),
        ],
      );
    }
    return _getMoreWidget();
  }

  /// 下拉刷新方法,为list重新赋值
  Future<Null> _onRefresh() async {
    await Future.delayed(const Duration(seconds: 3), () {
      setState(() {
        list = List.generate(20, (i) => _createPicBean(i));
      });
    });
  }

  /// 上拉加载更多
  Future _getMore() async {
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });
      await Future.delayed(const Duration(seconds: 1), () {
        setState(() {
          list.addAll(List.generate(5, (i) => _createPicBean(i)));
          isLoading = false;
        });
      });
    }
  }

  /// 加载更多时显示的组件,给用户提示
  Widget _getMoreWidget() {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: const <Widget>[
            Text(
              '加载中...',
              style: TextStyle(fontSize: 16.0),
            ),
            CircularProgressIndicator(
              strokeWidth: 1.0,
            )
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    debugPrint("PullListPage, dispose");
    _scrollController.dispose();
    super.dispose();
  }
}
