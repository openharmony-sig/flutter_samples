import 'package:flutter/cupertino.dart';

import 'base_page.dart';

class MainItem {
  /// Main item.
  MainItem(this.title, this.route);

  /// title.
  String title;

  /// Page route.
  Widget route;
}
