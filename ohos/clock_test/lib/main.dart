import 'package:clock_test/src/ClockTestPage.dart';
import 'package:clock_test/src/DefaultTestPage.dart';
import 'package:clock_test/src/ExampleTestPage.dart';
import 'package:clock_test/src/StopwatchTestPage.dart';
import 'package:flutter/material.dart';

import 'common/test_model_app.dart';
import 'common/test_route.dart';


void main() {
  final app = [
    MainItem('example_test', const ExampleTestPage(title: 'example_test')),
    MainItem('clock_test', ClockTestPage('clock_test')),
    MainItem('default_test', DefaultTestPage('default_test')),
    MainItem('stopwatch_test', StopwatchTestPage('stopwatch_test')),
  ];

  runApp(TestModelApp(
      appName: 'clock',
      data: app));
}
