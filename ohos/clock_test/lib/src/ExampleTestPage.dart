import 'dart:async';

import 'package:clock/clock.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

DateTime date(int year, [int? month, int? day]) =>
    DateTime(year, month ?? 1, day ?? 1);

class ExampleTestPage extends StatefulWidget {
  const ExampleTestPage({super.key, required this.title});

  final String title;

  @override
  State<StatefulWidget> createState() => _ExampleTestPageState();

}

class _ExampleTestPageState extends State<ExampleTestPage> {
  Stopwatch _stopwatch = Stopwatch();

  String time = '';

  String timeString = "00:00:00";
  Stopwatch stopwatch = Stopwatch();
  late Timer timer;

  void start() {
    stopwatch.start();
    timer = Timer.periodic(const Duration(milliseconds: 100), update);
  }

  void update(Timer t) {
    if (stopwatch.isRunning) {
      setState(() {
        timeString =
            (stopwatch.elapsed.inMinutes % 60).toString().padLeft(2, "0") +
                ":" +
                (stopwatch.elapsed.inSeconds % 60).toString().padLeft(2, "0") +
                ":" +
                (stopwatch.elapsed.inMilliseconds % 1000 / 10)
                    .clamp(0, 99)
                    .toStringAsFixed(0)
                    .padLeft(2, "0");
      });
    }
  }

  void stop() {
    setState(() {
      timer.cancel();
      stopwatch.stop();
    });
  }

  void reset() {
    timer.cancel();
    stopwatch.reset();
    setState(() {
      timeString = "00:00:00";
    });
    stopwatch.stop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 80, vertical: 60),
              child: Text("STOPWATCH",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.grey.shade900,
                  )),
            ),
            Expanded(
              child: Container(
                width: 250,
                height: 250,
                decoration: BoxDecoration(
                    color: Colors.pink,
                    shape: BoxShape.circle,
                    boxShadow: [
                      const BoxShadow(
                          offset: Offset(10, 10),
                          color: Colors.red,
                          blurRadius: 5),
                      BoxShadow(
                          offset: const Offset(-10, -10),
                          color: Colors.white.withOpacity(0.85),
                          blurRadius: 5)
                    ]),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('$timeString',
                        style: TextStyle(
                          fontSize: 40,
                          color: Colors.grey.shade900,
                        ))
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 60),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  TextButton(
                      onPressed: reset,
                      child: Container(
                        height: 100,
                        width: 100,
                        decoration: const BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                        ),
                        child: const Icon(Icons.refresh, size: 60),
                      )),
                  TextButton(
                    onPressed: () => {stopwatch.isRunning ? stop() : start()},
                    child: Container(
                      height: 100,
                      width: 100,
                      decoration: const BoxDecoration(
                        color: Colors.white,
                        shape: BoxShape.circle,
                      ),
                      child: Icon(
                          stopwatch.isRunning ? Icons.pause : Icons.play_arrow,
                          size: 60),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          clockTest();
        },
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

void clockTest() {
  Clock clock = Clock.fixed(date(2023));
  print(DateTime.now().difference(const Clock().now()).inMilliseconds.abs());
  print(clock.agoBy(const Duration(days: 366)));
  print(clock.fromNowBy(const Duration(days: 365)));
  print(clock.ago(
      days: 1,
      hours: 1,
      minutes: 1,
      seconds: 1,
      milliseconds: 1,
      microseconds: 1000));
  print(clock.fromNow(
      days: 1,
      hours: 1,
      minutes: 1,
      seconds: 1,
      milliseconds: 1,
      microseconds: 1000));
}

T runWithTiming<T>(T Function() callback) {
  var stopwatch = clock.stopwatch()..start();
  var result = callback();
  print('It took ${stopwatch.elapsed}!');
  return result;
}