import 'package:clock/clock.dart';
import 'package:clock_test/src/utils.dart';

import '../common/test_page.dart';

class ClockTestPage extends TestPage {
  ClockTestPage(super.title) {

    test('Clock().now()', () {
      expect(const Clock().now(), 'isNotNull');
    });

    // This test may be flaky on certain systems. I ran it over 10 million
    // cycles on my machine without any failures, but that's no guarantee.
    test('should be close enough to system clock', () {
      // At 10ms the test doesn't seem to be flaky.
      var epsilon = 10;
      expect(DateTime.now().difference(const Clock().now()).inMilliseconds.abs(), epsilon);
      expect(DateTime.now().difference(const Clock().now()).inMilliseconds.abs(), epsilon);
    });

    test('Clock(() => (date(2013), date(2014))).now()', () {
      var time = date(2013);
      var fixedClock = Clock(() => time);
      expect(fixedClock.now(), date(2013));

      time = date(2014);
      expect(fixedClock.now(), date(2014));
    });

    test('Clock.fixed(date(2013)).now()', () {
      expect(Clock.fixed(date(2013)).now(), date(2013));
    });

    test('Clock.fixed(date(2013)).agoBy(const Duration(days: 366))', () {
      Clock clock = Clock.fixed(date(2013));
      expect(clock.agoBy(const Duration(days: 366)), date(2012));
    });

    test('Clock.fixed(date(2013)).fromNowBy(const Duration(days: 365))', () {
      Clock clock = Clock.fixed(date(2013));
      expect(clock.fromNowBy(const Duration(days: 365)), date(2014));
    });

    test('Clock.fixed(date(2013)).ago()', () {
      Clock clock = Clock.fixed(date(2013));
      expect(
          clock.ago(
              days: 1,
              hours: 1,
              minutes: 1,
              seconds: 1,
              milliseconds: 1,
              microseconds: 1000),
          DateTime(2012, 12, 30, 22, 58, 58, 998));
    });

    test('Clock.fixed(date(2013)).fromNow()', () {
      Clock clock = Clock.fixed(date(2013));
      expect(
          clock.fromNow(
              days: 1,
              hours: 1,
              minutes: 1,
              seconds: 1,
              milliseconds: 1,
              microseconds: 1000),
          DateTime(2013, 1, 2, 1, 1, 1, 2));
    });

    test('Clock.fixed(date(2013)).microsAgo(1000)', () {
      Clock clock = Clock.fixed(date(2013));
      expect(clock.microsAgo(1000), DateTime(2012, 12, 31, 23, 59, 59, 999));
    });

    test('Clock.fixed(date(2013)).microsFromNow(1000)', () {
      Clock clock = Clock.fixed(date(2013));
      expect(clock.microsFromNow(1000), DateTime(2013, 1, 1, 0, 0, 0, 1));
    });

    test('Clock.fixed(date(2013)).millisAgo(1000)', () {
      Clock clock = Clock.fixed(date(2013));
      expect(clock.millisAgo(1000), DateTime(2012, 12, 31, 23, 59, 59));
    });

    test('Clock.fixed(date(2013)).millisFromNow(3)', () {
      Clock clock = Clock.fixed(date(2013));
      expect(clock.millisFromNow(3), DateTime(2013, 1, 1, 0, 0, 0, 3));
    });

    test('Clock.fixed(date(2013)).secondsAgo(10)', () {
      Clock clock = Clock.fixed(date(2013));
      expect(clock.secondsAgo(10), DateTime(2012, 12, 31, 23, 59, 50));
    });

    test('Clock.fixed(date(2013)).secondsFromNow(3)', () {
      Clock clock = Clock.fixed(date(2013));
      expect(clock.secondsFromNow(3), DateTime(2013, 1, 1, 0, 0, 3));
    });

    test('Clock.fixed(date(2013)).minutesAgo(10)', () {
      Clock clock = Clock.fixed(date(2013));
      expect(clock.minutesAgo(10), DateTime(2012, 12, 31, 23, 50));
    });

    test('Clock.fixed(date(2013)).minutesFromNow(3)', () {
      Clock clock = Clock.fixed(date(2013));
      expect(clock.minutesFromNow(3), DateTime(2013, 1, 1, 0, 3));
    });

    test('Clock.fixed(date(2013)).hoursAgo(10)', () {
      Clock clock = Clock.fixed(date(2013));
      expect(clock.hoursAgo(10), DateTime(2012, 12, 31, 14));
    });

    test('Clock.fixed(date(2013)).hoursFromNow(3)', () {
      Clock clock = Clock.fixed(date(2013));
      expect(clock.hoursFromNow(3), DateTime(2013, 1, 1, 3));
    });

    test('Clock.fixed(date(2013)).daysAgo(10)', () {
      Clock clock = Clock.fixed(date(2013));
      expect(clock.daysAgo(10), date(2012, 12, 22));
    });

    test('Clock.fixed(date(2013)).daysFromNow(3)', () {
      Clock clock = Clock.fixed(date(2013));
      expect(clock.daysFromNow(3), date(2013, 1, 4));
    });

    test('Clock.fixed(date(2013)).monthsAgo(x)', () {
      Clock clock = Clock.fixed(date(2013));
      expect(clock.monthsAgo(1), date(2012, 12, 1));
      expect(clock.monthsAgo(2), date(2012, 11, 1));
      expect(clock.monthsAgo(3), date(2012, 10, 1));
      expect(clock.monthsAgo(4), date(2012, 9, 1));
    });

    test('Clock.fixed(date(2013)).monthsFromNow(x)', () {
      Clock clock = Clock.fixed(date(2013));
      expect(clock.monthsFromNow(1), date(2013, 2, 1));
      expect(clock.monthsFromNow(2), date(2013, 3, 1));
      expect(clock.monthsFromNow(3), date(2013, 4, 1));
      expect(clock.monthsFromNow(4), date(2013, 5, 1));
    });

    test('fixed(2013, 5, 31).monthsAgo(6)', () {
      expect(fixed(2013, 5, 31).monthsAgo(6), date(2012, 11, 30));
    });

    test('fixed(2013, 3, 31).monthsAgo(1)', () {
      expect(fixed(2013, 3, 31).monthsAgo(1), date(2013, 2, 28));
    });

    test('fixed(2013, 5, 31).monthsAgo(3)', () {
      expect(fixed(2013, 5, 31).monthsAgo(3), date(2013, 2, 28));
    });

    test('fixed(2004, 3, 31).monthsAgo(1)', () {
      expect(fixed(2004, 3, 31).monthsAgo(1), date(2004, 2, 29));
    });

    test('fixed(2013, 3, 31).monthsFromNow(3)', () {
      expect(fixed(2013, 3, 31).monthsFromNow(3), date(2013, 6, 30));
    });

    test('fixed(2003, 12, 31).monthsFromNow(2)', () {
      expect(fixed(2003, 12, 31).monthsFromNow(2), date(2004, 2, 29));
    });

    test('fixed(2004, 2, 29).yearsAgo(1)', () {
      expect(fixed(2004, 2, 29).yearsAgo(1), date(2003, 2, 28));
    });

    test('fixed(2004, 2, 29).monthsAgo(12)', () {
      expect(fixed(2004, 2, 29).monthsAgo(12), date(2003, 2, 28));
    });

    test('fixed(2004, 2, 29).yearsFromNow(1)', () {
      expect(fixed(2004, 2, 29).yearsFromNow(1), date(2005, 2, 28));
    });

    test('fixed(2004, 2, 29).monthsFromNow(12)', () {
      expect(fixed(2004, 2, 29).monthsFromNow(12), date(2005, 2, 28));
    });

    test('Clock.fixed(date(2013)).yearsAgo(1)', () {
      Clock clock = Clock.fixed(date(2013));
      expect(clock.yearsAgo(1), date(2012, 1, 1)); // leap year
      expect(clock.yearsAgo(2), date(2011, 1, 1));
      expect(clock.yearsAgo(3), date(2010, 1, 1));
      expect(clock.yearsAgo(4), date(2009, 1, 1));
      expect(clock.yearsAgo(5), date(2008, 1, 1)); // leap year
      expect(clock.yearsAgo(6), date(2007, 1, 1));
      expect(clock.yearsAgo(30), date(1983, 1, 1));
      expect(clock.yearsAgo(2013), date(0, 1, 1));
    });

    test('Clock.fixed(date(2013)).yearsFromNow(1)', () {
      Clock clock = Clock.fixed(date(2013));
      expect(clock.yearsFromNow(1), date(2014, 1, 1));
      expect(clock.yearsFromNow(2), date(2015, 1, 1));
      expect(clock.yearsFromNow(3), date(2016, 1, 1));
      expect(clock.yearsFromNow(4), date(2017, 1, 1));
      expect(clock.yearsFromNow(5), date(2018, 1, 1));
      expect(clock.yearsFromNow(6), date(2019, 1, 1));
      expect(clock.yearsFromNow(30), date(2043, 1, 1));
      expect(clock.yearsFromNow(1000), date(3013, 1, 1));
    });
  }

}