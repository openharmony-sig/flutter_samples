import { DataModel } from './DataModel';
import router from '@ohos.router';

@Entry()
@Component
struct MainPage {
  @State mainIdentifier: number = 0;

  onCountUpdate(newCount: number) {
    this.mainIdentifier = newCount;
  }

  aboutToAppear(): void {
    this.mainIdentifier = DataModel.instance.getCounter();
    DataModel.instance.addObserver(this);
  }

  aboutToDisappear(): void {
    DataModel.instance.removeObserver(this);
  }

  onClickNext() {
    let num = this.mainIdentifier % 3;
    if (num == 1) {
      router.pushUrl({
        "url": "pages/DoubleFlutterPage",
      });
    } else if (num == 2) {
      router.pushUrl({
        "url": "pages/LazyListFlutterPage",
      });
    } else {
      router.pushUrl({
        "url": "pages/SingleFlutterPage",
      });
    }
  }

  onClickAdd() {
    DataModel.instance.increase();
  }

  build() {
    Row() {
      Column() {
        Row() {
          Text("Count:")
          Text(String(this.mainIdentifier))
        }
      }.width('50%')
      .height('100%')
      .alignItems(HorizontalAlign.Center)
      .justifyContent(FlexAlign.Center)

      Column() {
        Button("Add").onClick(() => {
          this.onClickAdd();
        }).margin(2)
        Button("Next").onClick(() => {
          this.onClickNext();
        }).margin(2)
      }
      .width('50%')
      .height('100%')
      .padding(20)
      .alignItems(HorizontalAlign.End)
      .justifyContent(FlexAlign.End)
    }.width('100%')
    .height('100%')
    .alignItems(VerticalAlign.Center)
    .justifyContent(FlexAlign.SpaceBetween)
  }
}
