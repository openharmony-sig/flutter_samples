# multiple_flutters_ohos

This is an add-to-app sample that uses the Flutter engine group API to host
multiple instances of Flutter in the app.

## Getting Started

```sh
cd ../multiple_flutters_module
flutter pub get
flutter build har --local-engine=$ENGINE_DEBUG --debug
cd -
cp -r ../multiple_flutters_module/.ohos/har/* ./har/
open -a "Deveco-Studio" ../multiple_flutters_ohos/ # macOS command
# Config signing configs: (File->Project Structure->Signing Configs->Support HarmonyOS & Automaticlally generate signature->OK)
# (build and run)
```

For more information see
[multiple_flutters_module](../multiple_flutters_module/README.md).
