import 'package:flutter/material.dart';
import 'package:music_player/pages/home_page.dart';
import 'components/FpsTextComponent.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "音乐播放器",
        theme: ThemeData(primaryColor: Colors.blue),
        home: const Scaffold(
          body: HomePage(),
          // floatingActionButton: Container(
          //   margin: const EdgeInsets.only(bottom: 80,left: 20),
          //   width: 80,
          //   child: FloatingActionButton(
          //     onPressed: () {
          //       // Add your onPressed code here!
          //     },
          //     backgroundColor: const Color(0x33000000),
          //     shape: RoundedRectangleBorder(
          //         borderRadius: BorderRadius.circular(6.0)),
          //     child: const FpsTextComponent(),
          //   ),
          // ),
          // floatingActionButtonLocation: FloatingActionButtonLocation.startDocked,
        ));
  }
}
