import 'package:logging/logging.dart';

class Log {

  static final log = Logger('MusicPlayer');

  static void config(String msg) {
    print(msg);
    log.config(msg);
  }

  static void info(String msg) {
    print(msg);
    log.info(msg);
  }

  static void warning(String msg) {
    print(msg);
    log.warning(msg);
  }

  static void severe(String msg) {
    print(msg);
    log.severe(msg);
  }
}
